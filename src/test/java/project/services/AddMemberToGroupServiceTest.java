package project.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddMemberRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.AddMemberRequestAssembler;
import project.exceptions.AlreadyExistsException;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * US003:
 * Como gestor de sistema, quero acrescentar pessoas ao grupo.
 */
@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
public class AddMemberToGroupServiceTest {
    @Autowired
    AddMemberToGroupService addMemberToGroupService;
    @Autowired
    GroupRepository groupMockRepository;

    Name pedroName;
    Address pedroBirthPlace;
    Date birthDate;
    Email pedroEmail;
    Person pedro;
    PersonID pedroID;
    Description groupDescriptionFamily;
    Group groupFamily;
    GroupID groupIDFamily;

    String personEmail;
    String groupDescription;
    GroupDTO expected;
    Set<PersonID> membersSet;
    Set<PersonID> responsibleSet;
    Set<Category> categoriesSet;


    @BeforeEach
    void setUp() {
        pedroName = new Name("Pedro");
        pedroBirthPlace = new Address("Porto");
        birthDate = new Date(LocalDateTime.of(1982, 3, 14, 0, 0));
        pedroEmail = new Email("pedro.1234@email.com");
        pedro = new Person(pedroName, pedroBirthPlace, birthDate, pedroEmail, null, null);
        pedroID = pedro.getPersonID();

        groupDescriptionFamily = new Description("Family");
        groupFamily = new Group(groupDescriptionFamily, pedroID);
        groupIDFamily = groupFamily.getID();

        groupDescription = "Family";
        personEmail = "pedro.1234@gmail.com";

        expected = new GroupDTO();
        expected.setGroupID(groupIDFamily);
        membersSet = new HashSet<>();
        responsibleSet = new HashSet<>();
        membersSet.add(pedroID);
        categoriesSet = new HashSet<>();
        responsibleSet.add(pedroID);

        membersSet.add(pedroID);
        expected.setMembers(membersSet);
        expected.setResponsibles(responsibleSet);
        expected.setCategories(categoriesSet);
        expected.setCreationDate(new Date(LocalDateTime.now()));

    }

    @DisplayName("addMemberToGroup - Happy Path")
    @Test
    void addMemberToAGroupHappyPath() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO = AddMemberRequestAssembler.mapToDTO(groupDescription, personEmail);
        Mockito.when(groupMockRepository.findById(addMemberRequestDTO.getGroupID())).thenReturn(Optional.of(groupFamily));
        Mockito.when(groupMockRepository.save(groupFamily)).thenReturn(groupFamily);

        //Act
        GroupDTO result = addMemberToGroupService.addMember(addMemberRequestDTO);

        //Assert
        assertEquals(expected.getGroupID(), result.getGroupID());
    }

    @DisplayName("addMemberToGroup - No GroupID Found")
    @Test
    void addMemberToGroupNoGroupIDFound() {
        //Arrange
        String groupDescription2 = "Football";
        String personEmail2 = "marco@isep.ipp.pt";

        AddMemberRequestDTO addMemberRequestDTO = AddMemberRequestAssembler.mapToDTO(groupDescription2, personEmail2);
        Mockito.when(groupMockRepository.findById(addMemberRequestDTO.getGroupID())).thenReturn(Optional.empty());

        //Act
        Exception exception = assertThrows(NotFoundException.class, () -> {
            addMemberToGroupService.addMember(addMemberRequestDTO);
        });

        String expectedMessage = "GroupID";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);
    }

    @DisplayName("addMemberToGroup - No GroupID cannot be null")
    @Test
    void addMemberToGroupGroupIDNull() {
        //Arrange
        String personEmail2 = "marco@isep.ipp.pt";
        Email persoMarcoEmail = new Email(personEmail2);
        PersonID personMarcoID = new PersonID(persoMarcoEmail);

        Mockito.when(groupMockRepository.findById(null)).thenThrow(new NullPointerException("The GroupID cannot be null"));
        AddMemberRequestDTO addMemberRequestDTO = new AddMemberRequestDTO(null, personMarcoID);

        //Act
        Exception exception = assertThrows(NullPointerException.class, () -> {
            addMemberToGroupService.addMember(addMemberRequestDTO);
        });

        String expectedMessage = "The GroupID cannot be null";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);
    }

    @DisplayName("addMemberToGroupService - MemberAlreadyExist")
    @Test
    void addMemberToGroupMemberAlreadyExist() {
        //Arrange

        Email ruiEmail = new Email("marco@isep.ipp.pt");
        PersonID ruiID = new PersonID(ruiEmail);
        Person rui = new Person(new Name("Rui"), new Address("Porto"), birthDate, ruiEmail, null, null);
        groupFamily.addMember(ruiID);

        AddMemberRequestDTO addMemberRequestDTO = new AddMemberRequestDTO(groupIDFamily, ruiID);
        Mockito.when(groupMockRepository.findById(addMemberRequestDTO.getGroupID())).thenReturn(Optional.of(groupFamily));
        Mockito.when(groupMockRepository.save(groupFamily)).thenThrow(new AlreadyExistsException("The Member already exist"));

        //Act
        Exception exception = assertThrows(AlreadyExistsException.class, () -> {
            addMemberToGroupService.addMember(addMemberRequestDTO);
        });

        String expectedMessage = "Member";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);
    }
}