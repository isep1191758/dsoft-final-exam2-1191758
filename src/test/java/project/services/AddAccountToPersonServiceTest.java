package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.dto.AddAccountToPersonRequestDTO;
import project.exceptions.AlreadyExistsException;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
class AddAccountToPersonServiceTest {
    @Autowired
    private AddAccountToPersonService addAccountToPersonService;
    @Autowired
    private AccountRepository accountMockRepository;
    @Autowired
    private PersonRepository personMockRepository;

    @DisplayName("addAccountToPerson - Happy Path")
    @Test
    void addAccountToPersonHappyPath() {
        //Arrange
        Description description = new Description("Junior School Account");
        Denomination denomination = new Denomination("Junior Dance");

        LocalDateTime personBirthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        Email personEmail = new Email("rosaliadejesusalvarengas@gmail.com");
        PersonID personID = new PersonID(personEmail);
        Person person = new Person(new Name("Rosalia de Jesus Alvarengas"), new Address("Porto"), new Date(personBirthDate), personEmail, null, null);
        Mockito.when(personMockRepository.findById(new PersonID(personEmail))).thenReturn(Optional.of(person));

        Mockito.when(accountMockRepository.findById(new AccountID(denomination, personID))).thenReturn(Optional.empty());

        AccountDTO expectedA = new AccountDTO();
        expectedA.setAccountID(new AccountID(denomination, personID));
        expectedA.setDescription(description);

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = new AddAccountToPersonRequestDTO(denomination, description, person.getPersonID());

        //Act
        AccountDTO result = addAccountToPersonService.addAccountToPerson(addAccountToPersonRequestDTO);

        //Assert
        assertEquals(expectedA.getAccountID(), result.getAccountID());
        assertEquals(expectedA.getDescription(), result.getDescription());
    }

    @DisplayName("addAccountToPerson - PersonIDNull")
    @Test
    void addAccountPersonIDNull() {
        //Arrange
        Description description = new Description("School Account");
        Denomination denomination = new Denomination("Dance School");

        Mockito.when(personMockRepository.findById(null)).thenThrow(new NullPointerException("The PersonID cannot be null"));

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = new AddAccountToPersonRequestDTO(denomination, description, null);
        Exception exceptionA = assertThrows(NullPointerException.class, () -> {
            addAccountToPersonService.addAccountToPerson(addAccountToPersonRequestDTO);
        });

        String expectedMessage = "The PersonID cannot be null";
        String actualMessage = exceptionA.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @DisplayName("addAccountToPerson - Account already exist in repository")
    @Test
    void addAccountToPersonThatAlreadyExist() {
        Description description = new Description("Schools Account");
        Denomination denomination = new Denomination("Danceteria School");

        LocalDateTime personBirthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        Email personEmail = new Email("rosaliamaria@gmail.com");
        PersonID personID = new PersonID(personEmail);
        Person person = new Person(new Name("Rosalia Maria de Jesus"), new Address("Porto"), new Date(personBirthDate), personEmail, null, null);

        Mockito.when(personMockRepository.findById(personID)).thenReturn(Optional.of(person));

        Mockito.when(accountMockRepository.findById(new AccountID(denomination, personID))).thenThrow(new AlreadyExistsException("Account already exists"));

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = new AddAccountToPersonRequestDTO(denomination, description, person.getPersonID());
        Exception exception = assertThrows(AlreadyExistsException.class, () -> {
            addAccountToPersonService.addAccountToPerson(addAccountToPersonRequestDTO);
        });

        String expectedMessage = "Account already exists";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @DisplayName("addAccountToPerson - Person does not exist in repository")
    @Test
    void addAccountPersonDoesntExist() {
        Description description = new Description("Schools Account");
        Denomination denomination = new Denomination("Danceteria School");
        Email person2Email = new Email("gil@gmail.com");
        LocalDateTime personBirthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        Person person2 = new Person((new Name("Lia")), new Address("Porto"), new Date(personBirthDate), person2Email, null, null);

        Mockito.when(personMockRepository.findById(person2.getPersonID())).thenThrow(new NotFoundException("The Person does not exist"));

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = new AddAccountToPersonRequestDTO(denomination, description, person2.getPersonID());
        Exception exception = assertThrows(NotFoundException.class, () -> {
            addAccountToPersonService.addAccountToPerson(addAccountToPersonRequestDTO);
        });

        String expectedMessage = "The Person does not exist";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @DisplayName("addAccountToPerson - Person does not exist in repository")
    @Test
    void addAccountPersonDoesntExistIntegration() {
        Description description = new Description("Schools Account");
        Denomination denomination = new Denomination("Danceteria School");
        Email person2Email = new Email("gil@gmail.com");
        LocalDateTime personBirthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        Person person2 = new Person((new Name("Lia")), new Address("Porto"), new Date(personBirthDate), person2Email, null, null);

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = new AddAccountToPersonRequestDTO(denomination, description, person2.getPersonID());
        Exception exception = assertThrows(NotFoundException.class, () -> {
            addAccountToPersonService.addAccountToPerson(addAccountToPersonRequestDTO);
        });

        String expectedMessage = "Person";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }
}