package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.GroupID;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class AddMemberRequestDTOTest {
    AddMemberRequestDTO addMemberRequestDTO;
    GroupID groupDescription2;
    GroupID groupDescription;
    PersonID personEmail;
    PersonID personID;

    @BeforeEach
    void setUp() {
        // Arrange
        groupDescription = new GroupID(new Description("Family"));
        personEmail = new PersonID(new Email("pedro@gmail.com"));
        addMemberRequestDTO = new AddMemberRequestDTO(groupDescription, personEmail);
        personID = new PersonID(new Email("marco@gmail.com"));
        groupDescription2 = new GroupID(new Description("Family123"));
    }

    @DisplayName("Get Group Description")
    @Test
    void getGroupDescription() {
        PersonID result = addMemberRequestDTO.getPersonID();
    }

    @DisplayName("Set Group Description")
    @Test
    void setGroupDescription() {

        addMemberRequestDTO.setGroupID(groupDescription2);
    }

    @DisplayName("Get Person Email")
    @Test
    void getPersonEmail() {

        PersonID result = addMemberRequestDTO.getPersonID();
    }

    @DisplayName("Set Person Email")
    @Test
    void setPersonEmail() {

        addMemberRequestDTO.setPersonID(personID);
    }

    @DisplayName("Override Equals - True")
    @Test
    void testEquals() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO1 = new AddMemberRequestDTO(groupDescription, personEmail);
        AddMemberRequestDTO addMemberRequestDTO2 = new AddMemberRequestDTO(groupDescription, personEmail);
        //Act
        boolean result = addMemberRequestDTO1.equals(addMemberRequestDTO2);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - Same Exact Object")
    @Test
    void testEqualsSameExactObject() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO = new AddMemberRequestDTO(groupDescription, personEmail);
        //Act
        boolean result = addMemberRequestDTO.equals(addMemberRequestDTO);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Equals - Different")
    @Test
    void testEqualsDifferent() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO1 = new AddMemberRequestDTO(groupDescription, personEmail);
        AddMemberRequestDTO addMemberRequestDTO2 = new AddMemberRequestDTO(groupDescription2, personID);
        //Act
        boolean result = addMemberRequestDTO1.equals(addMemberRequestDTO2);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalse() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO1 = new AddMemberRequestDTO(groupDescription, personEmail);

        //Act
        boolean result = addMemberRequestDTO1.equals(null);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - False")
    @Test
    void testEqualsFalseAnotherObject() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO1 = new AddMemberRequestDTO(groupDescription, personEmail);

        //Act
        boolean result = addMemberRequestDTO1.equals(personEmail);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Override Equals - True")
    @Test
    void testEqualsTrue() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO1 = new AddMemberRequestDTO(groupDescription, personEmail);

        //Act
        boolean result = addMemberRequestDTO1.equals(addMemberRequestDTO1);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Override Hash Code - Not Equals")
    @Test
    void testHashCode() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO1 = new AddMemberRequestDTO(groupDescription, personEmail);
        AddMemberRequestDTO addMemberRequestDTO2 = new AddMemberRequestDTO(groupDescription2, personID);

        //Assert
        assertNotEquals(addMemberRequestDTO1.hashCode(), addMemberRequestDTO2.hashCode());
    }

    @DisplayName("Override Hash Code - Equals")
    @Test
    void testHashCodeEquals() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO1 = new AddMemberRequestDTO(groupDescription, personEmail);
        AddMemberRequestDTO addMemberRequestDTO2 = new AddMemberRequestDTO(groupDescription, personEmail);

        //Assert
        assertEquals(addMemberRequestDTO1.hashCode(), addMemberRequestDTO2.hashCode());
    }
}