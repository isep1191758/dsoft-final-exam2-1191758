package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Name;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AddMembersRequestInfoDTOTest {
    List<String> membersList;
    AddMembersRequestInfoDTO addMembersRequestInfoDTO;

    @BeforeEach
    private void setUp() {
        membersList = new ArrayList<>();
        membersList.add("mrhyde@gmail.com");
        addMembersRequestInfoDTO = new AddMembersRequestInfoDTO(membersList);
    }

    @Test
    void getMembers() {
        //Act
        List<String> result = addMembersRequestInfoDTO.getMembers();

        //Assert
        assertTrue(result.contains("mrhyde@gmail.com"));
    }

    @Test
    void setMembers() {
        //Arrange
        List<String> newMembersList = new ArrayList<>();

        newMembersList.add("drjekyll@gmail.com");

        addMembersRequestInfoDTO.setMembers(newMembersList);

        //Act
        List<String> result = addMembersRequestInfoDTO.getMembers();

        //Assert
        assertTrue(result.contains("drjekyll@gmail.com"));
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        // Assert
        assertEquals(addMembersRequestInfoDTO, addMembersRequestInfoDTO);
    }

    @DisplayName("equals() - Different Object Same attributes")
    @Test
    void equalsDifferentObjectSameAttributes() {
        //Arrange
        AddMembersRequestInfoDTO addMembersRequestInfoDTOOther = new AddMembersRequestInfoDTO(membersList);

        //Act
        boolean result = addMembersRequestInfoDTO.equals(addMembersRequestInfoDTOOther);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentClassObject() {
        //Arrange
        Name jose = new Name("José");

        //Act
        boolean result = addMembersRequestInfoDTO.equals(jose);

        // Assert
        assertFalse(result);
    }

    @DisplayName("hashcode() - Same attributes Object")
    @Test
    void hashcodeSameAttributesObject() {
        //Arrange
        AddMembersRequestInfoDTO addMembersRequestInfoDTOOther = new AddMembersRequestInfoDTO(membersList);

        // Assert
        assertEquals(addMembersRequestInfoDTO.hashCode(), addMembersRequestInfoDTOOther.hashCode());
    }

    @DisplayName("hashcode() - Exact value")
    @Test
    void hashcodeExactValue() {
        //Arrange
        int expected = 95326694;
        int result = addMembersRequestInfoDTO.hashCode();

        // Assert
        assertEquals(expected, result);
    }
}