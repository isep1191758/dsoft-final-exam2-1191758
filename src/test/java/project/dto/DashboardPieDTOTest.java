package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Name;

import static org.junit.jupiter.api.Assertions.*;

class DashboardPieDTOTest {
    DashboardPieDTO dashboardPieDTO;

    @BeforeEach
    private void setUp() {
        dashboardPieDTO = new DashboardPieDTO("category", "Category", 10);
    }

    @Test
    void getId() {
        //Act
        String result = dashboardPieDTO.getId();

        //Assert
        assertEquals("category", result);
    }

    @Test
    void setId() {
        //Arrange
        dashboardPieDTO.setId("categories");

        //Act
        String result = dashboardPieDTO.getId();

        //Assert
        assertEquals("categories", result);
    }

    @Test
    void getLabel() {
        //Act
        String result = dashboardPieDTO.getLabel();

        //Assert
        assertEquals("Category", result);
    }

    @Test
    void setLabel() {
        //Arrange
        dashboardPieDTO.setLabel("Categories");

        //Act
        String result = dashboardPieDTO.getLabel();

        //Assert
        assertEquals("Categories", result);
    }

    @Test
    void getValue() {
        //Act
        int result = dashboardPieDTO.getValue();

        //Assert
        assertEquals(10, result);
    }

    @Test
    void setValue() {
        //Arrange
        dashboardPieDTO.setValue(20);

        //Act
        int result = dashboardPieDTO.getValue();

        //Assert
        assertEquals(20, result);
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        // Assert
        assertEquals(dashboardPieDTO, dashboardPieDTO);
    }

    @DisplayName("equals() - Different Object Same attributes")
    @Test
    void equalsDifferentObjectSameAttributes() {
        //Arrange
        DashboardPieDTO dashboardPieDTOOther = new DashboardPieDTO("category", "Category", 10);

        //Act
        boolean result = dashboardPieDTO.equals(dashboardPieDTOOther);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentClassObject() {
        //Arrange
        Name jose = new Name("José");

        //Act
        boolean result = dashboardPieDTO.equals(jose);

        // Assert
        assertFalse(result);
    }

    @DisplayName("hashcode() - Same attributes Object")
    @Test
    void hashcodeSameAttributesObject() {
        //Arrange
        DashboardPieDTO dashboardPieDTOOther = new DashboardPieDTO("category", "Category", 10);

        // Assert
        assertEquals(dashboardPieDTO.hashCode(), dashboardPieDTOOther.hashCode());
    }

    @DisplayName("hashcode() - Exact value")
    @Test
    void hashcodeExactValue() {
        //Arrange
        int expected = 50511133;
        int result = dashboardPieDTO.hashCode();

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        String expected = "{id:'category', label:'Category', value:10}";

        //Act
        String result = dashboardPieDTO.toString();

        //Assert
        assertEquals(expected, result);
    }
}