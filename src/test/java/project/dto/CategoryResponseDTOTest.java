package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.AddCategoryResponseAssembler;
import project.model.shared.Designation;
import project.model.shared.Designation1;
import project.model.shared.Name;

import static org.junit.jupiter.api.Assertions.*;

class CategoryResponseDTOTest {

    CategoryResponseDTO categoryResponseDTO;
    Designation designation;
    Designation1 designation1;
    Designation designation2;
    Designation1 designation3;
    CategoryDTO categoryDTO;
    CategoryDTO categoryDTO2;

    @BeforeEach
    void setUp() {

        designation = new Designation("Sports");
        designation1 = new Designation1("Hooks");
        designation = new Designation("Materials");
        designation1 = new Designation1("Hands");
        categoryDTO = new CategoryDTO();
        categoryDTO2 = new CategoryDTO();
        categoryDTO.setDesignation(designation);
        categoryDTO.setDesignation1(designation1);
        categoryDTO2.setDesignation(designation2);
        categoryDTO2.setDesignation1(designation3);
        categoryResponseDTO = AddCategoryResponseAssembler.mapToCategoryResponseDTO(categoryDTO);
    }

    @Test
    void setDesignation() {
        categoryResponseDTO.setDesignation(categoryDTO.getDesignation().getDesignation());
    }

    @Test
    void getDesignation() {
        String result = categoryResponseDTO.getDesignation();
    }

    @DisplayName("categoryResponseDTO - Equals Override is True")
    @Test
    void categoryResponseDTOEqualsTrue() {
        //Arrange
        CategoryResponseDTO categoryResponseDTO1 = new CategoryResponseDTO(categoryDTO.getDesignation().getDesignation(), categoryDTO.getDesignation1().getDesignation1());
        CategoryResponseDTO categoryResponseDTO2 = new CategoryResponseDTO(categoryDTO.getDesignation().getDesignation(), categoryDTO.getDesignation1().getDesignation1());
        //Act
        boolean result = categoryResponseDTO1.equals(categoryResponseDTO2);
        //Assert
        assertTrue(result);
    }

    @DisplayName("categoryResponseDTO - Same Object")
    @Test
    void categoryResponseDTOEqualsSameObject() {
        //Arrange
        CategoryResponseDTO categoryResponseDTO = new CategoryResponseDTO(categoryDTO.getDesignation().getDesignation(), categoryDTO.getDesignation1().getDesignation1());
        //Act
        boolean result = categoryResponseDTO.equals(categoryResponseDTO);
        //Assert
        assertTrue(result);
    }

    @DisplayName("categoryResponseDTO - Different Objects")
    @Test
    void categoryResponseDTODifferentObjects() {
        //Arrange
        CategoryResponseDTO categoryResponseDTO1 = new CategoryResponseDTO(categoryDTO.getDesignation().getDesignation(), categoryDTO.getDesignation1().getDesignation1());
        CategoryResponseDTO categoryResponseDTO2 = new CategoryResponseDTO("Leopard Fishing", "Leopard");

        //Act
        boolean result = categoryResponseDTO1.equals(categoryResponseDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("categoryResponseDTO - Different Type of Objects")
    @Test
    void categoryResponseDTODifferentTypeObjects() {
        //Arrange
        CategoryResponseDTO categoryResponseDTO1 = new CategoryResponseDTO(categoryDTO.getDesignation().getDesignation(), categoryDTO.getDesignation1().getDesignation1());
        Name name = new Name("Leopard Fishing");

        //Act //Assert
        assertFalse(categoryResponseDTO1.equals(name));
    }

    @DisplayName("categoryResponseDTO - Different Type of Objects")
    @Test
    void categoryResponseDTONullObjects() {
        //Arrange
        CategoryResponseDTO categoryResponseDTO1 = new CategoryResponseDTO(categoryDTO.getDesignation().getDesignation(), categoryDTO.getDesignation1().getDesignation1());
        CategoryResponseDTO categoryResponseDTO2 = null;

        //Act
        boolean result = categoryResponseDTO1.equals(categoryResponseDTO2);

        //Assert
        assertFalse(result);
    }

    @DisplayName("hashCode() - Happy Path")
    @Test
    void categoryResponseDTOHashcode() {
        //Arrange
        int expected = -1609867221;

        //Act
        int result = categoryResponseDTO.hashCode();

        //Assert
        assertEquals(expected, result);
    }
}