package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.shared.TransactionDate;
import project.model.shared.TransactionType;

import java.time.LocalDateTime;
import java.time.Month;

class AddTransactionResponseDTOTest {
    AddTransactionResponseDTO addTransactionResponseDTO;
    Double amount;
    String description;
    TransactionDate date;
    String category;
    String debit;
    String credit;
    TransactionType type;

    @BeforeEach
    void setUp() {
        // Arrange
        amount = 20.0;
        description = "Lunch";
        date = new TransactionDate(LocalDateTime.now());
        category = "Fun time";
        debit = "Pocket";
        credit = "Restaurante Pinheiro";
        type = TransactionType.DEBIT;

        addTransactionResponseDTO = new AddTransactionResponseDTO(amount, description, date, category, debit, credit, type);
    }

    @Test
    void getAmount() {
        Double result = addTransactionResponseDTO.getAmount();
    }

    @Test
    void setAmount() {
        Double amount2 = 10.0;
        addTransactionResponseDTO.setAmount(amount2);
    }

    @Test
    void getDescription() {
        String result = addTransactionResponseDTO.getDescription();
    }

    @Test
    void setDescription() {
        String description2 = "toll";
        addTransactionResponseDTO.setDescription(description2);
    }

    @Test
    void getDate() {
        TransactionDate result = addTransactionResponseDTO.getDate();
    }

    @Test
    void setDate() {
        TransactionDate date2 = new TransactionDate(LocalDateTime.of(2020, Month.APRIL, 1, 0, 0, 0));
        addTransactionResponseDTO.setDate(date2);
    }

    @Test
    void getCategory() {
        String result = addTransactionResponseDTO.getCategory();
    }

    @Test
    void setCategory() {
        String category2 = "Market";
        addTransactionResponseDTO.setCategory(category2);
    }

    @Test
    void getDebit() {
        String result = addTransactionResponseDTO.getDebit();
    }

    @Test
    void setDebit() {
        String debit2 = "wallet";
        addTransactionResponseDTO.setDebit(debit2);
    }

    @Test
    void getCredit() {
        String result = addTransactionResponseDTO.getCredit();
    }

    @Test
    void setCredit() {
        String credit2 = "Wash car";
        addTransactionResponseDTO.setCredit(credit2);
    }

    @Test
    void getType() {
        TransactionType result = addTransactionResponseDTO.getType();
    }

    @Test
    void setType() {
        TransactionType type2 = TransactionType.CREDIT;
        addTransactionResponseDTO.setType(type2);
    }
}