package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.IsSiblingRequestDTOAssembler;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class IsSiblingRequestDTOTest {
    PersonID personID1;
    PersonID personID2;
    IsSiblingRequestDTO isSiblingRequestDTO;
    String personID1A;
    String personID2A;

    @BeforeEach
    void setUp() {

        Email personEmail1 = new Email("duarte@stcp.com");
        Email personEmail2 = new Email("marta@stcp.com");

        personID1 = new PersonID(personEmail1);
        personID2 = new PersonID(personEmail2);

        personID1A = "duarte@stcp.com";
        personID2A = "marta@stcp.com";

        isSiblingRequestDTO = IsSiblingRequestDTOAssembler.mapToDTO(personID1A, personID2A);
    }

    @Test
    void getPersonID1() {
        isSiblingRequestDTO.getPersonID1();
    }

    @Test
    void getPersonID2() {
        isSiblingRequestDTO.getPersonID2();
    }

    @Test
    void setPersonID1() {
        isSiblingRequestDTO.setPersonID1(isSiblingRequestDTO.getPersonID1());
    }

    @Test
    void setPersonID2() {
        isSiblingRequestDTO.setPersonID2(isSiblingRequestDTO.getPersonID2());
    }

    @Test
    void testEqualsHappyPath() {
        assertEquals(isSiblingRequestDTO.getPersonID1(), personID1);
        assertEquals(isSiblingRequestDTO.getPersonID2(), personID2);
    }

    @Test
    void testEqualsNullObjects() {
        assertNotEquals(isSiblingRequestDTO.getPersonID1(), null);
    }

    @Test
    void testEqualsSameObject() {
        // Act
        boolean result = isSiblingRequestDTO.equals(isSiblingRequestDTO);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObject() {
        // Act
        boolean result = isSiblingRequestDTO.equals(personID2A);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObjects() {
        assertNotEquals(isSiblingRequestDTO.getPersonID1(), personID2A);
    }

    @Test
    void testHashCode() {
        IsSiblingRequestDTO expected = new IsSiblingRequestDTO(personID1A, personID2A);
        int result = isSiblingRequestDTO.hashCode();
        int expectedHash = expected.hashCode();

        assertEquals(result, expectedHash);
    }

    @Test
    void testHashCodeExact() {
        int expectedHash = -1932909975;

        int result = isSiblingRequestDTO.hashCode();

        assertEquals(result, expectedHash);
    }
}