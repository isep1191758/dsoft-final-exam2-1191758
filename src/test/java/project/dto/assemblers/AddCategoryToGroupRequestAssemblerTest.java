package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.AddCategoryToGroupRequestDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AddCategoryToGroupRequestAssemblerTest {

    String designation;
    String designation1;
    String groupDescription;
    String responsibleEmail;

    @BeforeEach
    void setUp() {
        designation = "Diamantes";
        designation = "Jóias";
        groupDescription = "Família Dos Santos";
        responsibleEmail = "isabel@angola.com";
    }

    @Test
    void AddCategoryRequestAssembler() {
        //Act
        AddCategoryToGroupRequestDTO expected = AddCategoryToGroupRequestAssembler.addCategoryMapToDTO(designation, designation1, groupDescription, responsibleEmail);
        AddCategoryToGroupRequestDTO result = AddCategoryToGroupRequestAssembler.addCategoryMapToDTO(designation, designation1, groupDescription, responsibleEmail);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    void AddCategoryRequestAssemblerError() {
        assertThrows(AssertionError.class, AddCategoryToGroupRequestAssembler::new);
    }
}