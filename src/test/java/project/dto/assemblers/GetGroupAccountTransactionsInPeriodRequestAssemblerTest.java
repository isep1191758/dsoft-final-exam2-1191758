package project.dto.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.GetGroupAccountTransactionsInPeriodRequestDTO;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GetGroupAccountTransactionsInPeriodRequestAssemblerTest {

    @DisplayName("GetGroupAccountTransactionsInPeriodRequestAssemblerTest - happy path")
    @Test
    void mapToDTO() {
        //arrange

        Email email = new Email("margarida@gmail.com");
        PersonID personID = new PersonID(email);
        Description groupDescription = new Description("Dance Group");
        GroupID groupID = new GroupID(groupDescription);
        TransactionDate initialDate = new TransactionDate(LocalDateTime.of(1970, Month.MAY, 5, 1, 1, 1));
        TransactionDate finalDate = new TransactionDate(LocalDateTime.of(1980, Month.APRIL, 5, 1, 1, 1));
        Denomination denomination = new Denomination("Hats");
        AccountID accountID = new AccountID(denomination, groupID);
        String inicialDateTrans = initialDate.toString();
        String finalDateTrans = finalDate.toString();

        GetGroupAccountTransactionsInPeriodRequestDTO expected = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID, accountID, initialDate, finalDate);
        //Act
        GetGroupAccountTransactionsInPeriodRequestDTO result = GetGroupAccountTransactionsInPeriodRequestAssembler.mapToDTO("margarida@gmail.com", "Dance Group", "Hats", inicialDateTrans, finalDateTrans);

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("GetGroupAccountTransactionsInPeriodRequestAssemblerTest - exception")
    @Test
    void GetGroupAccountTransactionsInPeriodRequestAssembler() throws Exception {

        assertThrows(AssertionError.class, () -> {
            GetGroupAccountTransactionsInPeriodRequestAssembler getGroupAccountTransactionsInPeriodRequestAssembler = new GetGroupAccountTransactionsInPeriodRequestAssembler();
        });
    }
}
