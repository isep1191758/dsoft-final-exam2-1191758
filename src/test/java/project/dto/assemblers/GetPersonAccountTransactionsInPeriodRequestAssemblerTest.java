package project.dto.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.GetPersonAccountTransactionsInPeriodRequestDTO;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GetPersonAccountTransactionsInPeriodRequestAssemblerTest {

    @DisplayName("GetPersonAccountTransactionsInPeriodRequestAssemblerTest - happy path")
    @Test
    void mapToDTO() {
        //arrange

        Email email = new Email("guilhermina@gmail.com");
        PersonID personID = new PersonID(email);
        TransactionDate initialDate = new TransactionDate(LocalDateTime.of(1970, Month.MAY, 5, 1, 1, 1));
        TransactionDate finalDate = new TransactionDate(LocalDateTime.of(1980, Month.APRIL, 5, 1, 1, 1));
        Denomination denomination = new Denomination("Computers");
        AccountID accountID = new AccountID(denomination, personID);
        String inicialDateTrans = initialDate.toString();
        String finalDateTrans = finalDate.toString();

        GetPersonAccountTransactionsInPeriodRequestDTO expected = new GetPersonAccountTransactionsInPeriodRequestDTO(personID, accountID, initialDate, finalDate);

        //Act
        GetPersonAccountTransactionsInPeriodRequestDTO result = GetPersonAccountTransactionsInPeriodRequestAssembler.mapToDTO("guilhermina@gmail.com", "Computers", inicialDateTrans, finalDateTrans);

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("GetPersonAccountTransactionsInPeriodRequestAssemblerTest - exception")
    @Test
    void GetPersonAccountTransactionsInPeriodRequestAssembler() throws Exception {

        assertThrows(AssertionError.class, () -> {
            GetPersonAccountTransactionsInPeriodRequestAssembler getPersonAccountTransactionsInPeriodRequestAssemblerTest = new GetPersonAccountTransactionsInPeriodRequestAssembler();
        });
    }
}

