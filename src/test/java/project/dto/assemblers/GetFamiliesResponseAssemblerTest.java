package project.dto.assemblers;

import org.junit.jupiter.api.Test;
import project.dto.GroupDTO;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertThrows;

class GetFamiliesResponseAssemblerTest {
    @Test
    void mapToDTO() {
        Description groupDescription = new Description("Switch Group");
        Email email = new Email("student@gmail.com");
        PersonID responsibleId = new PersonID(email);

        Group switchGroup = new Group(groupDescription, responsibleId);
        GroupDTO expected = new GroupDTO();

        GroupDTO groupDTO = GroupAssembler.mapToDTO(switchGroup);
    }

    @Test
    void GetFamiliesResponseAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            GetFamiliesResponseAssembler getFamiliesResponseAssembler = new GetFamiliesResponseAssembler();
        });
    }
}