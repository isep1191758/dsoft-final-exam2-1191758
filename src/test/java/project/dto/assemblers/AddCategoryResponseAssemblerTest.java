package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.CategoryDTO;
import project.dto.CategoryResponseDTO;
import project.model.shared.Designation;
import project.model.shared.Designation1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AddCategoryResponseAssemblerTest {

    CategoryDTO categoryDTO;
    CategoryResponseDTO categoryResponseDTO;
    Designation designation;
    Designation1 designation1;

    @BeforeEach
    void setUp() {
        designation = new Designation("Futebol");
        designation1 = new Designation1("Desportos");
        categoryDTO = new CategoryDTO();
        categoryDTO.setDesignation(designation);
        categoryResponseDTO = new CategoryResponseDTO(categoryDTO.getDesignation().getDesignation(), categoryDTO.getDesignation1().getDesignation1());
    }

    @Test
    void AddCategoryResponseAssembler() {
        //Act
        CategoryResponseDTO result = AddCategoryResponseAssembler.mapToCategoryResponseDTO(categoryDTO);
        CategoryResponseDTO expected = categoryResponseDTO;
        //Assert
        assertEquals(expected, result);
    }

    @Test
    void AddCategoryResponseAssemblerError() {
        assertThrows(AssertionError.class, AddCategoryResponseAssembler::new);
    }
}