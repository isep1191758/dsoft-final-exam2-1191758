package project.dto.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.CategoryDTO;
import project.model.shared.Category;
import project.model.shared.Designation;
import project.model.shared.Designation1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CategoryAssemblerTest {

    @DisplayName("CategoryAssembler - Happy Case")
    @Test
    void CategoryAssemblerHappyCase() {
        //Arrange
        Designation designation = new Designation("Tennis");
        Designation1 designation1 = new Designation1("Sports");
        Category category1 = new Category(designation, designation1);
        Category category2 = new Category(designation, designation1);
        //Act
        CategoryDTO categoryDTO1 = CategoryAssembler.mapToDTO(category1);
        CategoryDTO categoryDTO2 = CategoryAssembler.mapToDTO(category2);
        //Assert
        assertEquals(categoryDTO1.getDesignation1(), categoryDTO2.getDesignation1());
    }

    @Test
    void CategoryAssemblerError() {
        assertThrows(AssertionError.class, CategoryAssembler::new);
    }
}