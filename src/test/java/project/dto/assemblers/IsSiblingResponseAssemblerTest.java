package project.dto.assemblers;

import org.junit.jupiter.api.Test;
import project.dto.IsSiblingResponseDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IsSiblingResponseAssemblerTest {

    @Test
    void mapToDTOHappyPath() {
        //Arrange
        String isSibling = "Brothers";
        IsSiblingResponseDTO isSiblingResponseDTO = new IsSiblingResponseDTO(isSibling);

        //Act & Assert
        assertEquals(isSiblingResponseDTO, IsSiblingResponseAssembler.mapToDTO(isSibling));
    }

    @Test
    void IsSiblingResponseAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            IsSiblingResponseAssembler isSiblingResponseAssembler = new IsSiblingResponseAssembler();
        });
    }
}