package project.dto.assemblers;

import org.junit.jupiter.api.Test;
import project.dto.AddMembersRequestDTO;
import project.model.shared.Email;
import project.model.shared.PersonID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

class AddMembersRequestAssemblerTest {
    @Test
    void mapToDTO() {
        //Arrange
        List<String> membersList = new ArrayList<>();
        membersList.add("sicko@gmail.com");
        PersonID personID = new PersonID(new Email("sicko@gmail.com"));
        AddMembersRequestAssembler addMembersRequestAssembler = new AddMembersRequestAssembler();

        //Act
        AddMembersRequestDTO result = AddMembersRequestAssembler.mapToDTO("New Members Group", membersList);

        //Assert
        assertTrue(result.getPersonIDs().contains(personID));
    }
}