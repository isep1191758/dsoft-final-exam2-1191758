package project.dto.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.GroupDTO;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class GroupAssemblerTest {

    @DisplayName("Group Assembler - mapToDTO")
    @Test
    void GroupAssembler_MapToDTO() {
        //Assert
        Group group = new Group(new Description("TestGroup"), new PersonID(new Email("tarcisio@gmail.com")));

        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setResponsibles(group.getResponsibles());
        groupDTO.setGroupID(group.getID());
        groupDTO.setMembers(group.getMembers());
        groupDTO.setCategories(group.getCategories());
        groupDTO.setCreationDate(group.getCreationDate());

        GroupDTO result = GroupAssembler.mapToDTO(group);
        assertEquals(groupDTO, result);
    }

    @Test
    void GroupAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            GroupAssembler groupAssembler = new GroupAssembler();
        });
    }
}