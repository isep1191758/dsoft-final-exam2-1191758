package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.AddMembersRequestAssembler;
import project.model.group.Group;
import project.model.shared.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class AddMembersRequestDTOTest {
    // GROUPS
    PersonID personID;
    Description groupDescription;
    Group drJekMrHyGroup;
    List<String> membersList;
    AddMembersRequestDTO addMembersRequestDTO;

    @BeforeEach
    private void setUp() {
        personID = new PersonID(new Email("mrhyde@gmail.com"));
        membersList = new ArrayList<>();
        membersList.add("mrhyde@gmail.com");
        groupDescription = new Description("Dr Jekyll and Mr Hyde");
        drJekMrHyGroup = new Group(groupDescription, personID);
        addMembersRequestDTO = AddMembersRequestAssembler.mapToDTO("Dr Jekyll and Mr Hyde", membersList);
    }

    @Test
    void getGroupID() {
        //Arrange
        GroupID expected = drJekMrHyGroup.getID();
        //Act
        GroupID result = addMembersRequestDTO.getGroupID();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setGroupID() {
        //Arrange
        addMembersRequestDTO.setGroupID(new GroupID(new Description("Las Bolotas")));

        //Act
        GroupID result = addMembersRequestDTO.getGroupID();

        //Assert
        assertEquals("Las Bolotas", result.toString());
    }

    @Test
    void getPersonIDs() {
        //Arrange
        //Act
        Set<PersonID> result = addMembersRequestDTO.getPersonIDs();

        //Assert
        assertTrue(result.contains(personID));
    }

    @Test
    void setPersonIDs() {
        //Arrange
        PersonID newMember = new PersonID(new Email("drjekyll@gmail.com"));
        Set<PersonID> newMembersList = new HashSet<>();
        newMembersList.add(newMember);
        addMembersRequestDTO.setPersonIDs(newMembersList);
        //Act
        Set<PersonID> result = addMembersRequestDTO.getPersonIDs();

        //Assert
        assertTrue(result.contains(newMember));
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        // Assert
        assertEquals(addMembersRequestDTO, addMembersRequestDTO);
    }

    @DisplayName("equals() - Different Object Same attributes")
    @Test
    void equalsDifferentObjectSameAttributes() {
        //Arrange
        AddMembersRequestDTO addMembersRequestDTOOther = AddMembersRequestAssembler.mapToDTO("Dr Jekyll and Mr Hyde", membersList);

        //Act
        boolean result = addMembersRequestDTO.equals(addMembersRequestDTOOther);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentClassObject() {
        //Arrange
        Name jose = new Name("José");

        //Act
        boolean result = addMembersRequestDTO.equals(jose);

        // Assert
        assertFalse(result);
    }

    @DisplayName("hashcode() - Same attributes Object")
    @Test
    void hashcodeSameAttributesObject() {
        //Arrange
        AddMembersRequestDTO addMembersRequestDTOOther = AddMembersRequestAssembler.mapToDTO("Dr Jekyll and Mr Hyde", membersList);

        // Assert
        assertEquals(addMembersRequestDTO.hashCode(), addMembersRequestDTOOther.hashCode());
    }

    @DisplayName("hashcode() - Exact value")
    @Test
    void hashcodeExactValue() {
        //Arrange
        int expected = -1951612662;
        int result = addMembersRequestDTO.hashCode();

        // Assert
        assertEquals(expected, result);
    }
}