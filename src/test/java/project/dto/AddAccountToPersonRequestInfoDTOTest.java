package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddAccountToPersonRequestInfoDTOTest {
    String denomination;
    String description;
    String personEmail;
    AddAccountToPersonRequestInfoDTO addAccountToPersonRequestInfoDTO;

    @BeforeEach
    void setUp() {
        denomination = "School Account";
        description = "Dance School";
        personEmail = "lia@gmail.com";

        addAccountToPersonRequestInfoDTO = new AddAccountToPersonRequestInfoDTO(denomination, description);
    }

    @Test
    void getDenomination() {
        addAccountToPersonRequestInfoDTO.getDenomination();
    }

    @Test
    void setDenomination() {
        addAccountToPersonRequestInfoDTO.setDenomination(addAccountToPersonRequestInfoDTO.getDenomination());
    }

    @Test
    void getDescription() {
        addAccountToPersonRequestInfoDTO.getDescription();
    }

    @Test
    void setDescription() {
        addAccountToPersonRequestInfoDTO.setDescription(addAccountToPersonRequestInfoDTO.getDescription());
    }

    @Test
    void testEqualsHappyPath() {
        String denominationA = "School Account";
        String descriptionA = "Dance School";
        String personEmailA = "lia@gmail.com";

        AddAccountToPersonRequestInfoDTO expected = new AddAccountToPersonRequestInfoDTO(denominationA, descriptionA);

        assertTrue(addAccountToPersonRequestInfoDTO.equals(expected));
    }

    @Test
    void testEqualsSameObjectHappyPath() {
        assertTrue(addAccountToPersonRequestInfoDTO.equals(addAccountToPersonRequestInfoDTO));
    }

    @Test
    void testEqualsDifferentDescription() {
        String denominationA = "School Account";
        String descriptionA = "Danceteria School";
        String personEmailA = "lia@gmail.com";

        AddAccountToPersonRequestInfoDTO expected = new AddAccountToPersonRequestInfoDTO(denominationA, descriptionA);

        assertFalse(addAccountToPersonRequestInfoDTO.equals(expected));
    }

    @Test
    void testEqualsDifferentDenomination() {
        String denominationA = "Schools Account";
        String descriptionA = "Dance School";
        String personEmailA = "lia@gmail.com";

        AddAccountToPersonRequestInfoDTO expected = new AddAccountToPersonRequestInfoDTO(denominationA, descriptionA);

        assertFalse(addAccountToPersonRequestInfoDTO.equals(expected));
    }

    @Test
    void testEqualsDifferentObject() {

        String denominationA = "School Account";

        assertFalse(addAccountToPersonRequestInfoDTO.equals(denominationA));
    }

    @Test
    void testEqualsNullObject() {

        assertFalse(addAccountToPersonRequestInfoDTO.equals(null));
    }


    @Test
    void testHashCode() {
        AddAccountToPersonRequestInfoDTO expect = new AddAccountToPersonRequestInfoDTO(denomination, description);

        int expectHash = expect.hashCode();
        int resultHash = addAccountToPersonRequestInfoDTO.hashCode();

        assertEquals(expectHash, resultHash);
    }

    @Test
    void testHashCodeHappyPath() {
        AddAccountToPersonRequestInfoDTO addAccountToPersonRequestInfoDTO = new AddAccountToPersonRequestInfoDTO(denomination, description);

        int expectHash = 1360434977;

        int resultHash = addAccountToPersonRequestInfoDTO.hashCode();

        assertEquals(expectHash, resultHash);
    }
}