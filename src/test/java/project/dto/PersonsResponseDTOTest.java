package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.PersonAssembler;
import project.dto.assemblers.PersonsResponseDTOAssembler;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class PersonsResponseDTOTest {

    // Arrange
    //PersonsBirthDate
    LocalDateTime personBirthDate;

    //Mother
    Email motherEmail;
    PersonID motherID;
    Person mother;

    //Father
    Email fatherEmail;
    PersonID fatherID;
    Person father;

    //Brother
    Email brotherEmail;
    PersonID brotherID;
    Person brother;

    //Valentina
    Email valentinaEmail;
    PersonID valentinaID;
    Person valentina;
    PersonDTO valentinaDTO;
    PersonsResponseDTO valentinaResponseDTO;

    //Amanda
    Email amandaEmail;
    Person amanda;
    PersonDTO amandaDTO;

    @BeforeEach
    void setUp() {
        // Arrange
        //PersonsBirthDate
        personBirthDate = LocalDateTime.of(2006, Month.DECEMBER, 12, 18, 30, 6);

        //Mother
        motherEmail = new Email("cacilda@gmail.com");
        motherID = new PersonID(motherEmail);
        mother = new Person(new Name("Cacilda Maria"), new Address("Guimarães"), new Date(personBirthDate), motherEmail, null, null);

        //Father
        fatherEmail = new Email("zedesousa@gmail.com");
        fatherID = new PersonID(fatherEmail);
        father = new Person(new Name("Jose de Sousa"), new Address("Guimarães"), new Date(personBirthDate), fatherEmail, null, null);

        //Brother
        brotherEmail = new Email("ruidesousa@gmail.com");
        brotherID = new PersonID(brotherEmail);
        brother = new Person(new Name("Rui de Sousa"), new Address("Guimarães"), new Date(personBirthDate), brotherEmail, motherID, fatherID);

        //Valentina
        valentinaEmail = new Email("valentina@gmail.com");
        valentinaID = new PersonID(valentinaEmail);
        valentina = new Person(new Name("Valentina de Sousa"), new Address("Guimarães"), new Date(personBirthDate), valentinaEmail, motherID, fatherID);
        valentinaDTO = PersonAssembler.mapToDTO(valentina);
        valentinaResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);

        amandaEmail = new Email("amanda@gmail.com");
        amanda = new Person(new Name("Amanda de Sousa"), new Address("Guimarães"), new Date(personBirthDate), amandaEmail, null, null);
        amanda.addSibling(brotherID);
    }

    @Test
    void getPersonID() {
        PersonID result = new PersonID(new Email(valentinaResponseDTO.getPersonID()));

        assertEquals(result, valentinaID);
    }

    @Test
    void setPersonID() {
        valentinaResponseDTO.setPersonID("amanda@gmail.com");
    }

    @Test
    void getName() {
        String result = valentinaResponseDTO.getName();

        assertEquals("Valentina de Sousa", valentinaResponseDTO.getName());
    }

    @Test
    void setName() {
        valentinaResponseDTO.setName("Valentina");
    }

    @Test
    void getBirthdate() {
        String date = valentinaResponseDTO.getBirthdate();

        assertEquals("2006-12-12", valentinaResponseDTO.getBirthdate());
    }

    @Test
    void setBirthdate() {
        personBirthDate = LocalDateTime.of(1987, Month.MAY, 12, 18, 30, 6);
        valentinaResponseDTO.setBirthdate(personBirthDate.toString());
    }

    @Test
    void getBirthplace() {
        String place = valentinaResponseDTO.getBirthplace();

        assertEquals("Guimarães", valentinaResponseDTO.getBirthplace());
    }

    @Test
    void setBirthplace() {
        valentinaResponseDTO.setBirthplace("Porto");
    }

    @Test
    void getMother() {
        String mom = valentinaResponseDTO.getMother();

        assertEquals("cacilda@gmail.com", valentinaResponseDTO.getMother());
    }

    @Test
    void setMother() {
        valentinaResponseDTO.setMother("cacilda@gmail.com");
    }

    @Test
    void getFather() {
        String papa = valentinaResponseDTO.getFather();

        assertEquals("zedesousa@gmail.com", valentinaResponseDTO.getFather());
    }

    @Test
    void setFather() {
        valentinaResponseDTO.setFather("zedesousa@gmail.com");
    }

    @Test
    void getSiblings() {
        valentinaResponseDTO.setSiblings("marcolino@gmail.com");
        String bro = valentinaResponseDTO.getSiblings();

        assertEquals("marcolino@gmail.com", valentinaResponseDTO.getSiblings());
    }

    @Test
    void setSiblings() {
        valentinaResponseDTO.setSiblings("amanda@gmail.com");
    }

    @Test
    void getCategories() {
        valentinaResponseDTO.setCategories("Category");
        String cat = valentinaResponseDTO.getCategories();

        assertEquals("Category", valentinaResponseDTO.getCategories());
    }

    @Test
    void setCategories() {
        valentinaResponseDTO.setCategories("Dance Classes");
    }

    @Test
    void testHashCode() {
        //Arrange
        int expected = 1163327923;

        //Act
        int result = valentinaResponseDTO.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("Equals - Same Object")
    @Test
    void testEqualsSameObject() {
        //Assert
        assertEquals(valentinaResponseDTO, valentinaResponseDTO);
    }

    @DisplayName("Equals - Equal Object")
    @Test
    void testEqualsObject() {
        //Arrange//Act
        PersonsResponseDTO resultResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);

        //Assert
        assertEquals(resultResponseDTO, valentinaResponseDTO);
    }

    @DisplayName("Equals - Null Object")
    @Test
    void testEqualsNullObject() {
        //Assert
        assertFalse(valentinaResponseDTO.equals(null));
    }

    @DisplayName("Equals - Dif Object")
    @Test
    void testEqualsDifObject() {
        //Assert
        assertFalse(valentinaResponseDTO.equals(personBirthDate));
    }

    @DisplayName("Equals - Dif PersonID")
    @Test
    void testEqualsDifPersonID() {
        //Arrange//Act
        PersonsResponseDTO resultResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);
        resultResponseDTO.setPersonID("amanda@family");

        //Assert
        assertNotEquals(resultResponseDTO, valentinaResponseDTO);
    }

    @DisplayName("Equals - Dif name")
    @Test
    void testEqualsDifName() {
        //Arrange//Act
        PersonsResponseDTO resultResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);
        resultResponseDTO.setName("Amanda de Sousa");

        //Assert
        assertNotEquals(resultResponseDTO, valentinaResponseDTO);
    }


    @DisplayName("Equals - Dif Birthdate")
    @Test
    void testEqualsDifBirthdate() {
        //Arrange//Act
        PersonsResponseDTO resultResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);
        resultResponseDTO.setBirthdate("1988-07-28");

        //Assert
        assertNotEquals(resultResponseDTO, valentinaResponseDTO);
    }

    @DisplayName("Equals - Dif Birthplace")
    @Test
    void testEqualsDifBirthplace() {
        //Arrange//Act
        PersonsResponseDTO resultResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);
        resultResponseDTO.setBirthplace("V. N. de Famalicão");

        //Assert
        assertNotEquals(resultResponseDTO, valentinaResponseDTO);
    }

    @DisplayName("Equals - Dif Mother")
    @Test
    void testEqualsDifMother() {
        //Arrange//Act
        PersonsResponseDTO resultResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);
        resultResponseDTO.setMother("aurora@family.com");

        //Assert
        assertNotEquals(resultResponseDTO, valentinaResponseDTO);
    }

    @DisplayName("Equals - Dif Father")
    @Test
    void testEqualsDifFather() {
        //Arrange//Act
        PersonsResponseDTO resultResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);
        resultResponseDTO.setFather("amandio@family.com");

        //Assert
        assertNotEquals(resultResponseDTO, valentinaResponseDTO);
    }

    @DisplayName("Equals - Dif Sibling")
    @Test
    void testEqualsDifSibling() {
        //Arrange//Act
        PersonsResponseDTO resultResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);
        resultResponseDTO.setSiblings("andre@family.com");

        //Assert
        assertNotEquals(resultResponseDTO, valentinaResponseDTO);
    }

    @DisplayName("Equals - Dif Category")
    @Test
    void testEqualsDifCategory() {
        //Arrange//Act
        PersonsResponseDTO resultResponseDTO = PersonsResponseDTOAssembler.mapToDTO(valentinaDTO);
        resultResponseDTO.setCategories("Dances");

        //Assert
        assertNotEquals(resultResponseDTO, valentinaResponseDTO);
    }

}