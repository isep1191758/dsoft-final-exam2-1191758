package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.account.Account;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class GetGroupAccountTransactionsInPeriodRequestDTOTest {
    GetGroupAccountTransactionsInPeriodRequestDTO getGroupAccountTransactionsInPeriodRequestDTO;
    GetGroupAccountTransactionsInPeriodRequestDTO getGroupAccountTransactionsInPeriodRequestDTO2;
    Person mario;
    Person marta;
    Email marioEmail;
    Email martaEmail;
    Address marioBirthPlace;
    Address martaBirthPlace;
    Date marioBirthDate;
    Date martaBirthDate;
    Account account;
    Account account2;
    Group group;
    Group group2;
    PersonID personID;
    PersonID personID2;
    GroupID groupID;
    GroupID groupID2;
    Denomination denomination;
    Denomination denomination2;
    Description description;
    Description description2;
    AccountID accountID;
    AccountID accountID2;
    TransactionDate initialDate;
    TransactionDate endDate;
    TransactionDate initialDate2;
    TransactionDate endDate2;

    @BeforeEach
    void setUp() {
        // Arrange
        marioEmail = new Email("mario@family.com");
        marioBirthPlace = new Address("Porto");
        marioBirthDate = new Date(LocalDateTime.of(1990, Month.JANUARY, 1, 1, 1, 1));
        mario = new Person(new Name("Mario"), marioBirthPlace, marioBirthDate, marioEmail, null, null);
        personID = mario.getPersonID();
        group = new Group(new Description("Family"), mario.getPersonID());
        groupID = group.getID();
        denomination = new Denomination("Account Denomination");
        description = new Description("Account Description");
        account = new Account(denomination, description, mario.getPersonID());
        accountID = account.getAccountID();
        initialDate = new TransactionDate(LocalDateTime.of(2018, Month.JULY, 2, 19, 0, 34));
        endDate = new TransactionDate(LocalDateTime.of(2019, Month.JANUARY, 31, 8, 0, 59));

        getGroupAccountTransactionsInPeriodRequestDTO = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID, accountID, initialDate, endDate);

        martaEmail = new Email("marta@family.com");
        martaBirthPlace = new Address("Lisboa");
        martaBirthDate = new Date(LocalDateTime.of(1995, Month.JANUARY, 1, 2, 3, 4));
        marta = new Person(new Name("Marta"), martaBirthPlace, martaBirthDate, martaEmail, null, null);
        personID2 = marta.getPersonID();
        group2 = new Group(new Description("MartaFamily"), marta.getPersonID());
        groupID2 = group2.getID();
        denomination2 = new Denomination("Account2 Denomination");
        description2 = new Description("Account2 Description");
        account2 = new Account(denomination, description, marta.getPersonID());
        accountID2 = account2.getAccountID();
        initialDate2 = new TransactionDate(LocalDateTime.of(2018, Month.APRIL, 22, 15, 0, 0));
        endDate2 = new TransactionDate(LocalDateTime.of(2018, Month.DECEMBER, 31, 23, 59, 59));

        getGroupAccountTransactionsInPeriodRequestDTO2 = new GetGroupAccountTransactionsInPeriodRequestDTO(personID2, groupID2, accountID2, initialDate2, endDate2);
    }

    @DisplayName("GetTransactionsRequestDTO - Coverage")
    @Test
    void GetTransactionsRequestDTO_CoverageTests() {

        GetGroupAccountTransactionsInPeriodRequestDTO result = new GetGroupAccountTransactionsInPeriodRequestDTO(new PersonID(new Email("testemail@gmail.com")),
                new GroupID(new Description("Members")),
                new AccountID(new Denomination("Pocket"), groupID),
                new TransactionDate(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0)),
                new TransactionDate(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0)));
    }

    @DisplayName("Get PersonID")
    @Test
    void getPersonID() {
        //Act
        PersonID result = getGroupAccountTransactionsInPeriodRequestDTO.getPersonID();

        //Assert
        assertEquals(personID, result);
    }

    @DisplayName("Set PersonID")
    @Test
    void setPersonID() {
        getGroupAccountTransactionsInPeriodRequestDTO.setPersonID(personID);
    }

    @DisplayName("Get GroupID")
    @Test
    void getGroupID() {
        GroupID result = getGroupAccountTransactionsInPeriodRequestDTO.getGroupID();

        assertEquals(groupID, result);
    }

    @DisplayName("Set GroupID")
    @Test
    void setGroupID() {
        getGroupAccountTransactionsInPeriodRequestDTO.setGroupID(groupID);
    }

    @DisplayName("Get AccountID")
    @Test
    void getAccountID() {
        AccountID result = getGroupAccountTransactionsInPeriodRequestDTO.getAccountID();

        assertEquals(accountID, result);
    }

    @DisplayName("Set AccountID")
    @Test
    void setAccountID() {
        getGroupAccountTransactionsInPeriodRequestDTO.setAccountID(accountID);
    }

    @DisplayName("Get InitialDate")
    @Test
    void getInitialDate() {
        TransactionDate result = getGroupAccountTransactionsInPeriodRequestDTO.getInitialDate();

        assertEquals(initialDate, result);
    }

    @DisplayName("Set InitialDate")
    @Test
    void setInitialDate() {
        getGroupAccountTransactionsInPeriodRequestDTO.setInitialDate(initialDate);
    }

    @DisplayName("Get EndDate")
    @Test
    void getEndDate() {
        TransactionDate result = getGroupAccountTransactionsInPeriodRequestDTO.getEndDate();

        assertEquals(endDate, result);
    }

    @DisplayName("Set EndDate")
    @Test
    void setEndDate() {
        getGroupAccountTransactionsInPeriodRequestDTO.setEndDate(endDate);
    }

    @DisplayName("GetGroupAccountTransactionsInPeriodRequestDTOEqualsTrue - Testing Equals")
    @Test
    void TestEqualsTrue() {
        //Act
        GetGroupAccountTransactionsInPeriodRequestDTO expectedResult = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID, accountID, initialDate, endDate);
        GetGroupAccountTransactionsInPeriodRequestDTO result = getGroupAccountTransactionsInPeriodRequestDTO;
        //Assert
        assertEquals(expectedResult, result);
    }

    @DisplayName("GetGroupAccountTransactionsInPeriodRequestDTOEqualsTrue - Exact same object")
    @Test
    void TestEqualsExactSameObject() {
        //Act
        GetGroupAccountTransactionsInPeriodRequestDTO result = getGroupAccountTransactionsInPeriodRequestDTO;

        //Assert
        assertEquals(result, result);
    }

    @DisplayName("GetGroupAccountTransactionsInPeriodRequestDTOEqualsFalse - Testing False")
    @Test
    void TestEqualsFalse() {
        //Act
        GetGroupAccountTransactionsInPeriodRequestDTO expectedResult = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID, accountID, initialDate, endDate);
        GetGroupAccountTransactionsInPeriodRequestDTO result = getGroupAccountTransactionsInPeriodRequestDTO2;

        //Assert
        assertNotEquals(expectedResult, result);
    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        //Arrange
        GetGroupAccountTransactionsInPeriodRequestDTO getGroupAccountTransactionsInPeriodRequestDTO1 = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID, accountID, initialDate, endDate);
        int expectedResult = getGroupAccountTransactionsInPeriodRequestDTO.hashCode();
        //Act
        int result = getGroupAccountTransactionsInPeriodRequestDTO1.hashCode();
        //Assert
        assertEquals(expectedResult, result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentObjects() {
        //Arrange
        Name name = new Name("Zézinho");

        //Act
        boolean result = getGroupAccountTransactionsInPeriodRequestDTO.equals(name);

        //Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Different Object Same attributes")
    @Test
    void equalsDifferentObjectSameAttributes() {
        //Arrange
        GetGroupAccountTransactionsInPeriodRequestDTO getGroupAccountTransactionsInPeriodRequestDTO3;
        getGroupAccountTransactionsInPeriodRequestDTO3 = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID, accountID, initialDate, endDate);

        //Act
        boolean result = getGroupAccountTransactionsInPeriodRequestDTO.equals(getGroupAccountTransactionsInPeriodRequestDTO3);

        // Assert
        assertTrue(result);
    }

    @DisplayName("sameValueAs() - Transaction to compare is null")
    @Test
    void sameValueAsAmountIsNull() {
        //Arrange
        GetGroupAccountTransactionsInPeriodRequestDTO getGroupAccountTransactionsInPeriodRequestDTO1 = null;

        //Act
        boolean result = getGroupAccountTransactionsInPeriodRequestDTO.equals(getGroupAccountTransactionsInPeriodRequestDTO1);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Hash Code Override test - Not Equals")
    @Test
    void hashCodeTestNotEquals() {
        //Arrange
        GetGroupAccountTransactionsInPeriodRequestDTO getGroupAccountTransactionsInPeriodRequestDTO2;
        getGroupAccountTransactionsInPeriodRequestDTO2 = new GetGroupAccountTransactionsInPeriodRequestDTO(personID2, groupID2, accountID2, initialDate, endDate);

        int expectedResult = getGroupAccountTransactionsInPeriodRequestDTO.hashCode();

        //Act
        int result = getGroupAccountTransactionsInPeriodRequestDTO2.hashCode();

        //Assert
        assertNotEquals(expectedResult, result);
    }

    @Test
    void TestEqualsNullObjects() {
        assertNotNull(getGroupAccountTransactionsInPeriodRequestDTO);
    }

    @Test
    void TestEqualsDifferentPersonID() {
        //Arrange
        GetGroupAccountTransactionsInPeriodRequestDTO expectedResult = new GetGroupAccountTransactionsInPeriodRequestDTO(personID2, groupID, accountID, initialDate, endDate);
        GetGroupAccountTransactionsInPeriodRequestDTO result = getGroupAccountTransactionsInPeriodRequestDTO;

        //Assert
        assertNotEquals(expectedResult, result);
    }

    @Test
    void TestEqualsDifferentGroupID() {
        //Arrange
        GetGroupAccountTransactionsInPeriodRequestDTO expectedResult = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID2, accountID, initialDate, endDate);
        GetGroupAccountTransactionsInPeriodRequestDTO result = getGroupAccountTransactionsInPeriodRequestDTO;

        //Assert
        assertNotEquals(expectedResult, result);
    }

    @Test
    void TestEqualsDifferentAccountID() {
        //Arrange
        GetGroupAccountTransactionsInPeriodRequestDTO expectedResult = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID, accountID2, initialDate, endDate);
        GetGroupAccountTransactionsInPeriodRequestDTO aresult = getGroupAccountTransactionsInPeriodRequestDTO;

        boolean result = expectedResult.equals(aresult);

        //Assert
        assertFalse(result);
    }

    @Test
    void TestEqualsDifferentInitialDate() {
        //Arrange
        GetGroupAccountTransactionsInPeriodRequestDTO expectedResult = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID, accountID, initialDate2, endDate);
        GetGroupAccountTransactionsInPeriodRequestDTO aresult = getGroupAccountTransactionsInPeriodRequestDTO;

        boolean result = expectedResult.equals(aresult);

        //Assert
        assertFalse(result);
    }

    @Test
    void TestEqualsDifferentEndDate() {
        //Arrange
        GetGroupAccountTransactionsInPeriodRequestDTO expectedResult = new GetGroupAccountTransactionsInPeriodRequestDTO(personID, groupID, accountID, initialDate, endDate2);
        GetGroupAccountTransactionsInPeriodRequestDTO aresult = getGroupAccountTransactionsInPeriodRequestDTO;

        boolean result = expectedResult.equals(aresult);

        //Assert
        assertFalse(result);
    }
}