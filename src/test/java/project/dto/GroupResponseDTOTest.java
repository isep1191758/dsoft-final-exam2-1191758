package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.GroupResponseAssembler;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GroupResponseDTOTest {
    private final String groupDescription = "Groupsie";
    List<String> responsibles = new ArrayList<>();
    List<String> members = new ArrayList<>();
    List<String> categories = new ArrayList<>();
    GroupResponseDTO groupResponseDTO;

    @BeforeEach
    private void setUp() {
        responsibles.add("seafood@gmail.com");
        members.add("seafood@gmail.com");
        categories.add("Megatory");
        groupResponseDTO = GroupResponseAssembler.mapToDTO(groupDescription, responsibles, members, categories);
    }

    @Test
    void GroupDTOTest_Constructor() {
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        new GroupResponseDTO(groupDescription, responsibles, members, categories);
    }

    @Test
    void getGroupDescription() {
        String result = groupResponseDTO.getGroupDescription();

        assertEquals("Groupsie", result);
    }

    @Test
    void setGroupDescription() {
        groupResponseDTO.setGroupDescription("Group2");
    }

    @Test
    void getResponsibles() {
        //Act
        List<String> result = groupResponseDTO.getResponsibles();

        //Assert
        assertTrue(result.contains("seafood@gmail.com"));
    }

    @Test
    void setResponsibles() {
        //Arrange
        List<String> newResponsibles = new ArrayList<>();
        newResponsibles.add("crab@gmail.com");
        groupResponseDTO.setResponsibles(newResponsibles);
        //Act
        List<String> result = groupResponseDTO.getResponsibles();

        //Assert
        assertTrue(result.contains("crab@gmail.com"));
    }

    @Test
    void getMembers() {
        //Act
        List<String> result = groupResponseDTO.getMembers();

        //Assert
        assertTrue(result.contains("seafood@gmail.com"));
    }

    @Test
    void setMembers() {
        //Arrange
        List<String> newMembers = new ArrayList<>();
        newMembers.add("crab@gmail.com");
        groupResponseDTO.setMembers(newMembers);
        //Act
        List<String> result = groupResponseDTO.getMembers();

        //Assert
        assertTrue(result.contains("crab@gmail.com"));
    }

    @Test
    void getCategories() {
        //Act
        List<String> result = groupResponseDTO.getCategories();

        //Assert
        assertTrue(result.contains("Megatory"));
    }

    @Test
    void setCategories() {
        //Arrange
        List<String> newCategories = new ArrayList<>();
        newCategories.add("Sea Shells");
        groupResponseDTO.setCategories(newCategories);
        //Act
        List<String> result = groupResponseDTO.getCategories();

        //Assert
        assertTrue(result.contains("Sea Shells"));
    }

    @DisplayName("GroupResponseDTO - Equals - same object")
    @Test
    void GroupResponseDTO_Equals_same_object() {
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription, responsibles, members, categories);
        assertEquals(groupResponseDTO, groupResponseDTO);
    }

    @DisplayName("GroupResponseDTO - Equals - Equal object")
    @Test
    void GroupResponseDTO_Equals_equal_object() {
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription, responsibles, members, categories);
        GroupResponseDTO groupResponseDTO2 = new GroupResponseDTO(groupDescription, responsibles, members, categories);
        assertEquals(groupResponseDTO, groupResponseDTO2);
    }

    @DisplayName("AddGroupRequestDTO - Equals - object has different class")
    @Test
    void GroupResponseDTO_Equals_different_class() {
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription, responsibles, members, categories);
        Object o = new Object();
        assertNotEquals(groupResponseDTO, o);
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is different")
    @Test
    void GroupResponseDTO_Equals_different() {
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription, responsibles, members, categories);
        GroupResponseDTO groupResponseDTO2 = new GroupResponseDTO("TestGroup2", responsibles, members, categories);
        assertNotEquals(groupResponseDTO, groupResponseDTO2);
    }

    @DisplayName("AddGroupRequestDTO - Equals - object is null")
    @Test
    void GroupResponseDTO_Equals_null() {
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        GroupResponseDTO groupResponseDTO = new GroupResponseDTO(groupDescription, responsibles, members, categories);
        assertNotEquals(null, groupResponseDTO);
    }

    @DisplayName("Override Hash Code - Not Equals")
    @Test
    void testHashCode() {
        //Arrange
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        GroupResponseDTO groupResponseDTO2 = new GroupResponseDTO("TestGroup2", responsibles, members, categories);
        //Assert
        assertNotEquals(groupResponseDTO.hashCode(), groupResponseDTO2.hashCode());
    }

    @DisplayName("Override Hash Code - Equals")
    @Test
    void testHashCodeEquals() {
        //Arrange
        List<String> responsibles = new ArrayList<>();
        List<String> members = new ArrayList<>();
        List<String> categories = new ArrayList<>();
        GroupResponseDTO groupResponseDTO2 = new GroupResponseDTO(groupDescription, responsibles, members, categories);

        //Assert
        assertEquals(groupResponseDTO.hashCode(), groupResponseDTO2.hashCode());
    }
}
