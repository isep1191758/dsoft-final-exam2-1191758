package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.shared.Description;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class GetResponsiblesResponseDTOTest {
    GetResponsiblesResponseDTO getResponsiblesResponseDTO;

    @BeforeEach
    void setUp() {
        getResponsiblesResponseDTO = new GetResponsiblesResponseDTO("luis");
    }

    @Test
    void getResponsibles() {
        String Responsibles = getResponsiblesResponseDTO.getPerson();
    }

    @Test
    void setFamily() {
        getResponsiblesResponseDTO.setPerson("luis");
    }

    @Test
    void testEquals() {
        GetResponsiblesResponseDTO getResponsiblesResponseTestDTO;
        getResponsiblesResponseTestDTO = new GetResponsiblesResponseDTO("luis");

        assertEquals(getResponsiblesResponseDTO, getResponsiblesResponseTestDTO);
    }

    @Test
    void testEqualsSameObject() {
        assertEquals(getResponsiblesResponseDTO, getResponsiblesResponseDTO);
    }

    @Test
    void testEqualsDifferentClassObject() {
        Description description = new Description("Beautiful thing");
        //Act
        boolean result = getResponsiblesResponseDTO.equals(description);

        // Assert
        assertFalse(result);
    }

    @Test
    void testEqualsNullObject() {
        //Act
        boolean result = getResponsiblesResponseDTO.equals(null);

        // Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        GetResponsiblesResponseDTO getResponsiblesResponseTestDTO;
        getResponsiblesResponseTestDTO = new GetResponsiblesResponseDTO("luis");

        assertEquals(getResponsiblesResponseDTO.hashCode(), getResponsiblesResponseTestDTO.hashCode());
    }

    @Test
    void testToString() {
        assertEquals("luis", getResponsiblesResponseDTO.toString());
    }

}