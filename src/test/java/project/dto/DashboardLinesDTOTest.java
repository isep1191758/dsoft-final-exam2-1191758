package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Name;

import static org.junit.jupiter.api.Assertions.*;

class DashboardLinesDTOTest {
    DashboardLinesDTO dashboardLinesDTO;

    @BeforeEach
    private void setUp() {
        dashboardLinesDTO = new DashboardLinesDTO("July", 1.5);
    }

    @Test
    void getX() {
        //Act
        String result = dashboardLinesDTO.getX();

        //Assert
        assertEquals("July", result);
    }

    @Test
    void setX() {
        //Arrange
        dashboardLinesDTO.setX("May");

        //Act
        String result = dashboardLinesDTO.getX();

        //Assert
        assertEquals("May", result);
    }

    @Test
    void getY() {
        //Act
        double result = dashboardLinesDTO.getY();

        //Assert
        assertEquals(1.5, result);
    }

    @Test
    void setY() {
        //Arrange
        dashboardLinesDTO.setY(2.5);

        //Act
        double result = dashboardLinesDTO.getY();

        //Assert
        assertEquals(2.5, result);
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        // Assert
        assertEquals(dashboardLinesDTO, dashboardLinesDTO);
    }

    @DisplayName("equals() - Different Object Same attributes")
    @Test
    void equalsDifferentObjectSameAttributes() {
        //Arrange
        DashboardLinesDTO dashboardLinesDTOOther = new DashboardLinesDTO("July", 1.5);

        //Act
        boolean result = dashboardLinesDTO.equals(dashboardLinesDTOOther);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentClassObject() {
        //Arrange
        Name jose = new Name("José");

        //Act
        boolean result = dashboardLinesDTO.equals(jose);

        // Assert
        assertFalse(result);
    }

    @DisplayName("hashcode() - Same attributes Object")
    @Test
    void hashcodeSameAttributesObject() {
        //Arrange
        DashboardLinesDTO dashboardLinesDTOOther = new DashboardLinesDTO("July", 1.5);

        // Assert
        assertEquals(dashboardLinesDTO.hashCode(), dashboardLinesDTOOther.hashCode());
    }

    @DisplayName("hashcode() - Exact value")
    @Test
    void hashcodeExactValue() {
        //Arrange
        int expected = 2320471;
        int result = dashboardLinesDTO.hashCode();

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        String expected = "{x:'July', y:1.5}";
        String result = dashboardLinesDTO.toString();

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void compareTo() {
        //Arrange
        int expected = 0;
        int result = dashboardLinesDTO.compareTo(new DashboardLinesDTO("July", 2.5));

        // Assert
        assertEquals(expected, result);
    }
}