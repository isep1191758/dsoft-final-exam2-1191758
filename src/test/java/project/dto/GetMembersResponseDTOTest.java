package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.shared.Description;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class GetMembersResponseDTOTest {
    GetMembersResponseDTO getMembersResponseDTO;

    @BeforeEach
    void setUp() {
        getMembersResponseDTO = new GetMembersResponseDTO("duarte");
    }

    @Test
    void getMembers() {
        String Members = getMembersResponseDTO.getPerson();
    }

    @Test
    void setFamily() {
        getMembersResponseDTO.setPerson("duarte");
    }

    @Test
    void testEquals() {
        GetMembersResponseDTO getMembersResponseTestDTO;
        getMembersResponseTestDTO = new GetMembersResponseDTO("duarte");

        assertEquals(getMembersResponseDTO, getMembersResponseTestDTO);
    }

    @Test
    void testEqualsSameObject() {
        assertEquals(getMembersResponseDTO, getMembersResponseDTO);
    }

    @Test
    void testEqualsDifferentClassObject() {
        Description description = new Description("Marvelous thing");
        //Act
        boolean result = getMembersResponseDTO.equals(description);

        // Assert
        assertFalse(result);
    }

    @Test
    void testEqualsNullObject() {
        //Act
        boolean result = getMembersResponseDTO.equals(null);

        // Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        GetMembersResponseDTO getMembersResponseTestDTO;
        getMembersResponseTestDTO = new GetMembersResponseDTO("duarte");

        assertEquals(getMembersResponseDTO.hashCode(), getMembersResponseTestDTO.hashCode());
    }

    @Test
    void testToString() {
        assertEquals("duarte", getMembersResponseDTO.toString());
    }

}
