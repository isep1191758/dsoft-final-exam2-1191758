package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.PersonAssembler;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PersonDTOTest {
    PersonDTO personDTO;

    @BeforeEach
    void setUp() {
        Address birthAddress = new Address("VNGaia, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1980, 8, 11, 0, 1));

        Name tarcisio = new Name("Duarte");
        Email tarcisioEmail = new Email("duarte@stcp.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, null, null);

        personDTO = PersonAssembler.mapToDTO(son);
    }

    @Test
    void getPersonID() {
        PersonID result = personDTO.getPersonID();

        assertEquals("duarte@stcp.com", result.getEmail().getEmail());
    }

    @Test
    void setPersonID() {
        personDTO.setPersonID(personDTO.getPersonID());
    }

    @Test
    void getName() {
        Name result = personDTO.getName();
    }

    @Test
    void setName() {
        personDTO.setName(personDTO.getName());
    }

    @Test
    void getBirthPlace() {
        Address result = personDTO.getBirthPlace();
    }

    @Test
    void setBirthPlace() {
        personDTO.setBirthPlace(personDTO.getBirthPlace());
    }

    @Test
    void getBirthDate() {
        Date result = personDTO.getBirthDate();
    }

    @Test
    void setBirthDate() {
        personDTO.setBirthDate(personDTO.getBirthDate());
    }

    @Test
    void getMother() {
        PersonID result = personDTO.getMother();
    }

    @Test
    void setMother() {
        personDTO.setMother(personDTO.getMother());
    }

    @Test
    void getFather() {
        PersonID result = personDTO.getFather();
    }

    @Test
    void setFather() {
        personDTO.setFather(personDTO.getFather());
    }

    @Test
    void getSiblings() {
        Set<PersonID> result = personDTO.getSiblings();
    }

    @Test
    void setSiblings() {
        personDTO.setSiblings(personDTO.getSiblings());
    }

    @Test
    void getCategories() {
        Set<Category> result = personDTO.getCategories();
    }

    @Test
    void setCategories() {
        personDTO.setCategories(personDTO.getCategories());
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        // Assert
        assertEquals(personDTO, personDTO);
    }

    @DisplayName("equals() - Different Object Same attributes")
    @Test
    void equalsDifferentObjectSameAttributes() {
        //Arrange
        Address birthTarcisionAddress = new Address("VNGaia, Portugal");
        Date personsTarcisioBirthdate = new Date(LocalDateTime.of(1980, 8, 11, 0, 1));

        Name tarcisioName = new Name("Duarte");
        Email tarcisio2Email = new Email("duarte@stcp.com");
        Person tarcisioPerson = new Person(tarcisioName, birthTarcisionAddress, personsTarcisioBirthdate, tarcisio2Email, null, null);

        PersonDTO tarcisioPersonDTO = PersonAssembler.mapToDTO(tarcisioPerson);

        //Act
        boolean result = personDTO.equals(tarcisioPersonDTO);

        // Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Object is from a different class")
    @Test
    void equalsDifferentClassObject() {
        //Arrange
        Name jose = new Name("José");

        //Act
        boolean result = personDTO.equals(jose);

        // Assert
        assertFalse(result);
    }

    @DisplayName("hashcode() - Same attributes Object")
    @Test
    void hashcodeSameAttributesObject() {
        //Arrange
        Address birthAddress = new Address("VNGaia, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1980, 8, 11, 0, 1));

        Name tarcisio = new Name("Duarte");
        Email tarcisioEmail = new Email("duarte@stcp.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, null, null);

        PersonDTO tarcisioPersonDTO = PersonAssembler.mapToDTO(son);

        // Assert
        assertEquals(personDTO.hashCode(), tarcisioPersonDTO.hashCode());
    }

    @DisplayName("hashcode() - Exact value")
    @Test
    void hashcodeExactValue() {
        //Arrange
        Address birthAddress = new Address("VNGaia, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1980, 8, 11, 0, 1));

        Name tarcisio = new Name("Duarte");
        Email tarcisioEmail = new Email("duarte@stcp.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, null, null);

        int expected = 596789233;
        PersonDTO result = PersonAssembler.mapToDTO(son);

        // Assert
        assertEquals(expected, result.hashCode());
    }
}