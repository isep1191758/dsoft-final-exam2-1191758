package project.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.model.account.Account;
import project.model.shared.*;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class AccountRepositoryTest {
    @Autowired
    AccountRepository accountRepository;
    Denomination denomination;
    Description description;
    Email email;
    OwnerID ownerID;

    @BeforeEach()
    void setUp() {
        denomination = new Denomination("Main Bank");
        description = new Description("Paycheck account");
        email = new Email("joana@gmail.com");
        ownerID = new PersonID(email);
    }

    @DisplayName("save() - Happy Path")
    @Test
    void saveHappyPath() {
        //Arrange
        Account testAccount = new Account(denomination, description, ownerID);

        //Act
        Account result = accountRepository.save(testAccount);
        System.out.println(result.getAccountID());
        //Assert
        assertEquals(testAccount, result);
    }

    @DisplayName("save() - Null Account")
    @Test
    void saveNullAccount() {
        //Arrange
        Account testAccount = null;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            Account result = accountRepository.save(testAccount);
        });
    }

    @DisplayName("findAll() - Happy Path")
    @Test
    void findAll() {
        //Arrange
        final Account[] result = {null};
        Account testAccount = new Account(denomination, description, ownerID);
        accountRepository.save(testAccount);

        Account expected = new Account(denomination, description, ownerID);

        //Act
        Iterable<Account> accounts = accountRepository.findAll();

        accounts.forEach(account -> {
            result[0] = account;
        });

        //Assert
        assertEquals(testAccount, result[0]);
    }

    @DisplayName("findById() - Happy Path")
    @Test
    void findByIdHappyPath() {
        //Assert
        AccountID accountId = new AccountID(denomination, ownerID);
        Account testAccount = new Account(denomination, description, ownerID);
        Account resultAccount = null;
        accountRepository.save(testAccount);

        //Act
        if (accountRepository.findById(accountId).isPresent()) {
            resultAccount = accountRepository.findById(accountId).get();
        }

        //Assert
        assertEquals(testAccount, resultAccount);
    }

    @DisplayName("findById() - No accounts found")
    @Test
    void findById() {
        accountRepository.deleteAll();
        //Assert
        Denomination testDenomination = new Denomination("General Store Account");
        AccountID accountId = new AccountID(denomination, ownerID);
        boolean result = true;

        //Act
        if (!accountRepository.findById(accountId).isPresent()) {
            result = false;
        }

        //Assert
        assertFalse(result);
    }

    @DisplayName("findByAccountID_DenominationAndAccountID_OwnerID() - Happy Path")
    @Test
    void findByAccountID_DenominationAndAccountID_OwnerIDHappyPath() {
        //Assert
        AccountID accountId = new AccountID(denomination, ownerID);
        Account testAccount = new Account(denomination, description, ownerID);
        Account resultAccount = null;
        accountRepository.save(testAccount);
        Optional<Account> foundAccount = accountRepository.findByAccountID_DenominationAndAccountID_OwnerID(denomination, ownerID);

        //Act
        if (foundAccount.isPresent()) {
            resultAccount = accountRepository.findById(accountId).get();
        }

        //Assert
        assertEquals(testAccount, resultAccount);
    }
}