package project.exceptions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ActiveProfiles("exceptions")
@ExtendWith(SpringExtension.class)
class RestExceptionHandlerTest {
    @Autowired
    NullPointerException nullPointerMockException;

    @Test
    void handleNullPointerException() {
        RestExceptionHandler restExceptionHandler = new RestExceptionHandler();
        ResponseEntity<Object> response = restExceptionHandler.handleNullPointerException(new NullPointerException("Cannot be null"));
    }

    @Test
    void handleNotFoundException() {
        RestExceptionHandler restExceptionHandler = new RestExceptionHandler();

        ResponseEntity<Object> response = restExceptionHandler.handleNotFound(new NotFoundException("Not found with that ID"));
    }

    @Test
    void handleEmptyException() {
        RestExceptionHandler restExceptionHandler = new RestExceptionHandler();

        ResponseEntity<Object> response = restExceptionHandler.handleEmptyException(new EmptyException("Empty"));
    }

    @Test
    void handleAlreadyExistException() {
        RestExceptionHandler restExceptionHandler = new RestExceptionHandler();

        ResponseEntity<Object> response = restExceptionHandler.handleAlreadyExistsException(new AlreadyExistsException("Already Exist"));
    }
}