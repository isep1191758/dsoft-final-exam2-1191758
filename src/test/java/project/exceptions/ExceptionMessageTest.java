package project.exceptions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class ExceptionMessageTest {
    ExceptionMessage exceptionMessage;

    @BeforeEach
    void setUp() {
        exceptionMessage = new ExceptionMessage(HttpStatus.BAD_REQUEST, "Error Message Localized", Arrays.asList("Error Message"));
    }

    @Test
    void setApiErrorAsList() {
        List<String> errorList = new ArrayList<>();
        errorList.add("New error");

        ExceptionMessage exceptionMessageAsList = new ExceptionMessage(HttpStatus.BAD_REQUEST, "Error Message Localized", errorList);
    }

    @Test
    void getStatus() {
        HttpStatus status = exceptionMessage.getStatus();
    }

    @Test
    void setStatus() {
        exceptionMessage.setStatus(HttpStatus.OK);
    }

    @Test
    void getMessage() {
        String message = exceptionMessage.getMessage();
    }

    @Test
    void setMessage() {
        exceptionMessage.setMessage("New error message");
    }

    @Test
    void getErrors() {
        List<String> errors = exceptionMessage.getErrors();
    }

    @Test
    void setErrors() {
        List<String> errorList = new ArrayList<>();
        errorList.add("New error");

        exceptionMessage.setErrors(errorList);
    }
}