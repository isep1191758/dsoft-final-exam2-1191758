package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.shared.AccountID;
import project.model.shared.Denomination;
import project.model.shared.Email;
import project.model.shared.PersonID;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class AccountDataTest {
    @Test
    void getAccountID() {
        //Arrange
        AccountData noArgsAccountData = new AccountData();
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");

        //Act
        AccountID result = accountData.getAccountID();

        //Assert
        assertEquals(accountID, result);
    }

    @Test
    void getDescription() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");

        String expected = "Great description for a Great Account";

        //Act
        String result = accountData.getDescription();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setAccountID() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");

        accountData.setAccountID(accountID);
        //Act
        AccountID result = accountData.getAccountID();

        //Assert
        assertEquals(accountID, result);
    }

    @Test
    void setDescription() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");

        String expected = "Great description for the Greatest Account";

        accountData.setDescription(expected);

        //Act
        String result = accountData.getDescription();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEquals() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");

        //Act
        boolean result = accountData.equals(accountData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObjectSameInfo() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");
        AccountData accountDataSameInfo = new AccountData(accountID, "Great description for a Great Account");

        //Act
        boolean result = accountData.equals(accountDataSameInfo);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");
        AccountData accountDataSameInfo = null;

        //Act
        boolean result = accountData.equals(accountDataSameInfo);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");

        //Act
        boolean result = accountData.equals(accountID);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");

        int expected = Objects.hash(accountID, "Great description for a Great Account");

        //Act
        int result = accountData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeExactValue() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");

        int expected = 267324397;

        //Act
        int result = accountData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        AccountID accountID = new AccountID(new Denomination("Great Account"), new PersonID(new Email("johnfromaccounting@gmail.com")));
        AccountData accountData = new AccountData(accountID, "Great description for a Great Account");

        String expected = "AccountData{accountID=Great Account, description='Great description for a Great Account'}";

        //Act
        String result = accountData.toString();

        //Assert
        assertEquals(expected, result);
    }
}