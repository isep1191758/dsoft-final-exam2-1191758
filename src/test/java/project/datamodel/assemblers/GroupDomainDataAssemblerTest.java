//package project.datamodel.assemblers;
//
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.datamodel.*;
//import project.model.group.Group;
//import project.model.person.Person;
//import project.model.shared.*;
//
//import java.time.LocalDateTime;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//class GroupDomainDataAssemblerTest {
//    @DisplayName("Convert a Group object to GroupData object")
//    @Test
//    void toData() {
//        //Arrange
//        Address birthAddress = new Address("Porto, Portugal");
//        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
//        Name maria = new Name("Maria");
//        Email mariaEmail = new Email("maria@family.com");
//        Person mother = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
//        Category categoryExample = new Category(new Designation("Test category"));
//        mother.addCategory(categoryExample);
//        Group groupDomain = new Group(new Description("Test Group"), mother.getPersonID());
//
//        GroupData expected = new GroupData(new GroupIDData("Test Group"), LocalDateTime.now().toString());
//        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", expected);
//        expected.setResponsible(new ResponsibleIDData(mother.getPersonID()));
//        expected.setMember(new MemberIDData(mother.getPersonID()));
//
//        //Act
//        GroupData result = GroupDomainDataAssembler.toData(groupDomain);
//
//        //Assert
//        assertEquals(expected, result);
//    }
//
//    @DisplayName("Convert a GroupData object to Group object")
//    @Test
//    void toDomain() {
//        //Arrange
//        Address birthAddress = new Address("Porto, Portugal");
//        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
//        Name maria = new Name("Maria");
//        Email mariaEmail = new Email("maria@family.com");
//        Person mother = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
//        Category categoryExample = new Category(new Designation("Test category"));
//        mother.addCategory(categoryExample);
//        Group expected = new Group(new Description("Test Group"), mother.getPersonID());
//
//        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
//        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
//        groupData.setResponsible(new ResponsibleIDData(mother.getPersonID()));
//        groupData.setMember(new MemberIDData(mother.getPersonID()));
//
//        //Act
//        Group result = GroupDomainDataAssembler.toDomain(groupData);
//
//        //Assert
//        assertEquals(expected, result);
//    }
//}