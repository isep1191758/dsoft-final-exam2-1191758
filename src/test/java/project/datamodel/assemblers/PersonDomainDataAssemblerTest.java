//package project.datamodel.assemblers;
//
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.datamodel.PersonCategoryData;
//import project.datamodel.PersonData;
//import project.model.person.Person;
//import project.model.shared.*;
//
//import java.time.LocalDateTime;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//class PersonDomainDataAssemblerTest {
//    @DisplayName("Convert a Person object to PersonData object")
//    @Test
//    void toData() {
//        //Arrange
//        Address birthAddress = new Address("Porto, Portugal");
//        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
//        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
//        Name maria = new Name("Maria");
//        Email mariaEmail = new Email("maria@family.com");
//        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
//        Name jose = new Name("José");
//        Email joseEmail = new Email("jose@family.com");
//        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
//        Name tarcisio = new Name("Tarcisio");
//        Email tarcisioEmail = new Email("tarcisio@family.com");
//        Category categoryExample = new Category(new Designation("Categoria Exemplo"));
//        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
//        son.addCategory(categoryExample);
//
//        PersonData expected = new PersonData(son.getPersonID(), son.getName().toString(), son.getBirthPlace().toString(), son.getBirthDate().toString());
//        expected.setFather(father.getPersonID().toString());
//        expected.setMother(mother.getPersonID().toString());
//
//        PersonCategoryData categoryDataExample = new PersonCategoryData("Categoria Exemplo", expected);
//        expected.setCategory(categoryDataExample);
//
//        //Act
//        PersonData result = PersonDomainDataAssembler.toData(son);
//
//        //Assert
//        assertEquals(expected, result);
//        assertEquals(expected.getCategories(), result.getCategories());
//    }
//
//    @Test
//    @DisplayName("Convert a Person object to PersonData object")
//    void toDomain() {
//        //Arrange
//        Address birthAddress = new Address("Porto, Portugal");
//        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
//        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
//        Name maria = new Name("Maria");
//        Email mariaEmail = new Email("maria@family.com");
//        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
//        Name jose = new Name("José");
//        Email joseEmail = new Email("jose@family.com");
//        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
//        Name tarcisio = new Name("Tarcisio");
//        Email tarcisioEmail = new Email("tarcisio@family.com");
//        Category categoryExample = new Category(new Designation("Categoria Exemplo"));
//        Person expected = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
//        expected.addCategory(categoryExample);
//
//        PersonData personData = new PersonData(expected.getPersonID(), expected.getName().toString(), expected.getBirthPlace().toString(), expected.getBirthDate().toString());
//        personData.setFather(father.getPersonID().toString());
//        personData.setMother(mother.getPersonID().toString());
//
//        PersonCategoryData categoryDataExample = new PersonCategoryData("Categoria Exemplo", personData);
//        personData.setCategory(categoryDataExample);
//
//        //Act
//        Person result = PersonDomainDataAssembler.toDomain(personData);
//
//        //Assert
//        assertEquals(expected, result);
//        assertEquals(expected.getCategories(), result.getCategories());
//    }
//}