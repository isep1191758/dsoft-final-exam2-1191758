package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.PersonID;

import java.time.LocalDateTime;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class PersonCategoryIDDataTest {
    @Test
    void getId() {
        //Arrange
        PersonCategoryIDData noArgsPersonCategoryIDData = new PersonCategoryIDData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        String expected = "Category Example";

        //Act
        String result = personCategoryIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        personCategoryIDData.setId("Category Example Modified");

        String expected = "Category Example Modified";

        //Act
        String result = personCategoryIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getPerson() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        //Act
        PersonData result = personCategoryIDData.getPerson();

        //Assert
        assertEquals(personData, result);
    }

    @Test
    void setPerson() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        personCategoryIDData.setPerson(personData);

        //Act
        PersonData result = personCategoryIDData.getPerson();

        //Assert
        assertEquals(personData, result);
    }

    @Test
    void testEqualsSameDifferentObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);
        PersonCategoryIDData personCategoryIDDataSameInfo = new PersonCategoryIDData("Category Example", personData);

        //Act
        boolean result = personCategoryIDData.equals(personCategoryIDDataSameInfo);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsSameObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        //Act
        boolean result = personCategoryIDData.equals(personCategoryIDData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        PersonCategoryIDData nullPersonCategoryData = null;

        //Act
        boolean result = personCategoryIDData.equals(nullPersonCategoryData);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        //Act
        boolean result = personCategoryIDData.equals(personData);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        int expected = Objects.hash(personCategoryIDData.getId(), personCategoryIDData.getPerson());

        //Act
        int result = personCategoryIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        int expected = 1285529687;

        //Act
        int result = personCategoryIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        String mariaName = "Maria";
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), mariaName, birthAddress.toString(), personsBirthdate.toString());

        PersonCategoryIDData personCategoryIDData = new PersonCategoryIDData("Category Example", personData);

        String expected = "PersonCategoryIDData(id=Category Example, person=PersonData{personId=maria@family.com, name='Maria', birthPlace='Porto, Portugal', birthDate='1978-08-12', mother='null', father='null', siblings=[], categories=[]})";

        //Act
        String result = personCategoryIDData.toString();

        //Assert
        assertEquals(expected, result);
    }
}