package project.datamodel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class GroupDataTest {
    GroupData groupData;

    @BeforeEach
    void setUp() {
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);
        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));
        groupData.setCategory(categoryData);
    }

    @Test
    void getId() {
        //Arrange
        GroupCategoryData noArgsGroup = new GroupCategoryData();
        GroupIDData expected = new GroupIDData("Test Group");

        //Act
        GroupIDData result = groupData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        GroupIDData expected = new GroupIDData("Test Group Modified");

        groupData.setId(expected);

        //Act
        GroupIDData result = groupData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getResponsibles() {
        //Arrange
        ResponsibleIDData expected = new ResponsibleIDData(new PersonID(new Email("maria@family.com")));

        //Act
        List<ResponsibleIDData> result = groupData.getResponsibles();

        //Assert
        assertTrue(result.contains(expected));
    }

    @Test
    void setResponsibles() {
        //Arrange
        ResponsibleIDData expected = new ResponsibleIDData(new PersonID(new Email("robertofernandes@family.com")));
        ResponsibleIDData alsoExpected = new ResponsibleIDData(new PersonID(new Email("veronicaraposo@family.com")));
        List<ResponsibleIDData> responsibleIDDataList = new ArrayList<>();
        responsibleIDDataList.add(expected);
        responsibleIDDataList.add(alsoExpected);

        groupData.setResponsibles(responsibleIDDataList);

        //Act
        List<ResponsibleIDData> result = groupData.getResponsibles();

        //Assert
        assertTrue(result.contains(expected));
        assertTrue(result.contains(alsoExpected));
    }

    @Test
    void setResponsible() {
        //Arrange
        ResponsibleIDData expected = new ResponsibleIDData(new PersonID(new Email("robertofontes@family.com")));

        groupData.setResponsible(expected);

        //Act
        List<ResponsibleIDData> result = groupData.getResponsibles();

        //Assert
        assertTrue(result.contains(expected));
    }

    @Test
    void getMembers() {
        //Arrange
        MemberIDData expected = new MemberIDData(new PersonID(new Email("maria@family.com")));

        //Act
        List<MemberIDData> result = groupData.getMembers();

        //Assert
        assertTrue(result.contains(expected));
    }

    @Test
    void setMembers() {
        //Arrange
        MemberIDData expected = new MemberIDData(new PersonID(new Email("robertofernandes@family.com")));
        MemberIDData alsoExpected = new MemberIDData(new PersonID(new Email("veronicaraposo@family.com")));
        List<MemberIDData> memberIDDataArrayList = new ArrayList<>();
        memberIDDataArrayList.add(expected);
        memberIDDataArrayList.add(alsoExpected);

        groupData.setMembers(memberIDDataArrayList);

        //Act
        List<MemberIDData> result = groupData.getMembers();

        //Assert
        assertTrue(result.contains(expected));
        assertTrue(result.contains(alsoExpected));
    }

    @Test
    void setMember() {
        //Arrange
        MemberIDData expected = new MemberIDData(new PersonID(new Email("robertofontes@family.com")));

        groupData.setMember(expected);

        //Act
        List<MemberIDData> result = groupData.getMembers();

        //Assert
        assertTrue(result.contains(expected));
    }

    @Test
    void getCategories() {
        //Arrange
        GroupCategoryData expected = new GroupCategoryData("Category Example", groupData);

        //Act
        List<GroupCategoryData> result = groupData.getCategories();

        //Assert
        assertTrue(result.contains(expected));
    }

    @Test
    void setCategories() {
        //Arrange
        GroupCategoryData expected = new GroupCategoryData("Fabulous", groupData);
        GroupCategoryData alsoExpected = new GroupCategoryData("Also Fabulous", groupData);
        List<GroupCategoryData> groupCategoryDataList = new ArrayList<>();
        groupCategoryDataList.add(expected);
        groupCategoryDataList.add(alsoExpected);

        groupData.setCategories(groupCategoryDataList);

        //Act
        List<GroupCategoryData> result = groupData.getCategories();

        //Assert
        assertTrue(result.contains(expected));
        assertTrue(result.contains(alsoExpected));
    }

    @Test
    void setCategory() {
        //Arrange
        GroupCategoryData expected = new GroupCategoryData("Fabulousness", groupData);

        groupData.setCategory(expected);

        //Act
        List<GroupCategoryData> result = groupData.getCategories();

        //Assert
        assertTrue(result.contains(expected));
    }

    @Test
    void getCreationDate() {
        //Arrange
        String str = LocalDate.now().toString();
        String[] dateSplit = str.split("-");

        String expected = dateSplit[0] + "-" + dateSplit[1] + "-" + dateSplit[2];

        //Act
        String result = groupData.getCreationDate();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setCreationDate() {
        //Arrange
        String str = LocalDate.now().toString();
        String[] dateSplit = str.split("-");

        String expected = dateSplit[0] + "-" + dateSplit[1] + "-" + dateSplit[2];

        groupData.setCreationDate(expected);

        //Act
        String result = groupData.getCreationDate();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCode() {
        //Arrange
        int expected = Objects.hash(groupData.getId());

        //Act
        int result = groupData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        int expected = -2075392593;

        //Act
        int result = groupData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEquals() {
        //Arrange
        //Act
        boolean result = groupData.equals(groupData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferenObjectSameInfo() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupDataCopy = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupDataCopy);
        GroupCategoryData categoryData = new GroupCategoryData("Category Example", groupDataCopy);
        groupDataCopy.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupDataCopy.setMember(new MemberIDData(person.getPersonID()));
        groupDataCopy.setCategory(categoryData);

        //Act
        boolean result = groupData.equals(groupDataCopy);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        GroupData groupDataCopy = null;

        //Act
        boolean result = groupData.equals(groupDataCopy);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        Description description = new Description("Cool desc yo");

        //Act
        boolean result = groupData.equals(description);

        //Assert
        assertFalse(result);
    }

    @Test
    void testToString() {
        //Arrange
        String expected = "GroupData{id=GroupIDData(id=Test Group), responsibles=[ResponsibleIDData(id=maria@family.com)], members=[MemberIDData(id=maria@family.com)], categories=[Category Example]}";

        //Act
        String result = groupData.toString();

        //Assert
        assertEquals(expected, result);
    }
}