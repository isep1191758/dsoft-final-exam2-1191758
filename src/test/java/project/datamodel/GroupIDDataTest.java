package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.shared.Description;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class GroupIDDataTest {
    @Test
    void getId() {
        //Arrange
        GroupIDData groupIDData = new GroupIDData("Group Example");

        String expected = "Group Example";

        //Act
        String result = groupIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        GroupIDData groupIDData = new GroupIDData("Group Example");

        String expected = "Group Example 2";

        //Act
        groupIDData.setId("Group Example 2");
        String result = groupIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEquals() {
        //Arrange
        GroupIDData groupIDData = new GroupIDData("Group Example");

        //Act
        boolean result = groupIDData.equals(groupIDData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObjectSameInfo() {
        //Arrange
        GroupIDData groupIDData = new GroupIDData("Group Example");
        GroupIDData groupIDDataSameInfo = new GroupIDData("Group Example");

        //Act
        //Assert
        assertEquals(groupIDDataSameInfo, groupIDData);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        GroupIDData groupIDData = new GroupIDData("Group Example");
        GroupIDData groupIDDataSameInfo = null;

        //Act
        boolean result = groupIDData.equals(groupIDDataSameInfo);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        GroupIDData groupIDData = new GroupIDData("Group Example");
        Description description = new Description("Description");

        //Act
        boolean result = groupIDData.equals(description);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        GroupIDData groupIDData = new GroupIDData("Group Example");

        int expected = Objects.hash(groupIDData.getId());

        //Act
        int result = groupIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        GroupIDData groupIDData = new GroupIDData("Group Example");

        int expected = -1230996312;

        //Act
        int result = groupIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        GroupIDData groupIDData = new GroupIDData("Group Example");

        String expected = "GroupIDData(id=Group Example)";

        //Act
        String result = groupIDData.toString();

        //Assert
        assertEquals(expected, result);
    }
}