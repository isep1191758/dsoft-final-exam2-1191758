package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.shared.Email;
import project.model.shared.PersonID;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class PersonCategoryDataTest {
    @Test
    void getId() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Category Example", personData);

        personData.setCategory(personCategoryData);

        String expected = "Category Example";

        //Act
        String result = personCategoryData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        Email fernandoEmail = new Email("fernando@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Category Example", personData);

        personData.setCategory(personCategoryData);

        personCategoryData.setId("Vananas com Chicolate");

        String expected = "Vananas com Chicolate";

        //Act
        String result = personCategoryData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEquals() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Category Example", personData);

        personData.setCategory(personCategoryData);

        //Act
        boolean result = personCategoryData.equals(personCategoryData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObjectSameInfo() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Category Example", personData);
        PersonCategoryData personCategoryDataAlso = new PersonCategoryData("Category Example", personData);

        //Act
        boolean result = personCategoryData.equals(personCategoryDataAlso);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Category Example", personData);
        PersonCategoryData personCategoryDataAlso = null;

        //Act
        boolean result = personCategoryData.equals(personCategoryDataAlso);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Category Example", personData);

        //Act
        boolean result = personCategoryData.equals(mariaEmail);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Category Example", personData);

        int expected = Objects.hash("Category Example");

        //Act
        int result = personCategoryData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Category Example", personData);

        int expected = 481585255;

        //Act
        int result = personCategoryData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        PersonCategoryData noArgsGroup = new PersonCategoryData();
        Email mariaEmail = new Email("maria@family.com");
        PersonData personData = new PersonData(new PersonID(mariaEmail), "Maria", "Porto, Portugal", "1978-06-21");
        PersonCategoryData personCategoryData = new PersonCategoryData("Category Example", personData);

        String expected = "Category Example";

        //Act
        String result = personCategoryData.toString();

        //Assert
        assertEquals(expected, result);
    }
}