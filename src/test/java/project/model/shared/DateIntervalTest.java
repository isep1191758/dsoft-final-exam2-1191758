package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

class DateIntervalTest {
    @DisplayName("DateInterval - Happy Path")
    @Test
    void setTimePeriodIfValidHappyPath() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));

        //Act//Assert
        new DateInterval(initialDate, endDate);
    }

    @DisplayName("DateInterval - Null initial date")
    @Test
    void setTimePeriodIfValidNullInitalDate() {
        //Arrange
        Date initialDate = null;
        Date endDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));

        //Act//Assert
        assertThrows(NullPointerException.class, () -> new DateInterval(initialDate, endDate));
    }

    @DisplayName("DateInterval - Null end date")
    @Test
    void setTimePeriodIfValidNullEndDate() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endDate = null;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> new DateInterval(initialDate, endDate));
    }

    @DisplayName("DateInterval - End date before initial date")
    @Test
    void setTimePeriodIfValidEndDateBeforeInitialDate() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));
        Date endDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));

        //Act//Assert
        assertThrows(IllegalArgumentException.class, () -> new DateInterval(initialDate, endDate));
    }

    @DisplayName("getInitialDate - Happy Path")
    @Test
    void getInitialDateHappyPath() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));

        DateInterval dateInterval = new DateInterval(initialDate, endDate);

        //Act
        Date result = dateInterval.getInitialDate();

        // Assert
        assertEquals(initialDate, result);
    }

    @DisplayName("getEndDate - Happy Path")
    @Test
    void getEndDateHappyPath() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));

        DateInterval dateInterval = new DateInterval(initialDate, endDate);

        //Act
        Date result = dateInterval.getEndDate();

        // Assert
        assertEquals(endDate, result);
    }

    @DisplayName("sameValueAs - Same Dates")
    @Test
    void sameValueAsSameDates() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));
        DateInterval dateInterval = new DateInterval(initialDate, endDate);

        Date initialSecondDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endSecondDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));
        DateInterval secondDateInterval = new DateInterval(initialSecondDate, endSecondDate);

        //Act
        boolean result = dateInterval.sameValueAs(secondDateInterval);

        // Assert
        assertTrue(result);
    }

    @DisplayName("sameValueAs - Different Dates")
    @Test
    void sameValueAsDifferentDates() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));
        DateInterval dateInterval = new DateInterval(initialDate, endDate);

        Date initialSecondDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endSecondDate = new Date(LocalDateTime.of(2021, Month.JULY, 1, 0, 0, 0));
        DateInterval secondDateInterval = new DateInterval(initialSecondDate, endSecondDate);

        //Act
        boolean result = dateInterval.sameValueAs(secondDateInterval);

        // Assert
        assertFalse(result);
    }

    @DisplayName("sameValueAs - Null date to compare")
    @Test
    void sameValueAsNullDateIntervalCompare() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));
        DateInterval dateInterval = new DateInterval(initialDate, endDate);

        DateInterval secondDateInterval = null;

        //Act
        boolean result = dateInterval.sameValueAs(secondDateInterval);

        // Assert
        assertFalse(result);
    }

    @DisplayName("sameValueAs - Same Dates Interval")
    @Test
    void sameValueAsSameDatesInterval() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));
        DateInterval dateInterval = new DateInterval(initialDate, endDate);

        //Act
        boolean result = dateInterval.sameValueAs(dateInterval);

        // Assert
        assertTrue(result);
    }

    @DisplayName("sameValueAs - Different Dates")
    @Test
    void sameValueAsDifferentInitialEndDates() {
        //Arrange
        Date initialDate = new Date(LocalDateTime.of(2017, Month.JULY, 10, 0, 0, 0));
        Date endDate = new Date(LocalDateTime.of(2020, Month.JULY, 1, 0, 0, 0));
        DateInterval dateInterval = new DateInterval(initialDate, endDate);

        Date initialSecondDate = new Date(LocalDateTime.of(2019, Month.JULY, 1, 0, 0, 0));
        Date endSecondDate = new Date(LocalDateTime.of(2021, Month.JULY, 1, 0, 0, 0));
        DateInterval secondDateInterval = new DateInterval(initialSecondDate, endSecondDate);

        //Act
        boolean result = dateInterval.sameValueAs(secondDateInterval);

        // Assert
        assertFalse(result);
    }

}