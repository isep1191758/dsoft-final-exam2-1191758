package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OwnerIDTest {
    @DisplayName("create ownerID - Happy Path")
    @Test
    void setownerIDHappyPath() {
        //Arrange
        Long ownerIDNumber = 1099029010L;

        //Act//Assert
        new OwnerID(ownerIDNumber);
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        //Arrange
        Long ownerIDNumber = 1099029010L;
        OwnerID testOwnerId = new OwnerID(ownerIDNumber);

        //Act
        boolean result = testOwnerId.equals(testOwnerId);

        //Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Null Object")
    @Test
    void equalsNullObject() {
        //Arrange
        Long ownerIDNumber = 1099029010L;
        OwnerID testOwnerId = new OwnerID(ownerIDNumber);
        OwnerID nullOwner = null;

        //Act
        boolean result = testOwnerId.equals(nullOwner);

        //Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Different Class")
    @Test
    void equalsDifferentClass() {
        //Arrange
        Long ownerIDNumber = 1099029010L;
        OwnerID testOwnerId = new OwnerID(ownerIDNumber);
        Name testName = new Name("Nuno");

        //Act
        boolean result = testOwnerId.equals(testName);

        //Assert
        assertFalse(result);
    }

    @DisplayName("getSerialVersionUID() - Happy Path")
    @Test
    void getSerialVersionUID() {
        //Arrange
        Long ownerIDNumber = 1099029010L;
        OwnerID testOwnerId = new OwnerID(ownerIDNumber);

        //Act
        Long result = OwnerID.getSerialVersionUID();
        Long expected = 3489169914181983L;
        //Assert
        assertEquals(expected, result);
    }


    @DisplayName("hashCode() - Happy Path")
    @Test
    void hashCodeHappyPath() {
        //Arrange
        Long ownerIDNumber = 1099029010L;
        OwnerID testOwnerId = new OwnerID(ownerIDNumber);

        //Act
        int result = testOwnerId.hashCode();

        //Assert
        assertEquals(1099029041, result);
    }

    @DisplayName("toString() - Happy Path")
    @Test
    void toStringHappyPath() {
        //Arrange
        Long ownerIDNumber = 1099029010L;
        OwnerID testOwnerId = new OwnerID(ownerIDNumber);

        //Act
        String result = testOwnerId.toString();

        //Assert
        assertEquals("1099029010", result);
    }

    @DisplayName("getDate() - Happy Path")
    @Test
    void getIDHappyPath() {
        //Arrange
        Long ownerIDNumber = 1099029010L;
        OwnerID testOwnerId = new OwnerID(ownerIDNumber);

        //Act
        Long result = testOwnerId.getId();
        Long expected = 1099029010L;
        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("setId() - Happy Path")
    @Test
    void tosetId() {
        //Arrange
        long expected = 1099029010L;
        OwnerID testOwnerId = new OwnerID();
        testOwnerId.setId(expected);

        //Act
        long result = testOwnerId.getId();

        //Assert
        assertEquals(expected, result);
    }
}