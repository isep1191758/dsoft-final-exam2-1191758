package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.account.Account;

import static org.junit.jupiter.api.Assertions.*;

class CategoryTest {
    @DisplayName("Equals override - Same Object")
    @Test
    void testEqualsSameObject() {
        Category category = new Category(new Designation("Groceries"), new Designation1("Week1"));

        boolean result = category.equals(category);

        assertTrue(result);
    }

    @DisplayName("Equals override - Different Objects with the same attributes")
    @Test
    void testEqualsSamesameAttriburesObject() {
        Category category = new Category(new Designation("Groceries"), new Designation1("Week1"));
        Category sameAttributesCategory = new Category(new Designation("Groceries"), new Designation1("Week1"));

        boolean result = category.equals(sameAttributesCategory);

        assertTrue(result);
    }

    @DisplayName("Equals override - Different Object Type")
    @Test
    void testEqualsDiferentObjectType() {
        Category category = new Category(new Designation("Groceries"), new Designation1("Week1"));
        Denomination denominationAccount = new Denomination("Groceries");
        Description descriptionAccount = new Description("Description");
        Email email = new Email("joana@gmail.com");
        OwnerID ownerID = new PersonID(email);


        Account account = new Account(denominationAccount, descriptionAccount, ownerID);

        boolean result = category.equals(account);

        assertFalse(result);
    }

    @DisplayName("Equals override - Null Object")
    @Test
    void testEqualsNullObject() {
        Category category = new Category(new Designation("Groceries"), new Designation1("Week1"));
        Category nullCategory = null;

        boolean result = category.equals(nullCategory);

        assertFalse(result);
    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        Category category = new Category(new Designation("Groceries"), new Designation1("Week1"));
        Category categorySameAttributes = new Category(new Designation("Groceries"), new Designation1("Week1"));

        int expectedResult = categorySameAttributes.hashCode();

        int result = category.hashCode();

        assertEquals(expectedResult, result);
    }

    @DisplayName("create category - designation Null")
    @Test
    void setIsCategoryValidDesignationNull() {

        //Arrange//Act//Assert
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new Category(null, null);
        });

        String expectedMessage = "Parameter cannot be null";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @DisplayName("create category - sameValueAs")
    @Test
    void setIsCategoryValidSameValueAs() {
        //Arrange
        Category categoryOne = new Category(new Designation("Groceries"), new Designation1("Week1"));
        Category categoryTwo = new Category(new Designation("Groceries"), new Designation1("Week1"));

        //Act
        boolean result = categoryOne.sameValueAs(categoryTwo);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create category - sameValueAsFalse")
    @Test
    void setIsCategoryValidSameValueAsFalse() {
        //Arrange
        Category categoryOne = new Category(new Designation("Groceries"), new Designation1("Week1"));
        Category categoryTwo = new Category(new Designation("Gym"), new Designation1("Week1"));

        //Act
        boolean result = categoryOne.sameValueAs(categoryTwo);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create category - sameValueAsNull")
    @Test
    void setIsCategoryValidSameValueAsNull() {
        //Arrange
        Category categoryOne = new Category(new Designation("Groceries"), new Designation1("Week1"));

        //Act
        boolean result = categoryOne.sameValueAs(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("toString")
    @Test
    void toStringHappyPath() {
        Category categoryOne = new Category(new Designation("Groceries"), new Designation1("Week1"));

        String expected = "Groceries";
        String result = categoryOne.toString();

        assertEquals(expected, result);
    }
}