package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.EmptyException;

import static org.junit.jupiter.api.Assertions.*;

class DenominationTest {
    @DisplayName("create denomination - Happy Path")
    @Test
    void setIsDenominationValidHappyPath() {
        //Arrange//Act//Assert
        new Denomination("Groceries");
    }

    @DisplayName("create denomination - Null")
    @Test
    void setIsDenominationValidNull() {

        //Arrange//Act//Assert
        Exception exception = assertThrows(NullPointerException.class, () -> {
            new Denomination(null);
        });
    }

    @DisplayName("create denomination - Empty")
    @Test
    void setIsDenominationValidEmpty() {

        //Arrange//Act//Assert
        Exception exception = assertThrows(EmptyException.class, () -> {
            new Denomination("");
        });
    }

    @DisplayName("create denomination - getDenomination")
    @Test
    void setIsDenominationValidGetDenomination() {
        //Arrange//Act//Assert
        Denomination denomination = new Denomination("Groceries");

        String expected = "Groceries";
        String result = denomination.getDenomination();

        assertEquals(expected, result);
    }

    @DisplayName("create denomination - toString")
    @Test
    void setIsDenominationValidToString() {
        //Arrange//Act//Assert
        Denomination denomination = new Denomination("Groceries");

        String expected = "Groceries";
        String result = denomination.toString();

        assertEquals(expected, result);
    }

    @DisplayName("create denomination - sameValueAs")
    @Test
    void setIsDenominationValidSameValueAs() {
        //Arrange
        Denomination denominationOne = new Denomination("Groceries");
        Denomination denominationTwo = new Denomination("Groceries");

        //Act
        boolean result = denominationOne.sameValueAs(denominationTwo);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create denomination - sameValueAsFalse")
    @Test
    void setIsDenominationValidSameValueAsFalse() {
        //Arrange
        Denomination denominationOne = new Denomination("Groceries");
        Denomination denominationTwo = new Denomination("Gym");

        //Act
        boolean result = denominationOne.sameValueAs(denominationTwo);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create denomination - sameValueAsNull")
    @Test
    void setIsDenominationValidSameValueAsNull() {
        //Arrange
        Denomination denominationOne = new Denomination("Groceries");

        //Act
        boolean result = denominationOne.sameValueAs(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create denomination - Override Equals False")
    @Test
    void setIsDenominationValidOverrideEqualsFalse() {
        //Arrange
        Denomination denominationOne = new Denomination("Groceries");
        Denomination denominationTwo = new Denomination("Gym");

        //Act
        boolean result = denominationOne.equals(denominationTwo);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create denomination - Override Equals Null")
    @Test
    void setIsDenominationValidOverrideEqualsNull() {
        //Arrange
        Denomination denominationOne = new Denomination("Groceries");

        //Act
        boolean result = denominationOne.equals(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create denomination - Override Equals Same Object")
    @Test
    void setIsDenominationValidOverrideEqualsSameObject() {
        //Arrange
        Denomination denominationOne = new Denomination("Groceries");

        //Act
        boolean result = denominationOne.equals(denominationOne);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create denomination - Override Equals Different Class")
    @Test
    void setIsDenominationValidOverrideEqualsDifferentClass() {
        //Arrange
        Denomination denominationOne = new Denomination("Groceries");
        Name name = new Name("Ludovina");

        //Act
        boolean result = denominationOne.equals(name);

        //Assert
        assertFalse(result);
    }
}