package project.controllers.cli;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.dto.AddAccountToPersonRequestDTO;
import project.dto.assemblers.AddAccountToPersonRequestAssembler;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.PersonRepository;
import project.services.AddAccountToPersonService;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class AddAccountToPersonControllerTest {
    PersonID personID;
    Email personEmail;
    Person person;
    LocalDateTime birthDate;
    Description description;
    Denomination denomination;

    @Autowired
    private AddAccountToPersonController addAccountToPersonController;
    @Autowired
    private AddAccountToPersonService addAccountToPersonMockService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private PersonRepository personRepository;

    @DisplayName("addAccountToPerson - HappyPath")
    @Test
    void addAccountToPersonHappyPath() {
        //Arrange
        birthDate = LocalDateTime.of(1988, Month.MAY, 01, 0, 0, 0);
        personEmail = new Email("rosaliafernandes@gmail.com");
        personID = new PersonID(personEmail);
        person = new Person(new Name("Rosalia"), new Address("Porto"), new Date(birthDate), personEmail, null, null);

        denomination = new Denomination("Shopping");
        description = new Description("Dress to impress");

        AccountDTO expected = new AccountDTO();
        expected.setDescription(description);
        expected.setAccountID(new AccountID(denomination, personID));

        final String denomination = "Shopping";
        final String description = "Dress to impress";
        final String personEmail = "rosaliafernandes@gmail.com";

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = AddAccountToPersonRequestAssembler.addAccountMapToDTO(denomination, description, personEmail);
        Mockito.when(addAccountToPersonMockService.addAccountToPerson(addAccountToPersonRequestDTO)).thenReturn(expected);

        //Act
        AccountDTO result = addAccountToPersonController.addAccountToPerson(denomination, description, personEmail);

        //Assert
        assertEquals(expected.getAccountID(), result.getAccountID());
        assertEquals(expected.getDescription(), result.getDescription());
    }

    @DisplayName("addAccountToPerson - Account already exist in repository")
    @Test
    void addAccountToPersonThatAlreadyExist() {
        birthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        personEmail = new Email("rosaliamaria@gmail.com");
        personID = new PersonID(personEmail);
        person = new Person(new Name("Rosalia Maria de Jesus"), new Address("Porto"), new Date(birthDate), personEmail, null, null);

        final String denomination = "Shopping Account";
        final String description = "Juicy";
        final String email = "rosaliamaria@gmail.com";
        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = AddAccountToPersonRequestAssembler.addAccountMapToDTO(denomination, description, email);
        Mockito.when(addAccountToPersonMockService.addAccountToPerson(addAccountToPersonRequestDTO)).thenThrow(new IllegalArgumentException("This account already exist"));

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            addAccountToPersonController.addAccountToPerson(denomination, description, email);
        });

        String expectedMessage = "This account already exist";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @DisplayName("addAccountToPerson - Person does not exist in repository")
    @Test
    void addAccountPersonDoesntExist() {

        final String denomination = "Shoe Shopping";
        final String description = "Keep Calm";
        final String email = "rosaliamariaserta@gmail.com";
        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = AddAccountToPersonRequestAssembler.addAccountMapToDTO(denomination, description, email);
        Mockito.when(addAccountToPersonMockService.addAccountToPerson(addAccountToPersonRequestDTO)).thenThrow(new IllegalArgumentException("The Person does not exist"));

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            addAccountToPersonController.addAccountToPerson(denomination, description, email);
        });

        String expectedMessage = "The Person does not exist";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }


}