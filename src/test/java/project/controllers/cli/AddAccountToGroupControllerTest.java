package project.controllers.cli;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.dto.AddAccountToGroupRequestDTO;
import project.dto.assemblers.AddAccountToGroupRequestAssembler;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.services.AddAccountToGroupService;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class AddAccountToGroupControllerTest {
    Name p1Name;
    Address p1BirthPlace;
    Date p1BirthDate;
    Email p1Email;
    Person p1;
    PersonID p1ID;
    Name p2Name;
    Address p2BirthPlace;
    Date p2BirthDate;
    Email p2Email;
    Person p2;
    PersonID p2ID;
    Description groupDescription;
    Group g1;
    GroupID g1ID;
    Description group2Description;
    Group g2;
    GroupID g2ID;
    Description accountDescription;
    Denomination accountDenomination;
    @Autowired
    private AddAccountToGroupService addAccountToGroupMockService;
    @Autowired
    private AddAccountToGroupController addAccountToGroupController;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private AccountRepository accountRepository;

    @BeforeEach
    void setUp() {
        //Person
        p1Name = new Name("Pedro");
        p1BirthPlace = new Address("Porto");
        p1BirthDate = new Date(LocalDateTime.of(1982, 03, 14, 0, 0));
        p1Email = new Email("pedro.1234@gmail.com");
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        //Person
        p2Name = new Name("Maria");
        p2BirthPlace = new Address("Maia");
        p2BirthDate = new Date(LocalDateTime.of(1988, 10, 2, 0, 0));
        p2Email = new Email("maria.1234@gmail.com");
        p2 = new Person(p2Name, p2BirthPlace, p2BirthDate, p2Email, null, null);
        p2ID = p2.getPersonID();

        //Group repository with group
        groupDescription = new Description("Family");
        g1 = new Group(groupDescription, p1ID);
        g1ID = g1.getID();
        groupRepository.save(g1);

        //Group
        group2Description = new Description("Friends");
        g2 = new Group(group2Description, p2ID);
        g2ID = g2.getID();

        //Account parameters
        accountDenomination = new Denomination("Groceries Account");
        accountDescription = new Description("Everything Juicy");
    }

    @DisplayName("addAccount() - Happy Path")
    @Test
    void addAccountHappyPath() {
        //Arrange
        p1Name = new Name("Pedro");
        p1BirthPlace = new Address("Porto");
        p1BirthDate = new Date(LocalDateTime.of(1982, 03, 14, 0, 0));
        p1Email = new Email("pedro.1234@gmail.com");
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();
        GroupID g1ID = g1.getID();
        groupRepository.save(g1);

        Denomination accountDenomination = new Denomination("Groceries Account");

        AccountDTO expected = new AccountDTO();
        expected.setAccountID(new AccountID(accountDenomination, g1ID));

        final String denomination = "Groceries Account";
        final String description = "Everything Juicy";
        final String groupDescription = "Family";
        final String responsibleEmail = "pedro.1234@gmail.com";

        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO(denomination, description, responsibleEmail, groupDescription);
        Mockito.when(addAccountToGroupMockService.addAccountToGroup(addAccountToGroupRequestDTO)).thenReturn(expected);

        //Act
        AccountDTO result = addAccountToGroupController.addAccountToGroup(denomination, description, responsibleEmail, groupDescription);

        //Assert
        assertEquals(expected.getAccountID(), result.getAccountID());
    }

    @DisplayName("addAccountToGroup() - Test if given GroupDescription is null")
    @Test
    void addAccountToGroupNullGroupDescription() {
        //Arrange
        PersonID p1ID = p1.getPersonID();
        GroupID g1ID = g1.getID();
        groupRepository.save(g1);

        Denomination accountDenomination = new Denomination("Groceries Account");
        Description accountDescription = new Description("Family");

        AccountDTO expected = new AccountDTO();
        expected.setAccountID(new AccountID(accountDenomination, g1ID));

        final String denomination = null;
        final String description = "Everything Juicy";
        final String groupDescription = null;
        final String responsibleEmail = "pedro.1234@gmail.com";


        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO(denomination, description, groupDescription, responsibleEmail);
        });
    }

    @DisplayName("addAccountToGroup() - Test if given denomination is null")
    @Test
    void addAccountToGroupNullDenomination() {
        //Arrange
        PersonID p1ID = p1.getPersonID();
        GroupID g1ID = g1.getID();
        groupRepository.save(g1);

        Denomination accountDenomination = new Denomination("Groceries Account");
        Description accountDescription = new Description("Family");

        AccountDTO expected = new AccountDTO();
        expected.setAccountID(new AccountID(accountDenomination, g1ID));

        final String denomination = null;
        final String description = "Everything Juicy";
        final String groupDescription = "Family";
        final String responsibleEmail = "pedro.1234@gmail.com";

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO(denomination, description, responsibleEmail, groupDescription);
        });
    }

    @DisplayName("addAccountToGroup() - Test if given responsible e-Mail is null")
    @Test
    void addAccountToGroupNullResponsibleEmail() {
        //Arrange
        PersonID p1ID = p1.getPersonID();
        GroupID g1ID = g1.getID();
        groupRepository.save(g1);

        Denomination accountDenomination = new Denomination("Groceries Account");
        Description accountDescription = new Description("Family");

        AccountDTO expected = new AccountDTO();
        expected.setAccountID(new AccountID(accountDenomination, g1ID));

        final String denomination = "Groceries Account";
        final String description = "Everything Juicy";
        final String groupDescription = "Family";
        final String responsibleEmail = null;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO(denomination, description, responsibleEmail, groupDescription);
        });
    }

    @DisplayName("addAccountToGroup() - Test if given description is null")
    @Test
    void addAccountToGroupNullDescription() {
        //Arrange
        PersonID p1ID = p1.getPersonID();
        GroupID g1ID = g1.getID();
        groupRepository.save(g1);

        Denomination accountDenomination = new Denomination("Groceries Account");
        Description accountDescription = new Description("Family");

        AccountDTO expected = new AccountDTO();
        expected.setAccountID(new AccountID(accountDenomination, g1ID));

        final String denomination = "Groceries Account";
        final String description = null;
        final String groupDescription = "Family";
        final String responsibleEmail = "pedro.1234@gmail.com";

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO(denomination, description, responsibleEmail, groupDescription);
        });
    }
}