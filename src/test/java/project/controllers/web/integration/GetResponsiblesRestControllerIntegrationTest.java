package project.controllers.web.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.AbstractTest;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;
import project.utils.GetJsonNodeValue;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetResponsiblesRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;

    @DisplayName("getResponsibles - Happy Path")
    @Test
    @Transactional
    public void getResponsiblesHappyPath() throws Exception {
        //Arrange
        String uri = "/groups/University/responsibles/";
        Name p1Name = new Name("Pedro Castro");
        Address pedroBirthPlace = new Address("Gaia");
        Date pedroBirthDate = new Date(LocalDateTime.of(1981, 3, 14, 0, 0));
        Email p1Email = new Email("pedrocastro2@gmail.com");
        Person pedro = new Person(p1Name, pedroBirthPlace, pedroBirthDate, p1Email, null, null);
        PersonID p1ID = pedro.getPersonID();
        Name p2Name = new Name("Pedro Castro");
        Address marcoBirthPlace = new Address("Gaia");
        Date marcoBirthDate = new Date(LocalDateTime.of(1981, 3, 14, 0, 0));
        Email p2Email = new Email("marcoalmendres@gmail.com");
        Person marco = new Person(p2Name, marcoBirthPlace, marcoBirthDate, p2Email, null, null);
        PersonID p2ID = marco.getPersonID();
        personRepository.save(marco);
        personRepository.save(pedro);

        //Group repository with group
        Description groupUniversityDescription = new Description("University");
        Group g1 = new Group(groupUniversityDescription, p1ID);
        groupRepository.save(g1);
        String groupDescription = "University";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals(200, status);
        assertEquals("[{\"person\":\"pedrocastro2@gmail.com\",\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost/responsibles/pedrocastro2@gmail.com\"}]}]", content);
    }

    @DisplayName("getResponsibles - Group not Found")
    @Test
    @Transactional
    public void getResponsiblesGroupNotFound() throws Exception {
        //Arrange
        String uri = "/groups/University123/responsibles/";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);
        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("Please select an existing GroupID", GetJsonNodeValue.nodeAsString(content, "message"));
        assertEquals("UNPROCESSABLE_ENTITY", GetJsonNodeValue.nodeAsString(content, "status"));
    }
}
