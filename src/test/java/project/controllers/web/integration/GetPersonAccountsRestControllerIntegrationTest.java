package project.controllers.web.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.AbstractTest;
import project.model.account.Account;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetPersonAccountsRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PersonRepository personRepository;

    @DisplayName("getPersonAccounts - Happy Path")
    @Test
    public void getPersonAccountsHappyPath() throws Exception {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));

        // Family with two sons
        Name name = new Name("Maria");
        Email mariaEmail = new Email("mariaconfitada@family.com");
        Person maria = new Person(name, birthAddress, parentsBirthdate, mariaEmail, null, null);

        personRepository.save(maria);

        //ACCOUNTS
        Account account1 = new Account(new Denomination("first account"),new Description("Created to test system"),maria.getPersonID());

        accountRepository.save(account1);

        final String uri = "/persons/mariaconfitada@family.com/accounts";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();

        assertTrue(content.contains("\"denomination\":\"first account\""));
        assertTrue(content.contains("\"description\":\"Created to test system\""));
        assertTrue(content.contains("\"ownerID\":\"mariaconfitada@family.com\""));
    }

    @DisplayName("getGroupAccounts - Non exising Person")
    @Test
    public void getPersonAccountsNonExistingGroup() throws Exception {
        //Arrange

        final String uri = "/persons/johndoe@missing.com/accounts";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String message = mvcResult.getResponse().getContentAsString();

        assertTrue(message.contains("\"message\":\"Please select an existing PersonID\""));
        assertTrue(message.contains("\"status\":\"UNPROCESSABLE_ENTITY\""));
    }
}
