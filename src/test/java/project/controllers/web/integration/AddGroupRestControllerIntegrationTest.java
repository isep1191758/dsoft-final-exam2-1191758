package project.controllers.web.integration;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.dto.AddGroupRequestInfoDTO;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddGroupRestControllerIntegrationTest extends AbstractTest {

    @Autowired
    GroupRepository groupRepository;

    @DisplayName("As a user, I want to create a group, becoming a group administrator -add Group - Happy path ")
    @Test
    void addGroupHappyPath() throws Exception {

        //Arrange
        final String uri = "/groups";
        final String groupDescription = "test group";
        final String responsibleEmail = "maria@gmail.com";

        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO(groupDescription, responsibleEmail);

        //Act
        String entryJson = super.mapToJson(addGroupRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String description = design.read("$.groupDescription");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(201, status);
        assertEquals("test group", description);
    }


    @DisplayName("/groups - Assert that a specific exception is thrown when the group already exists")
    @Test
    void addGroup_ExistingGroup() throws Exception {

        //Arrange
        Description description = new Description("Group2");
        Email email = new Email("maria@gmail.com");
        PersonID responsible = new PersonID(email);

        Group group2 = new Group(description, responsible);

        groupRepository.save(group2);

        final String uri = "/groups";

        final String groupDescription = "Group2";
        final String responsibleEmail = "maria@gmail.com";

        AddGroupRequestInfoDTO addGroupRequestInfoDTO = new AddGroupRequestInfoDTO(groupDescription, responsibleEmail);

        //Act
        String entryJson = super.mapToJson(addGroupRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String message = design.read("$.message");
        String status1 = design.read("$.status");
        int status = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(422, status);
        assertEquals("Please select an nonexistent Group", message);
        assertEquals("UNPROCESSABLE_ENTITY", status1);
    }
}
