package project.controllers.web.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.AbstractTest;
import project.model.account.Account;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetGroupAccountsRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;

    @DisplayName("getGroupAccounts - Happy Path")
    @Test
    public void getGroupAccountsHappyPath() throws Exception {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("mariaconfitada@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("joseabuelas@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisiogreat@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nunohombrero@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());

        // GROUPS
        Description familyDescription = new Description("Family2");
        Group familyGroup = new Group(familyDescription, father.getPersonID());

        familyGroup.addMember(son.getPersonID());
        familyGroup.addMember(mother.getPersonID());
        familyGroup.addMember(sibling.getPersonID());

        groupRepository.save(familyGroup);

        //ACCOUNTS
        Account account1 = new Account(new Denomination("first account"),new Description("Created to test system"),familyGroup.getID());

        accountRepository.save(account1);

        final String uri = "/groups/Family2/accounts";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertTrue(content.contains("\"denomination\":\"first account\""));
        assertTrue(content.contains("\"description\":\"Created to test system\""));
        assertTrue(content.contains("\"ownerID\":\"Family2\""));
    }

    @DisplayName("getGroupAccounts - Non existing Group")
    @Test
    public void getGroupAccountsNonExistingGroup() throws Exception {
        //Arrange

        final String uri = "/groups/Family2/accounts";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String message = mvcResult.getResponse().getContentAsString();

        assertTrue(message.contains("\"message\":\"Please select an existing GroupID\""));
        assertTrue(message.contains("\"status\":\"UNPROCESSABLE_ENTITY\""));
    }
}
