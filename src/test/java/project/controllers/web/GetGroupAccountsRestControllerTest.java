package project.controllers.web;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.exceptions.NotFoundException;
import project.model.account.Account;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.services.GetGroupAccountsService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class GetGroupAccountsRestControllerTest {
    @Autowired
    GetGroupAccountsRestController getGroupAccountsRestController;
    @Autowired
    GetGroupAccountsService getGroupAccountsMockService;

    @DisplayName("getGroupAccounts - Happy Path")
    @Test
    public void getGroupAccountsHappyPath() {

        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("mariaconfitada@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("joseabuelas@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisiogreat@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nunohombrero@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());

        // GROUPS
        Description familyDescription = new Description("Family3");
        Group familyGroup = new Group(familyDescription, father.getPersonID());

        familyGroup.addMember(son.getPersonID());
        familyGroup.addMember(mother.getPersonID());
        familyGroup.addMember(sibling.getPersonID());

        //ACCOUNTS
        Account account1 = new Account(new Denomination("first account"),new Description("Created to test system"),familyGroup.getID());

        Set<AccountDTO> accountsDTO = new HashSet<>();

        AccountDTO accountDTO1 = new AccountDTO();
        accountDTO1.setAccountID(account1.getAccountID());
        accountDTO1.setDescription(account1.getDescription());

        accountsDTO.add(accountDTO1);

        Mockito.when(getGroupAccountsMockService.getGroupAccounts("Family3")).thenReturn(accountsDTO);

        //Act
        ResponseEntity<Object> resultAccountsResponseDTO = getGroupAccountsRestController.getGroupsAccounts("Family3");

        //Assert
        int status = resultAccountsResponseDTO.getStatusCodeValue();
        assertEquals(200, status);
    }

    @DisplayName("getGroupAccounts - Non existing Group")
    @Test
    public void getGroupAccountsNonExistingGroup() {

        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));

        // Family with two sons
        Name jose = new Name("José");
        Email joseEmail = new Email("joseabuelas@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        Mockito.when(getGroupAccountsMockService.getGroupAccounts("Soon to be group")).thenThrow(new NotFoundException("GroupID does not exist"));

        //Act/Assert
        Exception exception = assertThrows(NotFoundException.class, () -> getGroupAccountsRestController.getGroupsAccounts("Soon to be group"));

        assertEquals("GroupID does not exist", exception.getMessage());
    }
}

