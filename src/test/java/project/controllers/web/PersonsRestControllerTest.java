package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.PersonDTO;
import project.dto.assemblers.PersonAssembler;
import project.model.person.Person;
import project.model.shared.*;
import project.services.PersonsService;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class PersonsRestControllerTest {
    @Autowired
    private PersonsService personsMockService;
    @Autowired
    private PersonsRestController personsRestController;

    @DisplayName("getAllPersons - Happy Path")
    @Test
    void getAllPersons() {
        //Arrange
        //PersonsBirthDate
        LocalDateTime personBirthDate = LocalDateTime.of(2006, Month.DECEMBER, 12, 18, 30, 6);

        //Mother
        Email motherEmail = new Email("gestrudesmaria@gmail.com");
        PersonID motherID = new PersonID(motherEmail);
        Person mother = new Person(new Name("Gestrudes Maria"), new Address("Guimarães"), new Date(personBirthDate), motherEmail, null, null);

        //Father
        Email fatherEmail = new Email("zedamura@gmail.com");
        PersonID fatherID = new PersonID(fatherEmail);
        Person father = new Person(new Name("Jose D'Amura"), new Address("Guimarães"), new Date(personBirthDate), fatherEmail, null, null);

        //Brother
        Email brotherEmail = new Email("ruidamura@gmail.com");
        PersonID brotherID = new PersonID(brotherEmail);
        Person brother = new Person(new Name("Rui D'Amura"), new Address("Guimarães"), new Date(personBirthDate), brotherEmail, motherID, fatherID);

        //Person
        Email personEmail = new Email("valentina@gmail.com");
        Person person = new Person(new Name("Valentina D'Amura"), new Address("Guimarães"), new Date(personBirthDate), personEmail, motherID, fatherID);
        person.addSibling(brotherID);

        Set<PersonDTO> personDTOSet = new HashSet<>();
        PersonDTO personDTO = PersonAssembler.mapToDTO(person);
        PersonDTO motherDTO = PersonAssembler.mapToDTO(mother);
        PersonDTO fatherDTO = PersonAssembler.mapToDTO(father);
        PersonDTO brotherDTO = PersonAssembler.mapToDTO(brother);
        personDTOSet.add(personDTO);
        personDTOSet.add(motherDTO);
        personDTOSet.add(fatherDTO);
        personDTOSet.add(brotherDTO);

        Mockito.when(personsMockService.getPersons()).thenReturn(personDTOSet);

        //Act
        ResponseEntity<Object> result = personsRestController.getAllPersons();

        //Assert
        assertEquals(200, result.getStatusCodeValue());
    }


    @DisplayName("getAllPersons - Empty Repo")
    @Test
    void getAllPersonsEmptyRepo() {
        //Arrange
        Set<PersonDTO> personDTOSet = new HashSet<>();

        Mockito.when(personsMockService.getPersons()).thenReturn(personDTOSet);

        //Act
        ResponseEntity<Object> result = personsRestController.getAllPersons();

        //Assert
        assertEquals(200, result.getStatusCodeValue());
    }

    @DisplayName("getPersonById - Happy Path")
    @Test
    void getPersonById() {
        //Arrange
        //PersonsBirthDate
        LocalDateTime personBirthDate = LocalDateTime.of(1988, Month.DECEMBER, 12, 18, 30, 6);

        //Mother
        Email motherEmail = new Email("mariacacilda@gmail.com");
        PersonID motherID = new PersonID(motherEmail);
        Person mother = new Person(new Name("Maria Cacilda"), new Address("Guimarães"), new Date(personBirthDate), motherEmail, null, null);

        //Father
        Email fatherEmail = new Email("zeventura@gmail.com");
        PersonID fatherID = new PersonID(fatherEmail);
        Person father = new Person(new Name("Jose Ventura"), new Address("Guimarães"), new Date(personBirthDate), fatherEmail, null, null);

        //Brother
        Email brotherEmail = new Email("gilventura@gmail.com");
        PersonID brotherID = new PersonID(brotherEmail);
        Person brother = new Person(new Name("Gil Ventura"), new Address("Guimarães"), new Date(personBirthDate), brotherEmail, motherID, fatherID);

        //Person
        Email personEmail = new Email("amanda@gmail.com");
        Person person = new Person(new Name("Amanda Ventura"), new Address("Guimarães"), new Date(personBirthDate), personEmail, motherID, fatherID);
        person.addSibling(brotherID);

        PersonDTO personDTO = PersonAssembler.mapToDTO(person);

        Mockito.when(personsMockService.getPersonById("amanda@gmail.com")).thenReturn(personDTO);

        //Act
        ResponseEntity<Object> result = personsRestController.getPersonById("amanda@gmail.com");

        //Assert
        assertEquals(200, result.getStatusCodeValue());
    }
}
