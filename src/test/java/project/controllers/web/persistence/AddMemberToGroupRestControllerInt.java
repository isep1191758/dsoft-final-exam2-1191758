package project.controllers.web.persistence;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.datamodel.GroupIDData;
import project.dto.AddMemberRequestInfoDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;
import project.repositories.jpa.GroupJPARepository;
import project.repositories.jpa.PersonJPARepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddMemberToGroupRestControllerInt extends AbstractTest {
    PersonID personId;
    Person person;
    Email email;
    Person mother;
    Person father;
    LocalDateTime personBirthDate;
    LocalDateTime fatherBirthDate;
    LocalDateTime motherBirthDate;
    Group group;

    @Autowired
    PersonRepository personRepository;
    @Autowired
    PersonJPARepository personJPARepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GroupJPARepository groupJPARepository;

    @BeforeEach
    void setUpTests() {
        //Person1
        personBirthDate = LocalDateTime.of(1982, Month.JANUARY, 1, 0, 0, 0);
        motherBirthDate = LocalDateTime.of(1952, Month.DECEMBER, 15, 0, 0, 0);
        fatherBirthDate = LocalDateTime.of(1951, Month.JANUARY, 10, 0, 0, 0);

        email = new Email("mario@family.com");
        personId = new PersonID(email);

        mother = new Person(new Name("Joana"), new Address("Porto"), new Date(motherBirthDate), new Email("joana@family.com"), null, null);
        father = new Person(new Name("Pedro"), new Address("Porto"), new Date(fatherBirthDate), new Email("pedro@family.com"), null, null);
        person = new Person(new Name("Mário"), new Address("Porto"), new Date(personBirthDate), email, father.getPersonID(), mother.getPersonID());

        personRepository.save(person);

        group = new Group(new Description("University"), personId);

        groupRepository.save(group);
    }

    @DisplayName("addAccountToPerson - Happy path ")
    @Test
    void AddMemberToGroupHappyPath() throws Exception {
        //Arrange
        final String uri = "/groups/University/members";
        final String groupDescription = "University";
        final String personEmail = "marco@gmail.com";

        assertFalse(personRepository.findById(new PersonID(new Email(personEmail))).isPresent());
        assertTrue(groupRepository.findById(new GroupID(new Description(groupDescription))).isPresent());

        AddMemberRequestInfoDTO infoDTO = new AddMemberRequestInfoDTO(groupDescription, personEmail);

        //Act
        String entryJson = super.mapToJson("[{\"value\":\"marco@gmail.com\",\"label\":\"Tarcisio\"},{\"value\":\"jose@family.com\",\"label\":\"José\"},{\"value\":\"nuno@family.com\",\"label\":\"Nuno\"},{\"value\":\"maria@family.com\",\"label\":\"Maria\"}]");
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();
        DocumentContext design = JsonPath.parse(mvcResult.getResponse().getContentAsString());
        String description1 = design.read("$.groupDescription");

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
        assertEquals("University", description1);


        //Assert persistence
        assertTrue(groupRepository.findById(new GroupID(new Description(groupDescription))).isPresent());
        Group savedGroup = groupRepository.findById(group.getID()).get();
        assertTrue(savedGroup.isMemberID(new PersonID(new Email("marco@gmail.com"))));


        //Delete saved entities
        GroupIDData groupIDData = new GroupIDData("University");
        groupJPARepository.deleteById(groupIDData);
        assertFalse(groupRepository.findById(new GroupID(new Description("University"))).isPresent());
    }

}
