package project;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;
import project.services.GetGroupAccountsService;
import project.services.GroupsService;
import project.utils.ClockUtil;

@Profile("test3")
@Configuration
public class TestServerConfiguration3 {

    @Bean
    @Primary
    public GroupRepository groupMockRepository() {
        return Mockito.mock(GroupRepository.class);
    }

    @Bean
    @Primary
    public GetGroupAccountsService getGroupAccountsMockService() {
        return Mockito.mock(GetGroupAccountsService.class);
    }

    @Bean
    @Primary
    public GroupsService groupsMockService() {
        return Mockito.mock(GroupsService.class);
    }

    @Bean
    @Primary
    public AccountRepository accountMockRepository() {
        return Mockito.mock(AccountRepository.class);
    }

    @Bean
    @Primary
    public PersonRepository personMockRepository() {
        return Mockito.mock(PersonRepository.class);
    }

    @Bean
    @Primary
    public LedgerRepository ledgerMockRepository() {
        return Mockito.mock(LedgerRepository.class);
    }

    @Bean
    @Primary
    public ClockUtil clockUtilMock() {
        return Mockito.mock(ClockUtil.class);
    }
}