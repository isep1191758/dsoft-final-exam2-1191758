package project.dto;

import java.util.Objects;

public class AddGroupRequestInfoDTO {
    private String groupDescription;
    private String responsibleEmail;

    public AddGroupRequestInfoDTO(String groupDescription, String responsibleEmail) {
        this.groupDescription = groupDescription;
        this.responsibleEmail = responsibleEmail;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getResponsibleEmail() {
        return responsibleEmail;
    }

    public void setResponsibleEmail(String responsibleEmail) {
        this.responsibleEmail = responsibleEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddGroupRequestInfoDTO that = (AddGroupRequestInfoDTO) o;
        return Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(responsibleEmail, that.responsibleEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupDescription, responsibleEmail);
    }
}
