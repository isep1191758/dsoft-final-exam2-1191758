package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CategoryResponseDTO extends RepresentationModel<CategoryResponseDTO> {

    private String designation;
    private String designation1;

    public CategoryResponseDTO(String designation, String designation1) {

        this.designation = designation;
        this.designation1 = designation1;
    }

    public String getDesignation1() {
        return designation1;
    }

    public void setDesignation1(String designation1) {
        this.designation1 = designation1;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CategoryResponseDTO that = (CategoryResponseDTO) o;
        return Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }


}