package project.dto;

import project.model.shared.Email;
import project.model.shared.PersonID;

import java.util.Objects;

public class IsSiblingRequestDTO {

    private PersonID personID1;
    private PersonID personID2;

    public IsSiblingRequestDTO(String personID1, String personID2) {
        this.personID1 = new PersonID(new Email(personID1));
        this.personID2 = new PersonID(new Email(personID2));
    }

    public PersonID getPersonID1() {
        return personID1;
    }

    public void setPersonID1(PersonID personID1) {
        this.personID1 = personID1;
    }

    public PersonID getPersonID2() {
        return personID2;
    }

    public void setPersonID2(PersonID personID2) {
        this.personID2 = personID2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IsSiblingRequestDTO)) return false;
        IsSiblingRequestDTO that = (IsSiblingRequestDTO) o;
        return Objects.equals(personID1, that.personID1) &&
                Objects.equals(personID2, that.personID2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID1, personID2);
    }
}
