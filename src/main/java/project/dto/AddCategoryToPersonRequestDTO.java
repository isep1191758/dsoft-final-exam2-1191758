package project.dto;

import project.model.shared.Designation;
import project.model.shared.Designation1;
import project.model.shared.PersonID;

import java.util.Objects;

public class AddCategoryToPersonRequestDTO {
    private Designation designation;
    private Designation1 designation1;
    private PersonID personID;

    public AddCategoryToPersonRequestDTO(Designation designation, Designation1 designation1, PersonID personID) {
        this.designation = designation;
        this.designation1 = designation1;
        this.personID = personID;
    }


    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    public Designation1 getDesignation1() {
        return designation1;
    }

    public void setDesignation1(Designation1 designation1) {
        this.designation1 = designation1;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddCategoryToPersonRequestDTO)) return false;
        AddCategoryToPersonRequestDTO that = (AddCategoryToPersonRequestDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(designation1, that.designation1) &&
                Objects.equals(personID, that.personID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, designation1, personID);
    }
}
