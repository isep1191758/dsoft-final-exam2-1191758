package project.dto;

import project.model.shared.Designation;
import project.model.shared.Designation1;
import project.model.shared.GroupID;
import project.model.shared.PersonID;

import java.util.Objects;

public class AddCategoryToGroupRequestDTO {
    private Designation designation;
    private Designation1 designation1;
    private GroupID groupDescription;
    private PersonID responsibleEmail;

    public AddCategoryToGroupRequestDTO(Designation designation, Designation1 designation1, GroupID groupDescription, PersonID responsibleEmail) {
        this.designation = designation;
        this.designation1 = designation1;
        this.groupDescription = groupDescription;
        this.responsibleEmail = responsibleEmail;
    }

    public Designation1 getDesignation1() {
        return designation1;
    }

    public void setDesignation1(Designation1 designation1) {
        this.designation1 = designation1;
    }

    public GroupID getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(GroupID groupDescription) {
        this.groupDescription = groupDescription;
    }

    public PersonID getResponsibleEmail() {
        return responsibleEmail;
    }

    public void setResponsibleEmail(PersonID responsibleEmail) {
        this.responsibleEmail = responsibleEmail;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddCategoryToGroupRequestDTO)) return false;
        AddCategoryToGroupRequestDTO that = (AddCategoryToGroupRequestDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(designation1, that.designation1) &&
                Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(responsibleEmail, that.responsibleEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, designation1, groupDescription, responsibleEmail);
    }


}