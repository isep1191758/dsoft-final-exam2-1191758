package project.dto;

import project.model.shared.AccountID;
import project.model.shared.Description;

import java.util.Objects;

public class AccountDTO {
    private Description description;
    private AccountID accountID;

    public AccountDTO() {
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public AccountID getAccountID() {
        return accountID;
    }

    public void setAccountID(AccountID accountID) {
        this.accountID = accountID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDTO that = (AccountDTO) o;
        return Objects.equals(description, that.description) &&
                Objects.equals(accountID, that.accountID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, accountID);
    }
}
