package project.dto;

import project.model.shared.GroupID;
import project.model.shared.PersonID;

import java.util.Objects;

public class AddMemberRequestDTO {
    GroupID groupID;
    PersonID personID;

    public AddMemberRequestDTO(GroupID groupID, PersonID personID) {
        this.groupID = groupID;
        this.personID = personID;
    }

    public GroupID getGroupID() {
        return groupID;
    }

    public void setGroupID(GroupID groupID) {
        this.groupID = groupID;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddMemberRequestDTO that = (AddMemberRequestDTO) o;
        return Objects.equals(groupID, that.groupID) &&
                Objects.equals(personID, that.personID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupID, personID);
    }
}
