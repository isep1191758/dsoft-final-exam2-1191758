package project.dto;

import project.model.shared.Description;
import project.model.shared.PersonID;

import java.util.Objects;

public class AddGroupRequestDTO {
    private Description groupDescription;
    private PersonID personID;

    public AddGroupRequestDTO(Description groupDescription, PersonID personID) {
        this.groupDescription = groupDescription;
        this.personID = personID;
    }

    public Description getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(Description groupDescription) {
        this.groupDescription = groupDescription;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddGroupRequestDTO that = (AddGroupRequestDTO) o;
        return groupDescription.equals(that.groupDescription) &&
                personID.equals(that.personID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupDescription, personID);
    }
}
