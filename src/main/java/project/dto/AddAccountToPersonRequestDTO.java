package project.dto;

import project.model.shared.Denomination;
import project.model.shared.Description;
import project.model.shared.PersonID;

import java.util.Objects;

public class AddAccountToPersonRequestDTO {
    private Denomination denomination;
    private Description description;
    private PersonID personID;

    public AddAccountToPersonRequestDTO(Denomination denomination, Description description, PersonID personID) {
        this.denomination = denomination;
        this.description = description;
        this.personID = personID;
    }

    public Denomination getDenomination() {
        return denomination;
    }

    public void setDenomination(Denomination denomination) {
        this.denomination = denomination;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddAccountToPersonRequestDTO that = (AddAccountToPersonRequestDTO) o;
        return Objects.equals(denomination, that.denomination) &&
                Objects.equals(description, that.description) &&
                Objects.equals(personID, that.personID);
    }

    @Override
    public int hashCode() {
        int hashCode = Objects.hash(denomination, description, personID);

        return hashCode;
    }
}
