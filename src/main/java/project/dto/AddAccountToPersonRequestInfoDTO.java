package project.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize
public class AddAccountToPersonRequestInfoDTO {
    private String denomination;
    private String description;

    public AddAccountToPersonRequestInfoDTO(String denomination, String description) {
        this.denomination = denomination;
        this.description = description;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddAccountToPersonRequestInfoDTO that = (AddAccountToPersonRequestInfoDTO) o;
        return Objects.equals(denomination, that.denomination) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(denomination, description);
    }
}
