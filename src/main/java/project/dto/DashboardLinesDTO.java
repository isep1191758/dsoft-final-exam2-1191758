package project.dto;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class DashboardLinesDTO implements Comparable<DashboardLinesDTO> {
    String x;
    double y;

    public DashboardLinesDTO(String x, double y) {
        this.x = x;
        this.y = y;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DashboardLinesDTO that = (DashboardLinesDTO) o;
        return Objects.equals(x, that.x);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x);
    }

    @Override
    public String toString() {
        return "{" +
                "x:'" + x + '\'' +
                ", y:" + y +
                '}';
    }

    @Override
    public int compareTo(@NotNull DashboardLinesDTO dashboardLinesDTO) {
        return this.x.compareTo(dashboardLinesDTO.getX());
    }
}
