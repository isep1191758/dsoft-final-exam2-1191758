package project.dto;

import project.model.shared.AccountID;
import project.model.shared.PersonID;
import project.model.shared.TransactionDate;

import java.util.Objects;

public class GetPersonAccountTransactionsInPeriodRequestDTO {
    private PersonID personID;
    private AccountID accountID;
    private TransactionDate initialDate;
    private TransactionDate endDate;

    public GetPersonAccountTransactionsInPeriodRequestDTO(PersonID personID, AccountID accountID, TransactionDate initialDate, TransactionDate endDate) {
        this.personID = personID;
        this.accountID = accountID;
        this.initialDate = initialDate;
        this.endDate = endDate;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    public AccountID getAccountID() {
        return accountID;
    }

    public void setAccountID(AccountID accountID) {
        this.accountID = accountID;
    }

    public TransactionDate getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(TransactionDate initialDate) {
        this.initialDate = initialDate;
    }

    public TransactionDate getEndDate() {
        return endDate;
    }

    public void setEndDate(TransactionDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetPersonAccountTransactionsInPeriodRequestDTO)) return false;
        GetPersonAccountTransactionsInPeriodRequestDTO that = (GetPersonAccountTransactionsInPeriodRequestDTO) o;
        return Objects.equals(personID, that.personID) &&
                Objects.equals(accountID, that.accountID) &&
                Objects.equals(initialDate, that.initialDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID, accountID, initialDate, endDate);
    }
}
