package project.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize
public class AddCategoryToPersonRequestInfoDTO {
    private String designation;
    private String designation1;
    private String personID;

    public AddCategoryToPersonRequestInfoDTO(String designation, String designation1, String personID) {
        this.designation = designation;
        this.designation1 = designation1;
        this.personID = personID;
    }


    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDesignation1() {
        return designation1;
    }

    public void setDesignation1(String designation1) {
        this.designation1 = designation1;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddCategoryToPersonRequestInfoDTO)) return false;
        AddCategoryToPersonRequestInfoDTO that = (AddCategoryToPersonRequestInfoDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(designation1, that.designation1) &&
                Objects.equals(personID, that.personID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, designation1, personID);
    }
}