package project.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

/**
 * DTO Class that is used when responding to a GetMembers request
 */
public class GetMembersResponseDTO extends RepresentationModel<GetMembersResponseDTO> {
    private String person;

    public GetMembersResponseDTO(String person) {
        this.person = person;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GetMembersResponseDTO that = (GetMembersResponseDTO) o;
        return Objects.equals(person, that.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person);
    }

    @Override
    public @NotNull String toString() {
        return person;
    }
}

