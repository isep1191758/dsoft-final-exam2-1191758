package project.dto;

import org.springframework.hateoas.RepresentationModel;
import project.model.shared.*;

import java.util.Objects;


public class TransactionResponseDTO extends RepresentationModel<TransactionResponseDTO> {
    private double amount;
    private final String date;
    private String description;
    private String category;
    private String debit;
    private String credit;
    private String type;

    public TransactionResponseDTO(double amount, Description description, TransactionDate date, Category category, AccountID debit, AccountID credit, TransactionType type) {
        this.amount = amount;
        this.description = description.toString();
        this.date = date.toString();
        this.category = category.toString();
        this.debit = debit.toString();
        this.credit = credit.toString();
        this.type = type.toString();
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description.toString();
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category.toString();
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(AccountID debit) {
        this.debit = debit.toString();
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(AccountID credit) {
        this.credit = credit.toString();
    }

    public String getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type.toString();
    }

    @Override
    public String toString() {
        return "TransactionResponseDTO{" +
                "amount=" + amount +
                ", description=" + description +
                ", date=" + date +
                ", category=" + category +
                ", debit=" + debit +
                ", credit=" + credit +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionResponseDTO that = (TransactionResponseDTO) o;
        return Double.compare(that.amount, amount) == 0 &&
                Objects.equals(description, that.description) &&
                Objects.equals(date, that.date) &&
                Objects.equals(category, that.category) &&
                Objects.equals(debit, that.debit) &&
                Objects.equals(credit, that.credit) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, description, date, category, debit, credit, type);
    }
}
