package project.dto;

import project.model.shared.Designation;
import project.model.shared.Designation1;

import java.util.Objects;

public class CategoryDTO {
    private Designation designation;
    private Designation1 designation1;

    public CategoryDTO() {
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    public Designation1 getDesignation1() {
        return designation1;
    }

    public void setDesignation1(Designation1 designation1) {
        this.designation1 = designation1;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CategoryDTO)) return false;
        CategoryDTO that = (CategoryDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(designation1, that.designation1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, designation1);
    }
}
