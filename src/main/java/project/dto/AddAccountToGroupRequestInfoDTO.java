package project.dto;

import java.util.Objects;

public class AddAccountToGroupRequestInfoDTO {

    private String denomination;
    private String description;
    private String responsibleEmail;

    public AddAccountToGroupRequestInfoDTO(String denomination, String description, String responsibleEmail) {
        this.denomination = denomination;
        this.description = description;
        this.responsibleEmail = responsibleEmail;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResponsibleEmail() {
        return responsibleEmail;
    }

    public void setResponsibleEmail(String responsibleEmail) {
        this.responsibleEmail = responsibleEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddAccountToGroupRequestInfoDTO that = (AddAccountToGroupRequestInfoDTO) o;
        return Objects.equals(denomination, that.denomination) &&
                Objects.equals(description, that.description) &&
                Objects.equals(responsibleEmail, that.responsibleEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(denomination, description, responsibleEmail);
    }
}
