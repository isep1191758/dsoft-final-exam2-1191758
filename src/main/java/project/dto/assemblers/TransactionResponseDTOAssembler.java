package project.dto.assemblers;

import project.dto.TransactionDTO;
import project.dto.TransactionResponseDTO;

public class TransactionResponseDTOAssembler {

    protected TransactionResponseDTOAssembler() {
        throw new AssertionError();
    }

    public static TransactionResponseDTO mapToDTO(TransactionDTO transactionDTO) {
        return new TransactionResponseDTO(transactionDTO.getAmount(), transactionDTO.getDescription(), transactionDTO.getDate(), transactionDTO.getCategory(), transactionDTO.getDebit(), transactionDTO.getCredit(), transactionDTO.getType());
    }
}
