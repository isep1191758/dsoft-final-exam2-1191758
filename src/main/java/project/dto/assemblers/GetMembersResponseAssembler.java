package project.dto.assemblers;

import project.dto.GetMembersResponseDTO;
import project.dto.PersonDTO;

/**
 * Assembler for GetMembersResponseDTO
 */
public class GetMembersResponseAssembler {

    protected GetMembersResponseAssembler() {
        throw new AssertionError();
    }

    public static GetMembersResponseDTO mapToDTO(PersonDTO personDTO) {
        return new GetMembersResponseDTO(personDTO.getPersonID().getEmail().toString());
    }
}


