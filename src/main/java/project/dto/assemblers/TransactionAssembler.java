package project.dto.assemblers;

import project.dto.TransactionDTO;
import project.model.ledger.Transaction;

public class TransactionAssembler {

    protected TransactionAssembler() {
        throw new AssertionError();
    }

    public static TransactionDTO mapToDTO(Transaction transaction) {
        TransactionDTO transactionDTO = new TransactionDTO();

        transactionDTO.setAmount(transaction.getAmount());
        transactionDTO.setCategory(transaction.getCategory());
        transactionDTO.setCredit(transaction.getCredit());
        transactionDTO.setDate(transaction.getDate());
        transactionDTO.setDebit(transaction.getDebit());
        transactionDTO.setDescription(transaction.getDescription());
        transactionDTO.setType(transaction.getType());

        return transactionDTO;
    }
}

