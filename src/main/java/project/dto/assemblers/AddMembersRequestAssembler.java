package project.dto.assemblers;

import project.dto.AddMembersRequestDTO;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.GroupID;
import project.model.shared.PersonID;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AddMembersRequestAssembler {
    public static AddMembersRequestDTO mapToDTO(String groupDescription, List<String> personEmails) {
        GroupID groupID = new GroupID(new Description(groupDescription));
        Set<PersonID> members = new HashSet<>();

        for (String personEmail : personEmails) {
            PersonID personID = new PersonID(new Email(personEmail));
            members.add(personID);
        }

        return new AddMembersRequestDTO(groupID, members);
    }
}
