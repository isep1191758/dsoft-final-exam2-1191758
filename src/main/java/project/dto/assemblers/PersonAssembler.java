package project.dto.assemblers;

import project.dto.PersonDTO;
import project.model.person.Person;

public class PersonAssembler {

    protected PersonAssembler() {
        throw new AssertionError();
    }

    public static PersonDTO mapToDTO(Person person) {
        PersonDTO personDTO = new PersonDTO();

        personDTO.setPersonID(person.getPersonID());
        personDTO.setName(person.getName());
        personDTO.setBirthPlace(person.getBirthPlace());
        personDTO.setBirthDate(person.getBirthDate());
        personDTO.setMother(person.getMother());
        personDTO.setFather(person.getFather());
        personDTO.setSiblings(person.getSiblings());
        personDTO.setCategories(person.getCategories());

        return personDTO;
    }
}
