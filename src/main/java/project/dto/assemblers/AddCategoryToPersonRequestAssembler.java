package project.dto.assemblers;

import project.dto.AddCategoryToPersonRequestDTO;
import project.model.shared.Designation;
import project.model.shared.Designation1;
import project.model.shared.Email;
import project.model.shared.PersonID;

public class AddCategoryToPersonRequestAssembler {
    /**
     * This will mapTo a RequestDTO with information from web and transforms it within CLI
     *
     * @param designation String parameter from JSON
     * @param personID    String parameter from JSON
     * @return AddCategoryEntryDTO(adesignation, gpID, pID)
     */
    public static AddCategoryToPersonRequestDTO mapToDTO(String designation, String personID) {
        AddCategoryToPersonRequestAssembler addCategoryToPersonRequestAssembler = new AddCategoryToPersonRequestAssembler();
        Designation adesignation = new Designation(designation);
        Designation1 bdesignation = new Designation1(designation);
        PersonID pID = new PersonID(new Email(personID));

        return new AddCategoryToPersonRequestDTO(adesignation, bdesignation, pID);
    }
}