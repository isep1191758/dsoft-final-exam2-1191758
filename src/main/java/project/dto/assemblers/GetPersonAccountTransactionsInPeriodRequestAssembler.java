package project.dto.assemblers;

import project.dto.GetPersonAccountTransactionsInPeriodRequestDTO;
import project.model.shared.*;

public class GetPersonAccountTransactionsInPeriodRequestAssembler {

    protected GetPersonAccountTransactionsInPeriodRequestAssembler() {
        throw new AssertionError();
    }

    public static GetPersonAccountTransactionsInPeriodRequestDTO mapToDTO(String personID, String denomination, String initialDate, String finalDate) {
        PersonID personIDTrans = new PersonID(new Email(personID));
        TransactionDate initialDateTrans = new TransactionDate(initialDate);
        TransactionDate finalDateTrans = new TransactionDate(finalDate);
        Denomination denominationAccount = new Denomination(denomination);
        AccountID accountIDtrans = new AccountID(denominationAccount, personIDTrans);

        return new GetPersonAccountTransactionsInPeriodRequestDTO(personIDTrans, accountIDtrans, initialDateTrans, finalDateTrans);
    }
}

