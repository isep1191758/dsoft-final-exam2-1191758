package project.dto.assemblers;

import project.dto.GroupResponseDTO;

import java.util.List;

public class GroupResponseAssembler {

    protected GroupResponseAssembler() {
        throw new AssertionError();
    }

    public static GroupResponseDTO mapToDTO(String groupDescription, List<String> responsibles, List<String> members, List<String> categories) {
        return new GroupResponseDTO(groupDescription, responsibles, members, categories);
    }
}