package project.dto.assemblers;

import project.dto.GetGroupAccountTransactionsInPeriodRequestDTO;
import project.model.shared.*;

public class GetGroupAccountTransactionsInPeriodRequestAssembler {

    protected GetGroupAccountTransactionsInPeriodRequestAssembler() {
        throw new AssertionError();
    }

    public static GetGroupAccountTransactionsInPeriodRequestDTO mapToDTO(String personID, String groupID, String denomination, String initialDate, String finalDate) {
        PersonID memberID = new PersonID(new Email(personID));
        GroupID groupDescription = new GroupID(new Description(groupID));
        TransactionDate initialDateTrans = new TransactionDate(initialDate);
        TransactionDate finalDateTrans = new TransactionDate(finalDate);
        Denomination denominationAccount = new Denomination(denomination);
        AccountID accountIDtrans = new AccountID(denominationAccount, groupDescription);

        return new GetGroupAccountTransactionsInPeriodRequestDTO(memberID, groupDescription, accountIDtrans, initialDateTrans, finalDateTrans);
    }
}

