package project.dto.assemblers;

import project.dto.AddMemberRequestDTO;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.GroupID;
import project.model.shared.PersonID;

public class AddMemberRequestAssembler {
    protected AddMemberRequestAssembler() {
        throw new AssertionError();
    }

    public static AddMemberRequestDTO mapToDTO(String groupDescription, String personEmail) {
        GroupID groupID = new GroupID(new Description(groupDescription));
        PersonID personID = new PersonID(new Email(personEmail));

        return new AddMemberRequestDTO(groupID, personID);
    }
}
