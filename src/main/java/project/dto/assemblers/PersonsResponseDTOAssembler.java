package project.dto.assemblers;

import project.dto.PersonDTO;
import project.dto.PersonsResponseDTO;

public class PersonsResponseDTOAssembler {

    protected PersonsResponseDTOAssembler() {
        throw new AssertionError();
    }

    public static PersonsResponseDTO mapToDTO(PersonDTO personsDTO) {

        String father = personsDTO.getFather() == null ? "" : personsDTO.getFather().toString();
        String mother = personsDTO.getMother() == null ? "" : personsDTO.getMother().toString();

        PersonsResponseDTO personsResponseDTO = new PersonsResponseDTO(personsDTO.getPersonID().toString(),
                personsDTO.getName().toString(),
                personsDTO.getBirthDate().toString(),
                personsDTO.getBirthPlace().toString(),
                mother,
                father,
                personsDTO.getSiblings().toString(),
                personsDTO.getCategories().toString());

        return personsResponseDTO;
    }
}