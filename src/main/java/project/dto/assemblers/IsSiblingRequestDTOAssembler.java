package project.dto.assemblers;

import project.dto.IsSiblingRequestDTO;

public class IsSiblingRequestDTOAssembler {

    protected IsSiblingRequestDTOAssembler() {
        throw new AssertionError();
    }

    public static IsSiblingRequestDTO mapToDTO(String aPersonEmail, String aSecondPersonEmail) {
        return new IsSiblingRequestDTO(aPersonEmail, aSecondPersonEmail);
    }
}
