package project.dto.assemblers;

import project.dto.CategoryDTO;
import project.model.shared.Category;

public final class CategoryAssembler {

    protected CategoryAssembler() {
        throw new AssertionError();
    }

    public static CategoryDTO mapToDTO(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();

        categoryDTO.setDesignation(category.getDesignation());

        return categoryDTO;
    }
}