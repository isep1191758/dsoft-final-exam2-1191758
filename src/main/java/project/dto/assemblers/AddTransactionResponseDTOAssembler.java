package project.dto.assemblers;

import project.dto.AddTransactionResponseDTO;
import project.dto.TransactionDTO;

public class AddTransactionResponseDTOAssembler {

    protected AddTransactionResponseDTOAssembler() {
        throw new AssertionError();
    }

    public static AddTransactionResponseDTO mapToDTO(TransactionDTO transactionDTO) {

        return new AddTransactionResponseDTO(transactionDTO.getAmount(), transactionDTO.getDescription().toString(), transactionDTO.getDate(), transactionDTO.getCategory().toString(), transactionDTO.getDebit().toString(), transactionDTO.getCredit().toString(), transactionDTO.getType());


    }
}
