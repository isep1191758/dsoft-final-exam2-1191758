package project.dto.assemblers;

import project.dto.AddAccountToGroupRequestDTO;
import project.model.shared.*;

public class AddAccountToGroupRequestAssembler {

    protected AddAccountToGroupRequestAssembler() {
        throw new AssertionError();
    }

    public static AddAccountToGroupRequestDTO mapToDTO(String denomination, String description, String responsibleEmail, String groupDescription) {
        Denomination accDenomination = new Denomination(denomination);
        Description accDescription = new Description(description);
        PersonID pID = new PersonID(new Email(responsibleEmail));
        GroupID gpID = new GroupID(new Description(groupDescription));

        return new AddAccountToGroupRequestDTO(accDenomination, accDescription, pID, gpID);
    }
}
