package project.dto;

import project.model.shared.Category;
import project.model.shared.Date;
import project.model.shared.GroupID;
import project.model.shared.PersonID;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class GroupDTO {
    private GroupID groupID;
    private Set<PersonID> responsibles;
    private Set<PersonID> members;
    private Set<Category> categories;
    private Date creationDate;

    public GroupDTO() {
    }

    public GroupID getGroupID() {
        return groupID;
    }

    public void setGroupID(GroupID groupID) {
        this.groupID = groupID;
    }

    public Set<PersonID> getResponsibles() {
        return new HashSet<>(this.responsibles);
    }

    public void setResponsibles(Set<PersonID> responsibles) {
        this.responsibles = new HashSet<>(responsibles);
    }

    public Set<PersonID> getMembers() {
        return new HashSet<>(this.members);
    }

    public void setMembers(Set<PersonID> members) {
        this.members = new HashSet<>(members);
    }

    public Set<Category> getCategories() {
        return new HashSet<>(this.categories);
    }

    public void setCategories(Set<Category> categories) {
        this.categories = new HashSet<>(categories);
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creatDate) {
        this.creationDate = creatDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupDTO groupDTO = (GroupDTO) o;
        return Objects.equals(groupID, groupDTO.groupID) &&
                Objects.equals(responsibles, groupDTO.responsibles) &&
                Objects.equals(members, groupDTO.members) &&
                Objects.equals(categories, groupDTO.categories) &&
                Objects.equals(creationDate, groupDTO.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupID, responsibles, members, categories, creationDate);
    }
}
