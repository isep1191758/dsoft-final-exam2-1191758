package project.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize
public class AddCategoryToGroupRequestInfoDTO {
    private String designation;
    private String designation1;
    private String groupDescription;
    private String responsibleEmail;

    public AddCategoryToGroupRequestInfoDTO(String designation, String designation1, String groupDescription, String responsibleEmail) {
        this.designation = designation;
        this.designation1 = designation1;
        this.groupDescription = groupDescription;
        this.responsibleEmail = responsibleEmail;
    }

    public String getDesignation1() {
        return designation1;
    }

    public void setDesignation1(String designation1) {
        this.designation1 = designation1;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getPersonID() {
        return responsibleEmail;
    }

    public void setPersonID(String personID) {
        this.responsibleEmail = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddCategoryToGroupRequestInfoDTO that = (AddCategoryToGroupRequestInfoDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(designation1, that.designation1) &&
                Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(responsibleEmail, that.responsibleEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, designation1, groupDescription, responsibleEmail);
    }


}