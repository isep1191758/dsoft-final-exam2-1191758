package project.model.ledger;

import project.model.shared.LedgerID;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Ledger {
    private final LedgerID ledgerID;
    private final Set<Transaction> transactions = new HashSet<>();

    public Ledger(LedgerID ledgerID) {
        this.ledgerID = ledgerID;
    }

    public LedgerID getLedgerID() {
        return ledgerID;
    }

    /**
     * Create a transaction by passing transaction information.
     *
     * @return Transaction
     */
    public Transaction addTransaction(Transaction transaction) {
        transactions.add(transaction);
        return transaction;
    }

    /**
     * Get all transactions in ledger
     *
     * @return
     */
    public Set<Transaction> getTransactions() {
        return new HashSet<>(transactions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ledger c = (Ledger) o;
        return c.ledgerID == this.ledgerID;
    }


    @Override
    public int hashCode() {
        return Objects.hash(ledgerID);
    }
}
