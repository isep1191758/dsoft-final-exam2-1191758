package project.model.shared;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class OwnerID implements Serializable {
    private static final long serialVersionUID = 3489169914181983L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    public OwnerID() {
    }

    public OwnerID(long id) {
        this.id = id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OwnerID ownerID = (OwnerID) o;
        return id == ownerID.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
