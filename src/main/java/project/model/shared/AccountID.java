package project.model.shared;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.NoArgsConstructor;
import project.model.framework.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor

@JsonDeserialize
@Embeddable
public class AccountID implements ValueObject<AccountID>, Serializable {
    private static final long serialVersionUID = 48916991423483L;
    private Denomination denomination;
    private OwnerID ownerID;

    public AccountID(Denomination denomination, OwnerID ownerID) {
        setDenominationIfValid(denomination);
        setOwnerIDIfValid(ownerID);
    }

    private void setDenominationIfValid(Denomination denomination) {
        if (denomination == null) {
            throw new NullPointerException("The denomination cannot be null");
        }
        this.denomination = denomination;
    }

    private void setOwnerIDIfValid(OwnerID ownerID) {
        if (ownerID == null) {
            throw new NullPointerException("The ownerID cannot be null");
        }
        this.ownerID = ownerID;
    }

    public OwnerID getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(OwnerID ownerID) {
        this.ownerID = ownerID;
    }

    public Denomination getDenomination() {
        return denomination;
    }

    public void setDenomination(Denomination denomination) {
        this.denomination = denomination;
    }

    @Override
    public int hashCode() {
        return Objects.hash(denomination, ownerID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountID accountID = (AccountID) o;
        return Objects.equals(denomination, accountID.denomination) &&
                Objects.equals(ownerID, accountID.ownerID);
    }

    @Override
    public String toString() {
        return denomination.toString();
    }

    @Override
    public boolean sameValueAs(AccountID other) {
        return other != null && this.denomination.equals(other.denomination) && this.ownerID.equals(other.ownerID);
    }
}

