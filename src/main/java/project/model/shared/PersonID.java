package project.model.shared;

import project.model.framework.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PersonID extends OwnerID implements ValueObject<PersonID>, Serializable {
    private static final long serialVersionUID = -31034092529989L;
    private Email email;

    /**
     * Constructor PersonID
     *
     * @param email
     */
    public PersonID(Email email) {
        setEmailIfValid(email);
    }

    public PersonID() {
    }

    private void setEmailIfValid(Email email) {
        if (email == null) {
            throw new NullPointerException("The email cannot be null");
        }
        this.email = email;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return email.toString();
    }

    @Override
    public boolean sameValueAs(PersonID other) {
        return other != null && this.email.equals(other.email);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersonID personID = (PersonID) o;
        return Objects.equals(email, personID.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}