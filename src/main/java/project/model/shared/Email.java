package project.model.shared;

import project.exceptions.EmptyException;
import project.model.framework.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Embeddable
public class Email implements ValueObject<Email>, Serializable {
    private static final long serialVersionUID = 7775664748L;
    private String email;

    public Email(String email) {
        setIsEmailValid(email);
    }

    public Email() {
    }

    /**
     * Returns the email value
     *
     * @return email The email value
     */
    public String getEmail() {
        return email;
    }

    /**
     * Verifies if provided email is null, empty or don't have the right format
     *
     * @param email
     */
    public void setIsEmailValid(String email) {
        if (email == null) {
            throw new NullPointerException("E-mail cannot be null");
        }
        if (email.trim().isEmpty()) {
            throw new EmptyException("Email cannot be empty");
        }

        Pattern regexPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z-.)]+\\.[(a-zA-z)]{2,3}$");
        Matcher regMatcher = regexPattern.matcher(email);
        if (regMatcher.matches()) {
            this.email = email.trim();
        } else {
            throw new IllegalArgumentException("E-mail is invalid");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email email1 = (Email) o;
        return Objects.equals(email, email1.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    @Override
    public String toString() {
        return email;
    }

    @Override
    public boolean sameValueAs(Email other) {
        return other != null && this.email.equals(other.email);
    }
}
