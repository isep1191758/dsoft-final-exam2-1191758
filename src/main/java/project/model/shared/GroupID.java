package project.model.shared;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.NoArgsConstructor;
import project.model.framework.ValueObject;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor

@JsonDeserialize
@Embeddable
public class GroupID extends OwnerID implements ValueObject<GroupID>, Serializable {
    private static final long serialVersionUID = -790379680481466L;
    @EmbeddedId
    private Description description;

    /**
     * Constructor GroupID
     *
     * @param description
     */
    public GroupID(Description description) {
        if (description == null) {
            throw new NullPointerException();
        }
        this.description = description;
    }

    public Description getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupID groupID = (GroupID) o;
        return Objects.equals(description, groupID.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }

    @Override
    public boolean sameValueAs(GroupID other) {
        return other != null && this.description.equals(other.description);
    }

    @Override
    public String toString() {
        return description.toString();
    }
}