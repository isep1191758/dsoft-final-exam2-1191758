package project.model.shared;

import project.model.framework.ValueObject;

public class DateInterval implements ValueObject<DateInterval> {
    private Date initialDate;
    private Date endDate;

    public DateInterval(Date initialDate, Date endDate) {
        setDatesIfValid(initialDate, endDate);
    }

    /**
     * Checks if parameters are null or if end date is before initial date and defines the attributes with the values
     * received.
     *
     * @param initialDate first date of time period
     * @param endDate     last date of time period
     */
    private void setDatesIfValid(Date initialDate, Date endDate) {
        if (initialDate == null || endDate == null) {
            throw new NullPointerException("The date cannot be null");
        }
        if (endDate.getDate().isBefore(initialDate.getDate())) {
            throw new IllegalArgumentException("The end date must be after the initial date");
        }
        this.initialDate = initialDate;
        this.endDate = endDate;
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @Override
    public boolean sameValueAs(DateInterval other) {
        return other != null && this.initialDate.equals(other.initialDate) && this.endDate.equals(other.endDate);
    }
}
