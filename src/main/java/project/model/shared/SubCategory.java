package project.model.shared;

import project.model.framework.ValueObject;

import java.util.Objects;

public class SubCategory implements ValueObject<SubCategory> {
    private Designation designation;
    private Designation1 designation1;

    public Designation getDesignation() {
        return designation;
    }

    public Designation1 getDesignation1() {
        return designation1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SubCategory)) return false;
        SubCategory that = (SubCategory) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(designation1, that.designation1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, designation1);
    }

    @Override
    public boolean sameValueAs(SubCategory other) {
        return false;
    }
}







