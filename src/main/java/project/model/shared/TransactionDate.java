package project.model.shared;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import project.model.framework.ValueObject;

import java.time.LocalDateTime;
import java.util.Objects;

@NoArgsConstructor
@Component
public class TransactionDate implements ValueObject<TransactionDate> {
    private LocalDateTime date;

    public TransactionDate(LocalDateTime date) {
        setIsDateValid(date);
    }

    public TransactionDate(String date) {
        this.date = convertFromString(date);
    }

    private LocalDateTime convertFromString(String date) {
        String[] firstSplit = date.split("T");
        String[] dateSplit = firstSplit[0].split("-");
        String[] hourSplit = firstSplit[1].split(":");
        String[] miliSplit = new String[2];

        if (hourSplit[2].contains(".")) {
            miliSplit = hourSplit[2].split("\\.");
        } else {
            miliSplit[0] = hourSplit[2];
        }

        return LocalDateTime.of(Integer.parseInt(dateSplit[0]),
                Integer.parseInt(dateSplit[1]),
                Integer.parseInt(dateSplit[2]),
                Integer.parseInt(hourSplit[0]),
                Integer.parseInt(hourSplit[1]),
                Integer.parseInt(miliSplit[0]));
    }

    private void setIsDateValid(LocalDateTime date) {
        if (date != null) {
            this.date = date;
        } else {
            throw new NullPointerException("Date can't be null");
        }
    }

    public LocalDateTime getDate() {
        return date;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TransactionDate date1 = (TransactionDate) o;
        return date.equals(date1.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }

    @Override
    public boolean sameValueAs(TransactionDate other) {
        return other != null && this.date.equals(other.date);
    }

    @Override
    public String toString() {
        return date.toString();
    }
}
