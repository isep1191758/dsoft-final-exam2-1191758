package project.model.shared;

import project.model.framework.ValueObject;

import java.util.Objects;

public class Category implements ValueObject<Category> {
    private final Designation designation;
    private final Designation1 designation1;

    public Category(Designation designation, Designation1 designation1) {
        if (designation == null) {
            throw new IllegalArgumentException("Parameter cannot be null");
        }
        if (designation1 == null) {
            throw new IllegalArgumentException("Parameter cannot be null");
        }
        this.designation = designation;
        this.designation1 = designation1;
    }

    public Designation getDesignation() {
        return designation;
    }

    public Designation1 getDesignation1() {
        return designation1;
    }

    @Override
    public boolean sameValueAs(Category other) {
        return other != null && this.designation.equals(other.designation) && this.designation1.equals(other.designation1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;
        Category category = (Category) o;
        return Objects.equals(designation, category.designation) &&
                Objects.equals(designation1, category.designation1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, designation1);
    }


}