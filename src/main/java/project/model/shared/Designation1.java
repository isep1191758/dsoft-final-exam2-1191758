package project.model.shared;

import project.exceptions.EmptyException;
import project.model.framework.ValueObject;

import java.util.Objects;

public class Designation1 implements ValueObject<Designation1> {
    private String designation1;

    public Designation1(String designation1) {
        setIsDesignationValid(designation1);
    }

    /**
     * Verifies if provided designation is null or empty
     *
     * @param designation1
     */
    private void setIsDesignationValid(String designation1) {
        if (designation1 == null) {
            throw new NullPointerException("Designation cannot be null");
        }
        if (designation1.trim().isEmpty()) {
            throw new EmptyException("Designation cannot be empty");
        } else {
            this.designation1 = designation1.trim();
        }
    }

    public String getDesignation1() {
        return designation1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Designation1 that = (Designation1) o;
        return Objects.equals(designation1, that.designation1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation1);
    }

    @Override
    public boolean sameValueAs(Designation1 other) {
        return other != null && this.designation1.equals(other.designation1);
    }

    @Override
    public String toString() {
        return designation1;
    }
}
