package project.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.datamodel.PersonData;
import project.datamodel.assemblers.PersonDomainDataAssembler;
import project.model.framework.Repository;
import project.model.person.Person;
import project.model.shared.PersonID;
import project.repositories.jpa.PersonJPARepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class PersonRepository implements Repository {
    @Autowired
    PersonJPARepository personJPARepository;

    /**
     * Saves an entity either by creating it or updating it in the persistence
     * store. The reference is no
     * longer valid and use the returned object instead. e.g.,
     *
     * @param entity
     * @return the object reference to the persisted object.
     */
    @Override
    public Person save(Object entity) {
        Person newPerson = (Person) entity;

        PersonData personData = PersonDomainDataAssembler.toData(newPerson);
        PersonData savedGroup = personJPARepository.save(personData);

        return PersonDomainDataAssembler.toDomain(savedGroup);
    }

    /**
     * Gets all entities from the repository.
     *
     * @return all entities from the repository
     */
    @Override
    public Iterable<Person> findAll() {
        Iterable<PersonData> allPersonsData = personJPARepository.findAll();
        List<Person> domainPersons = new ArrayList<>();
        for (PersonData personData : allPersonsData) {
            domainPersons.add(PersonDomainDataAssembler.toDomain(personData));
        }
        return domainPersons;
    }

    /**
     * Gets the entity with the specified primary key.
     *
     * @param id
     * @return the entity with the specified primary key
     */
    @Override
    public Optional<Person> findById(Object id) {
        PersonID personIdToSearch = (PersonID) id;
        Optional<Person> foundPerson = Optional.empty();
        Optional<PersonData> jpaPerson = personJPARepository.findById(personIdToSearch);

        if (jpaPerson.isPresent()) {
            PersonData personData = jpaPerson.get();
            foundPerson = Optional.of(PersonDomainDataAssembler.toDomain(personData));
        }
        return foundPerson;
    }

    public void deleteAll() {
        personJPARepository.deleteAll();
    }
}
