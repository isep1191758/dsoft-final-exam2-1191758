package project.datamodel.assemblers;

import org.springframework.stereotype.Service;
import project.datamodel.LedgerData;
import project.datamodel.TransactionData;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.util.Set;

@Service
public class LedgerDomainDataAssembler {
    public static LedgerData toData(Ledger ledger) {
        System.out.println("LedgerID:" + ledger.getLedgerID().toString());
        LedgerData ledgerData = new LedgerData(ledger.getLedgerID());
        Set<Transaction> transactions = ledger.getTransactions();

        for (Transaction iterator : transactions) {
            ledgerData.setTransaction(new TransactionData(iterator.getAmount(),
                    iterator.getDescription(),
                    iterator.getDate(),
                    iterator.getCategory().getDesignation().getDesignation(),
                    iterator.getDebit().getDenomination().toString(),
                    iterator.getCredit().getDenomination().toString(),
                    iterator.getType().name(), ledgerData)
            );
        }

        return ledgerData;
    }

    public static Ledger toDomain(LedgerData ledgerData) {
        LedgerID ledgerID = new LedgerID(ledgerData.getLedgerID().getId());
        Ledger ledger = new Ledger(ledgerID);

        if (ledgerData.getTransactions().size() > 0) {
            for (TransactionData iterator : ledgerData.getTransactions()) {
                LocalDateTime localDateTime = LocalDateTime.parse(iterator.getTransactionId().getId());
                ledger.addTransaction(new Transaction(iterator.getAmount(),
                        iterator.getDescription(),
                        new TransactionDate(localDateTime),
                        new Category(new Designation(iterator.getCategory()), new Designation1(iterator.getCategory())),
                        new AccountID(new Denomination(iterator.getDebitDenomination()),
                                new OwnerID(ledgerData.getLedgerID().getId().getId())),
                        new AccountID(new Denomination(iterator.getCreditDenomination()),
                                new OwnerID(ledgerData.getLedgerID().getId().getId())),
                        TransactionType.valueOf(iterator.getType())));
            }
        }
        return ledger;
    }
}