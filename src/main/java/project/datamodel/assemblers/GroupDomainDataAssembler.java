package project.datamodel.assemblers;

import org.springframework.stereotype.Service;
import project.datamodel.*;
import project.model.group.Group;
import project.model.shared.*;

import java.time.LocalDateTime;

@Service
public class GroupDomainDataAssembler {
    public static GroupData toData(Group group) {
        GroupData groupData = new GroupData(new GroupIDData(group.getID().getDescription().toString()), group.getCreationDate().toString());
        if (group.getMembers().size() > 0) {
            for (PersonID id : group.getMembers()) {
                groupData.setMember(new MemberIDData(id.getEmail().toString()));
            }
        }
        if (group.getResponsibles().size() > 0) {
            for (PersonID id : group.getResponsibles()) {
                groupData.setResponsible(new ResponsibleIDData(id.getEmail().toString()));
            }
        }

        if (group.getCategories().size() > 0) {
            for (Category id : group.getCategories()) {
                groupData.setCategory(new GroupCategoryData(id.getDesignation().getDesignation(), groupData));
            }
        }

        return groupData;
    }

    public static Group toDomain(GroupData groupData) {

        Group group = new Group(new Description(groupData.getId().getId()), new PersonID(new Email(groupData.getResponsibles().iterator().next().getId())));

        if (groupData.getMembers().size() > 0) {
            for (MemberIDData memberIDData : groupData.getMembers()) {
                group.addMember(new PersonID(new Email(memberIDData.getId())));
            }
        }

        if (groupData.getResponsibles().size() > 0) {
            for (ResponsibleIDData responsibleIDData : groupData.getResponsibles()) {
                group.addResponsible(new PersonID(new Email(responsibleIDData.getId())));
            }
        }

        if (groupData.getCategories().size() > 0) {
            for (GroupCategoryData categoryData : groupData.getCategories()) {
                group.addCategory(new Category(new Designation(categoryData.toString()), new Designation1(categoryData.toString())));
            }
        }
        String str = groupData.getCreationDate();
        String[] dateSplit = str.split("-");
        group.setCreationDate(new Date(LocalDateTime.of(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1]), Integer.parseInt(dateSplit[2]), 0, 0, 0)));

        return group;
    }
}