package project.datamodel;

import project.model.shared.Description;
import project.model.shared.TransactionDate;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "transactions")
public class TransactionData implements Serializable {
    private static final long serialVersionUID = 21589603674571L;
    @EmbeddedId
    TransactionIDData transactionId;
    private double amount;
    private Description description;
    private String category;
    private String debitDenomination;
    private String creditDenomination;
    private String type;

    public TransactionData() {
    }

    public TransactionData(double amount, Description description, TransactionDate date, String categoryID, String debitDenomination, String creditDenomination, String type, LedgerData ledgerData) {
        this.transactionId = new TransactionIDData(date.toString(), ledgerData);
        this.amount = amount;
        this.description = description;
        this.category = categoryID;
        this.debitDenomination = debitDenomination;
        this.creditDenomination = creditDenomination;
        this.type = type;
    }

    public TransactionIDData getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(TransactionIDData transactionId) {
        this.transactionId = transactionId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDebitDenomination() {
        return debitDenomination;
    }

    public void setDebitDenomination(String debitDenomination) {
        this.debitDenomination = debitDenomination;
    }

    public String getCreditDenomination() {
        return creditDenomination;
    }

    public void setCreditDenomination(String creditDenomination) {
        this.creditDenomination = creditDenomination;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionData that = (TransactionData) o;
        return Objects.equals(transactionId, that.transactionId) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(description, that.description) &&
                Objects.equals(category, that.category) &&
                Objects.equals(debitDenomination, that.debitDenomination) &&
                Objects.equals(creditDenomination, that.creditDenomination) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, amount, description, category, debitDenomination, creditDenomination, type);
    }
}
