package project.datamodel;

import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@ToString

public class PersonCategoryIDData implements Serializable {
    private static final long serialVersionUID = 9198900360351045241L;
    private String id;
    private PersonData person;

    public PersonCategoryIDData(String id, PersonData person) {
        this.id = id;
        this.person = person;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PersonData getPerson() {
        return person;
    }

    public void setPerson(PersonData person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonCategoryIDData that = (PersonCategoryIDData) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(person, that.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, person);
    }
}

