package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AccountDTO;
import project.dto.AddAccountToGroupRequestDTO;
import project.dto.assemblers.AccountAssembler;
import project.exceptions.AlreadyExistsException;
import project.exceptions.NotFoundException;
import project.model.account.Account;
import project.model.group.Group;
import project.model.shared.AccountID;
import project.repositories.AccountRepository;
import project.repositories.GroupRepository;

import java.util.Optional;

@Service
public class AddAccountToGroupService {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    AccountRepository accountRepository;

    /**
     * @param addAccountToGroupRequestDTO
     * @return account DTO
     * Finds group and person on repository and adds new account if person is responsible
     */
    public AccountDTO addAccountToGroup(AddAccountToGroupRequestDTO addAccountToGroupRequestDTO) {
        Optional<Group> groupOptional = groupRepository.findById(addAccountToGroupRequestDTO.getGroupID());

        if (!groupOptional.isPresent()) {
            throw new NotFoundException("GroupID");
        }

        Group targetGroup = groupOptional.get();

        boolean isNotResponsible = !targetGroup.isResponsible(addAccountToGroupRequestDTO.getPersonID());

        if (isNotResponsible) {
            throw new NotFoundException("Responsible");
        }

        AccountID accountID = new AccountID(addAccountToGroupRequestDTO.getDenomination(), addAccountToGroupRequestDTO.getGroupID());
        Optional<Account> accountOptional = accountRepository.findById(accountID);

        if (accountOptional.isPresent()) {
            throw new AlreadyExistsException("That account is already in the repository");
        }

        Account account = new Account(addAccountToGroupRequestDTO.getDenomination(), addAccountToGroupRequestDTO.getDescription(), addAccountToGroupRequestDTO.getGroupID());

        Account savedAccount = accountRepository.save(account);

        return AccountAssembler.mapToDTO(savedAccount);
    }
}
