package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.IsSiblingRequestDTO;
import project.exceptions.NotFoundException;
import project.model.person.Person;
import project.repositories.PersonRepository;

import java.util.Optional;

@Service
public class IsSiblingPersonService {
    @Autowired
    private PersonRepository personRepository;

    /**
     * @param isSiblingRequestDTO
     * @return
     */
    public boolean isSiblingPerson(IsSiblingRequestDTO isSiblingRequestDTO) {
        Optional<Person> mainPersonOptional = this.personRepository.findById(isSiblingRequestDTO.getPersonID1());
        Optional<Person> secondPersonOptional = this.personRepository.findById(isSiblingRequestDTO.getPersonID2());

        Person mainPerson = mainPersonOptional.orElseThrow(() -> new NotFoundException());
        Person secondPerson = secondPersonOptional.orElseThrow(() -> new NotFoundException());

        return mainPerson.isSibling(secondPerson);
    }
}
