package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.TransactionDTO;
import project.dto.assemblers.TransactionAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GetGroupTransactionsInPeriodService {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    LedgerRepository ledgerRepository;

    public Set<TransactionDTO> getGroupTransactionsInPeriod(String groupID, String personID, String accountID, String initialDate, String endDate) {

        Set<TransactionDTO> transactionInPeriodDTOSet = new HashSet<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime localInitialDate = LocalDateTime.parse(initialDate, formatter);
        LocalDateTime localEndDate = LocalDateTime.parse(endDate, formatter);
        AccountID actualAccountID = new AccountID(new Denomination(accountID), new GroupID(new Description(groupID)));

        Optional<Person> personOptional = personRepository.findById(new PersonID(new Email(personID)));
        if (!personOptional.isPresent()) {
            throw new NotFoundException("PersonID");
        }
        Optional<Group> groupOptional = groupRepository.findById(new GroupID(new Description(groupID)));
        if (!groupOptional.isPresent()) {
            throw new NotFoundException("GroupID");
        }
        Group group = groupOptional.get();
        if (!group.isMemberID(new PersonID(new Email(personID)))) {
            throw new NotFoundException("PersonID");
        }
        Optional<Ledger> ledgerOptional = ledgerRepository.findById(new LedgerID(new GroupID(new Description(groupID))));
        Ledger ledger;

        if (ledgerOptional.isPresent()) {
            ledger = ledgerOptional.get();
        } else {
            throw new NotFoundException("LedgerID");
        }

        Set<Transaction> transactions = ledger.getTransactions();
        for (Transaction transaction : transactions) {
            if ((transaction.getDate().getDate().isAfter(localInitialDate) ||
                    transaction.getDate().getDate().isEqual(localInitialDate)) &&
                    (transaction.getDate().getDate().isBefore(localEndDate) || transaction.getDate().getDate().isEqual(localEndDate))) {
                if (transaction.getCredit().getDenomination().getDenomination().equals(actualAccountID.getDenomination().getDenomination()) ||
                        transaction.getDebit().getDenomination().getDenomination().equals(actualAccountID.getDenomination().getDenomination())) {

                    transactionInPeriodDTOSet.add(TransactionAssembler.mapToDTO(transaction));
                }
            }
        }

        return transactionInPeriodDTOSet;
    }
}

