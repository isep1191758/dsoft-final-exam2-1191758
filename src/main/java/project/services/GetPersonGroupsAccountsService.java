package project.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AccountDTO;
import project.dto.GroupDTO;
import project.model.shared.Email;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Set;

@Service
public class GetPersonGroupsAccountsService {

    @Autowired
    GroupRepository groupRepository;
    @Autowired
    private GroupsService groupsService;
    @Autowired
    private GetGroupAccountsService getGroupAccountsService;

    public Set<AccountDTO> getPersonGroupsAccounts(String personID) {

        // Set<GroupDTO> groupsDTOSet = new HashSet<>();
        //groupsService.getGroups(new PersonID(new Email(personID)));

        Set<GroupDTO> groupDTOSet = groupsService.getGroups(new PersonID(new Email(personID)));
        Set<AccountDTO> resultSet = new HashSet<>();
        for (GroupDTO iterator : groupDTOSet) {
            System.out.println(iterator.getGroupID().toString());
            resultSet.addAll(getGroupAccountsService.getGroupAccounts(iterator.getGroupID().toString()));
        }
        return resultSet;
    }
}


