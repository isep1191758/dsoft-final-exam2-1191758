package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.PersonDTO;
import project.dto.assemblers.PersonAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.Description;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GetMembersService {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    PersonRepository personRepository;

    /**
     * Finds group passed by parameter on repository and get its members
     *
     * @param groupDescription
     * @return Set<person> members
     */
    public Set<PersonDTO> getMembers(String groupDescription) {
        Set<PersonDTO> members = new HashSet<>();
        GroupID groupID = new GroupID(new Description(groupDescription));
        Group group;
        Optional<Group> groupOptional = groupRepository.findById(groupID);

        if (groupOptional.isPresent()) {
            group = groupOptional.get();
        } else {
            throw new NotFoundException("GroupID");
        }

        Set<PersonID> membersInGroup = group.getMembers();
        for (PersonID memberID : membersInGroup) {
            Optional<Person> personOptional = personRepository.findById(memberID);
            if (personOptional.isPresent()) {
                Person member = personOptional.get();
                members.add(PersonAssembler.mapToDTO(member));
            }
        }

        return members;
    }
}

