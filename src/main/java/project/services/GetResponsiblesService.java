package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.PersonDTO;
import project.dto.assemblers.PersonAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.Description;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GetResponsiblesService {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    PersonRepository personRepository;

    /**
     * Finds group passed by parameter on repository and get it's responsibles
     *
     * @param groupDescription
     * @return Set<person> responsibles
     */
    public Set<PersonDTO> getResponsibles(String groupDescription) {
        Set<PersonDTO> responsibles = new HashSet<>();
        GroupID groupID = new GroupID(new Description(groupDescription));
        Optional<Group> groupOptional = groupRepository.findById(groupID);
        Group group;

        if (groupOptional.isPresent()) {
            group = groupOptional.get();
        } else {
            throw new NotFoundException("GroupID");
        }

        Set<PersonID> responsiblesInGroup = group.getResponsibles();
        for (PersonID responsibleID : responsiblesInGroup) {
            Optional<Person> personOptional = personRepository.findById(responsibleID);
            if (personOptional.isPresent()) {
                Person responsible = personOptional.get();
                responsibles.add(PersonAssembler.mapToDTO(responsible));
            }
        }

        return responsibles;
    }
}
