package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AccountDTO;
import project.dto.assemblers.AccountAssembler;
import project.exceptions.NotFoundException;
import project.model.account.Account;
import project.model.shared.Email;
import project.model.shared.PersonID;
import project.repositories.AccountRepository;
import project.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Set;

@Service
public class GetPersonAccountsService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PersonRepository personRepository;

    public Set<AccountDTO> getPersonAccounts(String personID) {

        if (!personRepository.findById(new PersonID(new Email(personID))).isPresent()) {
            throw new NotFoundException("PersonID");
        } else {
            Iterable<Account> accountList = accountRepository.findAllByAccountID_OwnerID(new PersonID(new Email(personID)));
            Set<AccountDTO> accountDTOSet = new HashSet<>();
            for (Account account : accountList) {
                accountDTOSet.add(AccountAssembler.mapToDTO(account));
            }
            return accountDTOSet;
        }
    }
}
