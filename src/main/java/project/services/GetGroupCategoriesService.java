package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CategoryDTO;
import project.dto.assemblers.CategoryAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.shared.Category;
import project.model.shared.Description;
import project.model.shared.GroupID;
import project.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GetGroupCategoriesService {

    @Autowired
    GroupRepository groupRepository;

    public Set<CategoryDTO> getGroupCategories(String groupID) {

        Set<CategoryDTO> categoryDTOSet = new HashSet<>();

        Optional<Group> groupOptional = groupRepository.findById(new GroupID(new Description(groupID)));
        if (!groupOptional.isPresent()) {
            throw new NotFoundException("GroupID");
        }
        Group group = groupOptional.get();
        Set<Category> categories = group.getCategories();
        for (Category category : categories) {
            categoryDTOSet.add(CategoryAssembler.mapToDTO(category));
        }
        return categoryDTOSet;
    }
}
