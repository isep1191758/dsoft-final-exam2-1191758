package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.TransactionDTO;
import project.dto.assemblers.TransactionAssembler;
import project.exceptions.NotFoundException;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GetPersonTransactionsInPeriodService {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    LedgerRepository ledgerRepository;

    public Set<TransactionDTO> getPersonTransactionsInPeriod(String personID, String accountID, String initialDate, String endDate) {

        Set<TransactionDTO> transactionInPeriodDTOSet = new HashSet<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime localInitialDate = LocalDateTime.parse(initialDate, formatter);
        LocalDateTime localEndDate = LocalDateTime.parse(endDate, formatter);
        AccountID actualAccountID = new AccountID(new Denomination(accountID), new PersonID(new Email(personID)));

        Optional<Person> personOptional = personRepository.findById(new PersonID(new Email(personID)));
        if (!personOptional.isPresent()) {
            throw new NotFoundException("PersonID");
        }
        Optional<Ledger> ledgerOptional = ledgerRepository.findById(new LedgerID(new PersonID(new Email(personID))));
        if (!ledgerOptional.isPresent()) {
            throw new NotFoundException("LedgerID");
        }

        Ledger ledger = ledgerOptional.get();
        Set<Transaction> transactions = ledger.getTransactions();
        for (Transaction transaction : transactions) {
            if ((transaction.getDate().getDate().isAfter(localInitialDate) ||
                    transaction.getDate().getDate().isEqual(localInitialDate)) &&
                    (transaction.getDate().getDate().isBefore(localEndDate) || transaction.getDate().getDate().isEqual(localEndDate))) {
                if (transaction.getCredit().getDenomination().getDenomination().equals(actualAccountID.getDenomination().getDenomination()) ||
                        transaction.getDebit().getDenomination().getDenomination().equals(actualAccountID.getDenomination().getDenomination())) {
                    transactionInPeriodDTOSet.add(TransactionAssembler.mapToDTO(transaction));
                }
            }
        }

        return transactionInPeriodDTOSet;
    }
}

