package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.GroupDTO;
import project.dto.assemblers.GroupAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GroupsService {
    @Autowired
    GroupRepository groupRepository;

    public Set<GroupDTO> getGroups(PersonID personID) {
        Set<GroupDTO> groupsDTO = new HashSet<>();
        for (Group group : groupRepository.findAll()) {
            if (group.getMembers().contains(personID)) {
                groupsDTO.add(GroupAssembler.mapToDTO(group));
            }
        }
        return groupsDTO;
    }

    public GroupDTO getGroup(String groupDescription) {
        Group group = null;
        GroupID groupID = new GroupID(new Description(groupDescription));
        if (!groupRepository.findById(groupID).isPresent()) {
            throw new NotFoundException("GroupID");
        }
        Optional<Group> groupOptional = groupRepository.findById(groupID);
        if (groupOptional.isPresent()) {
            group = groupOptional.get();
        }
        return GroupAssembler.mapToDTO(group);
    }
}