package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.TransactionDTO;
import project.dto.assemblers.TransactionAssembler;
import project.exceptions.NotFoundException;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.Email;
import project.model.shared.LedgerID;
import project.model.shared.PersonID;
import project.repositories.LedgerRepository;
import project.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class GetPersonTransactionsService {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    LedgerRepository ledgerRepository;

    public Set<TransactionDTO> getPersonTransactions(String personID) {
        Set<TransactionDTO> getPersonTransactions = new HashSet<>();
        Optional<Person> personOptional = personRepository.findById(new PersonID(new Email(personID)));
        Person person;
        if (!personOptional.isPresent()) {
            throw new NotFoundException("Person");
        }
        else {
            person = personOptional.get();
        }
        Optional<Ledger> ledgerOptional = ledgerRepository.findById(new LedgerID(person.getPersonID()));
        if (!ledgerOptional.isPresent()) {
            throw new NotFoundException("LedgerID");
        }
        Ledger ledger = ledgerOptional.get();
        Set<Transaction> transactions = ledger.getTransactions();
        for (Transaction transaction : transactions) {
            getPersonTransactions.add(TransactionAssembler.mapToDTO(transaction));
        }
        return getPersonTransactions;
    }
}