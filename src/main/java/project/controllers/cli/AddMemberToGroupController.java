package project.controllers.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import project.dto.AddMemberRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.AddMemberRequestAssembler;
import project.services.AddMemberToGroupService;

@Controller
public class AddMemberToGroupController {
    @Autowired
    private AddMemberToGroupService service;

    /**
     * As a system manager, I want to add people to the group.
     *
     * @param groupDescription
     * @param personEmail
     * @return
     */
    public GroupDTO addMember(String groupDescription, String personEmail) {
        AddMemberRequestDTO addMemberRequestDTO = AddMemberRequestAssembler.mapToDTO(groupDescription, personEmail);
        return service.addMember(addMemberRequestDTO);
    }

}
