package project.controllers.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.dto.AddGroupRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.AddGroupRequestAssembler;
import project.services.AddGroupService;

@Component
public class AddGroupController {

    @Autowired
    private AddGroupService addGroupService;

    /**
     * As a user, I want to create a group, becoming its group administrator.
     *
     * @param groupDescription
     * @param responsibleEmail
     * @return create a group and add it to the GroupRepository and return a string
     */
    public GroupDTO addGroup(String groupDescription, String responsibleEmail) {
        AddGroupRequestDTO addGroupRequestDTO = AddGroupRequestAssembler.mapToDTO(groupDescription, responsibleEmail);
        return addGroupService.addGroup(addGroupRequestDTO);
    }
}

