package project.controllers.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.dto.AddCategoryToGroupRequestDTO;
import project.dto.CategoryDTO;
import project.dto.assemblers.AddCategoryToGroupRequestAssembler;
import project.services.AddSubCategoryToGroupService;

@Component
public class AddSubCategoryToGroupController {
    @Autowired
    private AddSubCategoryToGroupService service;

    /**
     * Finds group on the repository and adds new category and its subcategory, if person is responsible.
     *
     * @param groupDescription
     * @param responsibleEmail
     * @param designation
     * @param designation1
     * @return service.addCategoryToGroup(addCategoryRequestDTO)
     */
    public CategoryDTO addCategoryToGroup(String groupDescription, String responsibleEmail, String designation, String designation1) {
        AddCategoryToGroupRequestDTO addCategoryToGroupRequestDTO;
        addCategoryToGroupRequestDTO = AddCategoryToGroupRequestAssembler.addCategoryMapToDTO(designation, designation1, groupDescription, responsibleEmail);

        return service.addCategoryToGroup(addCategoryToGroupRequestDTO);
    }
}