package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.AccountDTO;
import project.dto.AccountResponseDTO;
import project.dto.assemblers.AccountResponseAssembler;
import project.services.GetGroupAccountsService;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class GetGroupAccountsRestController {

    @Autowired
    private GetGroupAccountsService service;

    @GetMapping("groups/{groupID}/accounts")
    public ResponseEntity<Object> getGroupsAccounts(@PathVariable String groupID){

        Set<AccountDTO> accountsDTO = service.getGroupAccounts(groupID);

        Set<AccountResponseDTO> accountsResponseDTO = new HashSet<>();

        for(AccountDTO iterator: accountsDTO){

            AccountResponseDTO accountResponseDTO = AccountResponseAssembler.mapToDTO(iterator);
            Link selfLink = linkTo(GetGroupAccountsRestController.class).slash("groups").slash(groupID).slash("accounts").slash(iterator.getAccountID()).withSelfRel();
            accountResponseDTO.add(selfLink);
            accountsResponseDTO.add(accountResponseDTO);
        }
        return new ResponseEntity<>(accountsResponseDTO, HttpStatus.OK);
    }
}
