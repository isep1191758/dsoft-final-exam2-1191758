package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.TransactionDTO;
import project.dto.TransactionResponseDTO;
import project.dto.assemblers.TransactionResponseDTOAssembler;
import project.services.GetGroupTransactionsInPeriodService;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class GetGroupTransactionsInPeriodRestController {
    @Autowired
    private GetGroupTransactionsInPeriodService service;

    /**
     * As a group member, I want to obtain the movements of a given account in a given period.
     *
     * @param groupID
     * @param personID
     * @param accountDenomination
     * @param initialDate
     * @param endDate
     * @return
     */
    @GetMapping("groups/{groupID}/persons/{personID}/accounts/{accountDenomination}/transactions/{initialDate}/{endDate}")
    public ResponseEntity<Object> getPersonTransactionInPeriod(@PathVariable String groupID, @PathVariable String personID, @PathVariable String accountDenomination, @PathVariable String initialDate, @PathVariable String endDate) {

        Set<TransactionDTO> transactionsInPeriodDTO = service.getGroupTransactionsInPeriod(groupID, personID, accountDenomination, initialDate, endDate);

        Set<TransactionResponseDTO> transactionsInPeriodResponseDTO = new HashSet<>();

        for (TransactionDTO iterator : transactionsInPeriodDTO) {

            TransactionResponseDTO transactionResponseDTO = TransactionResponseDTOAssembler.mapToDTO(iterator);

            Link selfLink = linkTo(GetGroupTransactionsInPeriodRestController.class).slash("groups").slash(groupID).slash("persons").slash(personID).slash("accounts").
                    slash(accountDenomination).slash("transactions").slash(iterator.getDate().getDate()).withSelfRel();

            transactionResponseDTO.add(selfLink);
            transactionsInPeriodResponseDTO.add(transactionResponseDTO);
        }
        return new ResponseEntity<>(transactionsInPeriodResponseDTO, HttpStatus.OK);
    }

}
