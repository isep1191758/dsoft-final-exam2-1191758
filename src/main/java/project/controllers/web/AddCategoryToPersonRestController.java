package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.AddCategoryToPersonRequestDTO;
import project.dto.AddCategoryToPersonRequestInfoDTO;
import project.dto.CategoryDTO;
import project.dto.CategoryResponseDTO;
import project.dto.assemblers.AddCategoryResponseAssembler;
import project.dto.assemblers.AddCategoryToPersonRequestAssembler;
import project.services.AddCategoryToPersonService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping
public class AddCategoryToPersonRestController {
    @Autowired
    private AddCategoryToPersonService service;

    @PostMapping("persons/{personID}/categories")
    public ResponseEntity<Object> addCategoryToPerson(@RequestBody AddCategoryToPersonRequestInfoDTO info, @PathVariable String personID) {
        AddCategoryToPersonRequestDTO addCategoryToPersonRequestDTO = AddCategoryToPersonRequestAssembler.mapToDTO(info.getDesignation(), info.getPersonID());

        CategoryDTO categoryDTO = service.addCategoryToPerson(addCategoryToPersonRequestDTO);

        CategoryResponseDTO categoryResponseDTO = AddCategoryResponseAssembler.mapToCategoryResponseDTO(
                categoryDTO);

        Link selfLink = linkTo(AddCategoryToPersonRestController.class).slash("Persons").slash(personID).slash("categories").withSelfRel();
        categoryResponseDTO.add(selfLink);

        return new ResponseEntity<>(categoryResponseDTO, HttpStatus.CREATED);
    }
}