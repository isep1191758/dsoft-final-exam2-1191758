package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.GetMembersResponseDTO;
import project.dto.PersonDTO;
import project.dto.assemblers.GetMembersResponseAssembler;
import project.services.GetMembersService;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class GetMembersRestController {
    @Autowired
    private GetMembersService service;

    @GetMapping("/groups/{groupDescription}/members/")
    @ResponseBody
    public ResponseEntity<Object> getMembers(@PathVariable String groupDescription) {
        Set<PersonDTO> membersDTO = service.getMembers(groupDescription);

        Set<GetMembersResponseDTO> getMembersResponseDTO = new HashSet<>();

        for (PersonDTO memberDTO : membersDTO) {
            GetMembersResponseDTO membersResponseDTO = GetMembersResponseAssembler.mapToDTO(memberDTO);
            Link selfLink = linkTo(AddGroupRestController.class).slash("members").slash(memberDTO.getPersonID().getEmail()).withSelfRel();
            membersResponseDTO.add(selfLink);
            getMembersResponseDTO.add(membersResponseDTO);
        }
        return new ResponseEntity<>(getMembersResponseDTO, HttpStatus.OK);
    }
}

