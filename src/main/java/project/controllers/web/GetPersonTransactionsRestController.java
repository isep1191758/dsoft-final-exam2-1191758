package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.TransactionDTO;
import project.dto.TransactionResponseDTO;
import project.dto.assemblers.TransactionResponseDTOAssembler;
import project.services.GetPersonTransactionsService;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class GetPersonTransactionsRestController {
    @Autowired
    private GetPersonTransactionsService service;

    /**
     * Get all transactions of person.
     *
     * @param personID
     * @return
     */
    @GetMapping("/persons/{personID}/transactions")
    public ResponseEntity<Object> getPersonTransactionInPeriod(@PathVariable String personID) {

        Set<TransactionDTO> transactionsDTO = service.getPersonTransactions(personID);

        Set<TransactionResponseDTO> transactionsResponseDTO = new HashSet<>();

        for (TransactionDTO iterator : transactionsDTO) {

            TransactionResponseDTO transactionResponseDTO = TransactionResponseDTOAssembler.mapToDTO(iterator);

            Link selfLink = linkTo(GetPersonTransactionsRestController.class).slash("person").slash(personID).slash("transactions").withSelfRel();

            transactionResponseDTO.add(selfLink);
            transactionsResponseDTO.add(transactionResponseDTO);
        }
        return new ResponseEntity<>(transactionsResponseDTO, HttpStatus.OK);
    }
}
