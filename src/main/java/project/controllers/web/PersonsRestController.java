package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.PersonDTO;
import project.dto.PersonsResponseDTO;
import project.dto.assemblers.PersonsResponseDTOAssembler;
import project.services.PersonsService;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class PersonsRestController {
    @Autowired
    private PersonsService service;

    @GetMapping("/persons")
    @ResponseBody
    public ResponseEntity<Object> getAllPersons() {

        Set<PersonDTO> personDTOSet = service.getPersons();

        Set<PersonsResponseDTO> personsResponseDTOSet = new HashSet<>();

        for (PersonDTO personDTO : personDTOSet) {
            PersonsResponseDTO personsResponseDTO = PersonsResponseDTOAssembler.mapToDTO(personDTO);
            Link selfLink = linkTo(PersonsRestController.class).slash("persons").slash(personDTO.getPersonID().getEmail()).withSelfRel();
            personsResponseDTO.add(selfLink);
            personsResponseDTOSet.add(personsResponseDTO);
        }

        return new ResponseEntity<>(personsResponseDTOSet, HttpStatus.OK);
    }


    @GetMapping("/persons/{personID}")
    @ResponseBody
    public ResponseEntity<Object> getPersonById(@PathVariable String personID) {

        PersonDTO personDTO = service.getPersonById(personID);
        PersonsResponseDTO personsResponseDTO = PersonsResponseDTOAssembler.mapToDTO(personDTO);

        Link selfLink = linkTo(PersonsRestController.class).slash("persons").slash(personID).withSelfRel();
        personsResponseDTO.add(selfLink);

        return new ResponseEntity<>(personsResponseDTO, HttpStatus.OK);
    }
}
