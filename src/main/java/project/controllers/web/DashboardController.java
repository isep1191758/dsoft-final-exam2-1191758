package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.*;
import project.model.shared.Email;
import project.model.shared.PersonID;
import project.services.GetGroupTransactionsService;
import project.services.GetPersonTransactionsService;
import project.services.GroupsService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class DashboardController {
    @Autowired
    private GetPersonTransactionsService personTransactionsService;
    @Autowired
    private GetGroupTransactionsService groupTransactionsService;
    @Autowired
    private GroupsService groupsService;

    @GetMapping("/dashboard/{personID}")
    public ResponseEntity<Object> getPersonLinks(@PathVariable String personID) {
        DashboardLinksResponseDTO dashboardLinksResponseDTO = new DashboardLinksResponseDTO();

        Link groupsLink = linkTo(GroupsRestController.class).slash("persons").slash(personID).slash("groups").withRel("groups");
        Link transactionsLink = linkTo(GroupsRestController.class).slash("persons").slash(personID).slash("transactions").withRel("transactions");
        Link accountsLink = linkTo(GroupsRestController.class).slash("persons").slash(personID).slash("accounts").withRel("accounts");
        Link accountsGroupLink = linkTo(GroupsRestController.class).slash("persons").slash(personID).slash("groups").slash("accounts").withRel("accountsGroup");
        Link categoriesLink = linkTo(GroupsRestController.class).slash("persons").slash(personID).slash("categories").withRel("categories");
        Link dashboardPieLink = linkTo(GroupsRestController.class).slash("dashboard").slash(personID).slash("pie").withRel("pie");
        Link dashboardLinesLink = linkTo(GroupsRestController.class).slash("dashboard").slash(personID).slash("lines").withRel("lines");
        dashboardLinksResponseDTO.add(groupsLink);
        dashboardLinksResponseDTO.add(transactionsLink);
        dashboardLinksResponseDTO.add(accountsLink);
        dashboardLinksResponseDTO.add(accountsGroupLink);
        dashboardLinksResponseDTO.add(categoriesLink);
        dashboardLinksResponseDTO.add(dashboardPieLink);
        dashboardLinksResponseDTO.add(dashboardLinesLink);

        List<DashboardLinksResponseDTO> dashboardLinksResponseDTOS = new ArrayList<>();
        dashboardLinksResponseDTOS.add(dashboardLinksResponseDTO);

        return new ResponseEntity<>(dashboardLinksResponseDTOS, HttpStatus.OK);
    }

    /**
     * Get all transactions of person for dashboard pie chart.
     *
     * @param personID
     * @return
     */
    @GetMapping("/dashboard/{personID}/pie")
    public ResponseEntity<Object> getPersonTransactionForPieChart(@PathVariable String personID) {

        Set<TransactionDTO> transactionsDTO = personTransactionsService.getPersonTransactions(personID);
        Set<GroupDTO> groupDTOS = groupsService.getGroups(new PersonID(new Email(personID)));

        for (GroupDTO groupDTO : groupDTOS) {
            transactionsDTO.addAll(groupTransactionsService.getGroupTransactions(groupDTO.getGroupID().toString()));
        }

        List<DashboardPieDTO> categoriesTransactionsData = new ArrayList<>();

        for (TransactionDTO iterator : transactionsDTO) {

            DashboardPieDTO dashboardPieDTO = new DashboardPieDTO(iterator.getCategory().toString(), iterator.getCategory().toString(), 1);

            if (!categoriesTransactionsData.contains(dashboardPieDTO)) {
                categoriesTransactionsData.add(dashboardPieDTO);
            } else {
                int i = new ArrayList<>(categoriesTransactionsData).indexOf(dashboardPieDTO);
                categoriesTransactionsData.get(i).setValue(categoriesTransactionsData.get(i).getValue() + 1);
            }
        }
        return new ResponseEntity<>(categoriesTransactionsData, HttpStatus.OK);
    }

    /**
     * Get all transactions of person for dashboard pie chart.
     *
     * @param personID
     * @return
     */
    @GetMapping("/dashboard/{personID}/lines")
    public ResponseEntity<Object> getPersonTransactionForlinesChart(@PathVariable String personID) {

        Set<TransactionDTO> transactionsDTO = personTransactionsService.getPersonTransactions(personID);
        Set<GroupDTO> groupDTOS = groupsService.getGroups(new PersonID(new Email(personID)));

        for (GroupDTO groupDTO : groupDTOS) {
            transactionsDTO.addAll(groupTransactionsService.getGroupTransactions(groupDTO.getGroupID().toString()));
        }

        List<DashboardLinesDTO> monthTransactionsData = new ArrayList<>();

        for (TransactionDTO iterator : transactionsDTO) {

            DashboardLinesDTO dashboardLinesDTO = new DashboardLinesDTO(iterator.getDate().toString().substring(0, 10), iterator.getAmount());

            if (!monthTransactionsData.contains(dashboardLinesDTO)) {
                monthTransactionsData.add(dashboardLinesDTO);
            } else {
                int i = new ArrayList<>(monthTransactionsData).indexOf(dashboardLinesDTO);
                monthTransactionsData.get(i).setY(monthTransactionsData.get(i).getY() + iterator.getAmount());
                System.out.println(i);
            }
        }
        Collections.sort(monthTransactionsData);
        return new ResponseEntity<>(monthTransactionsData, HttpStatus.OK);
    }
}
