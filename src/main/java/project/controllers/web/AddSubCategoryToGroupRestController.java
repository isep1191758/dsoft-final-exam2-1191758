package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.AddCategoryToGroupRequestDTO;
import project.dto.AddCategoryToGroupRequestInfoDTO;
import project.dto.CategoryDTO;
import project.dto.CategoryResponseDTO;
import project.dto.assemblers.AddCategoryResponseAssembler;
import project.dto.assemblers.AddCategoryToGroupRequestAssembler;
import project.services.AddSubCategoryToGroupService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping
public class AddSubCategoryToGroupRestController {
    @Autowired
    private AddSubCategoryToGroupService service;

    @PostMapping("groups/{groupId}/categories/subcategories")
    public ResponseEntity<Object> addCategoryToGroup(@RequestBody AddCategoryToGroupRequestInfoDTO info, @PathVariable String groupId) {
        AddCategoryToGroupRequestDTO addCategoryToGroupRequestDTO = AddCategoryToGroupRequestAssembler.addCategoryMapToDTO(info.getDesignation(),
                info.getDesignation1(),
                groupId,
                info.getPersonID());

        CategoryDTO categoryDTO;

        categoryDTO = service.addCategoryToGroup(addCategoryToGroupRequestDTO);

        CategoryResponseDTO categoryResponseDTO = AddCategoryResponseAssembler.mapToCategoryResponseDTO(
                categoryDTO);

        Link selfLink = linkTo(AddSubCategoryToGroupRestController.class).slash("groups").slash(groupId).slash("categories").slash("subcategories").slash(categoryResponseDTO.getDesignation()).withSelfRel();
        categoryResponseDTO.add(selfLink);

        return new ResponseEntity<>(categoryResponseDTO, HttpStatus.CREATED);
    }
}