package project.exceptions;

public class EmptyException extends RuntimeException {
    static final long serialVersionUID = 3435346545655675L;

    public EmptyException(String errorMessage) {
        super(errorMessage);
    }

    public EmptyException() {
        super();
    }
}



