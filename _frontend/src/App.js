import React from "react";
import {Router} from "react-router-dom";

import history from "./services/history";
import Routes from "./routes";
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
    const theme = React.useMemo(
        () =>
            createMuiTheme({
                palette: {
                    type: 'light',
                },
            }),
        [],
    );
    return (
        <Router history={history}>
            <ThemeProvider theme={theme}>
                <CssBaseline/>
                <ToastContainer/>
                <Routes/>
            </ThemeProvider>
        </Router>
    );
}

export default App;
