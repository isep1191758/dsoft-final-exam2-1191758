import React from 'react';
import PropTypes from "prop-types";
import {Provider} from './AppContext';
import reducer from '../reducers/Reducer';
import createPersistedReducer from 'use-persisted-reducer';

const usePersistedReducer = createPersistedReducer('state');

const initialState = {
    isLogged: false,
    hypermedia: {},
    user:
        {
            "personID": "jose@family.com",
            "name": "Georgios Kyriacos Panayiotou",
            "birthdate": "1963-06-25",
            "birthplace": "Middlesex, England",
            "mother": "maria@family.com",
            "father": "jose@family.com",
            "siblings": "[nuno@family.com]",
            "categories": "[vananas, Nanasis]",
            "links": [{"rel": "self", "href": "http://localhost:8080/persons/tarcisio@family.com"}]
        }
    ,
    groups: {
        loading: false,
        error: null,
        data: []
    },
    accounts: {
        loading: false,
        error: null,
        data: []
    },
    transactions: {
        loading: false,
        error: null,
        data: []
    },
    groupTransactions: {
        loading: false,
        error: null,
        data: []
    },
    groupAccounts: {
        loading: false,
        error: null,
        data: []
    },
    categories: {
        loading: false,
        error: null,
        data: []
    },
    groupCategories: {
        loading: false,
        error: null,
        data: []
    },
    dashboardpie: {
        loading: false,
        error: null,
        data: []
    },
    dashboardlines: {
        loading: false,
        error: null,
        data: []
    },
    dashboardtransactions: {
        loading: false,
        error: null,
        data: []
    },
    persons: {
        loading: false,
        error: null,
        data: []
    }
};

const AppProvider = (props) => {
    const [state, dispatch] = usePersistedReducer(reducer, initialState);
    return (
        <Provider value={{
            state,
            dispatch
        }}>
            {props.children}
        </Provider>
    );
};
AppProvider.propTypes = {
    children: PropTypes.node,
};

export default AppProvider;