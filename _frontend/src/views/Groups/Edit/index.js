import React, {useContext} from 'react';
import GridItem from "../../../components/Grid/GridItem";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import GridContainer from "../../../components/Grid/GridContainer";
import AppContext from "../../../context/AppContext";
import {withFormik} from 'formik';
import '../../../assets/css/formik.css'
import * as Yup from 'yup';
import {ReactSelect} from '../../../components/Inputs/ReactSelect'
import {Label} from "../../../components/Inputs/Label";
import {TextInput} from "../../../components/Inputs/TextInput";
import {toast} from "react-toastify";
import {css} from "glamor";
import history from "../../../services/history";
import MaterialButton from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";

let responsiblesArr = [];
let membersArr = [];
let personsArr = [];
let groupDescription = '';

const navigateToGroups = () => {
    history.push('/groups');
};

export default function EditGroupForm(props) {
    const {state} = useContext(AppContext);
    const {groups} = state;
    const {data} = groups;
    const selectedGroupId = props.match.params.id;
    groupDescription = data[selectedGroupId].groupDescription;
    responsiblesArr = [];
    membersArr = [];
    personsArr = [];

    for (let j = 0; j < state.persons.data.length; j++) {
        let person = state.persons.data[j];
        const found = personsArr.some(el => el.value === person);
        if (!found) {
            personsArr.push({
                value: state.persons.data[j].personID,
                label: state.persons.data[j].name
            });
        }
    }
    for (let j = 0; j < data[selectedGroupId].responsibles.length; j++) {
        let responsible = data[selectedGroupId].responsibles[j];
        const found = responsiblesArr.some(el => el.value === responsible);
        if (!found) {
            let responsibleName = '';
            for (var i in personsArr) {
                if (personsArr[i].value === responsible) {
                    responsibleName = personsArr[i].label
                }
            }
            responsiblesArr.push({
                value: responsible,
                label: responsibleName
            });
        }
    }
    for (let j = 0; j < data[selectedGroupId].members.length; j++) {
        let member = data[selectedGroupId].members[j];
        const found = membersArr.some(el => el.value === member);
        if (!found) {
            let memberName = '';
            for (var k in personsArr) {
                if (personsArr[k].value === member) {
                    memberName = personsArr[k].label
                }
            }
            membersArr.push({
                value: member,
                label: memberName
            });
        }
    }
    membersArr.concat(personsArr);
    responsiblesArr.concat(personsArr);

    return <GridContainer key={groupDescription}>
        <GridItem xs={12} sm={12} md={12}>
            <Card>
                <CardBody>
                    <h2>Edit group</h2>
                    <EnhancedForm key={selectedGroupId} data={{
                        id: selectedGroupId,
                        groupDescription: groupDescription,
                        responsibles: responsiblesArr,
                        members: membersArr,
                        persons: personsArr
                    }}/>
                </CardBody>
            </Card>
        </GridItem>
    </GridContainer>
}
const formikEnhancer = withFormik({
    enableReinitialize: true,
    validationSchema: Yup.object().shape({
        members: Yup.array()
            .of(
                Yup.object().shape({
                    label: Yup.string().required(),
                    value: Yup.string().required(),
                })
            ),
    }),
    mapPropsToValues(props) {
        return {
            groupDescription: props.data.groupDescription,
            responsibles: props.data.responsibles,
            members: props.data.members,
        };
    },
    mapPropsToTouched(props) {
        return {
            groupDescription: props.data.groupDescription,
            responsibles: props.data.responsibles,
            members: props.data.members,
        }
    },
    handleSubmit: (values, {setSubmitting, resetForm}) => {
        let data = values.members;
        const url = `http://localhost:8080/groups/${groupDescription}/members`
        fetch(url,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            })
            .then(function (res) {
                toast.success("Group saved successfully", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    className: css({
                        backgroundColor: '#007E33',
                        fontWeight: 500,
                        color: '#ffffff'
                    })
                });
                resetForm();
                history.push('/groups')
            })
            .catch(function (res) {
                toast.error(res.message);
            })
        setSubmitting(false);
    },
    displayName: 'GroupEditForm',
});

const GroupEditForm = props => {
    const {
        values,
        errors,
        setFieldValue,
        setFieldTouched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    } = props;
    return (
        <form onSubmit={handleSubmit} key={props.data.id} id={`groupeditform-${props.data.id}`}>
            <TextInput
                key={props.data.id}
                id={`groupDescription-${props.data.id}`}
                name="groupDescription"
                type="text"
                label="Name"
                placeholder="Insert group name..."
                value={props.data.groupDescription}
                onChange={handleChange}
                onBlur={handleBlur}
                disabled
            />
            <GridContainer key={groupDescription}>
                <GridItem xs={12} sm={12} md={6}>
                    <Label>Responsibles</Label>
                    <ReactSelect
                        id={`responsibles-${props.data.id}`}
                        name="responsibles"
                        defaultValue={values.responsibles}
                        options={props.data.persons}
                        isMulti
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                    <Label>Members</Label>
                    <ReactSelect
                        id={`members-${props.data.id}`}
                        name="members"
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                        error={errors.members}
                        touched={props.data.members}
                        defaultValue={props.data.members}
                        options={props.data.persons}
                        isMulti
                    />
                </GridItem>
            </GridContainer>
            <div style={{textAlign: 'center', marginTop: '20px'}}>
                <MaterialButton type="submit" variant="contained"
                                color="primary"
                                size="medium"
                                style={{margin: '0 10px 0 0'}}
                                startIcon={<SaveIcon/>}
                                disabled={isSubmitting}>Save</MaterialButton>
                <MaterialButton variant="contained"
                                size="medium"
                                startIcon={<CancelIcon/>}
                                color="default" onClick={navigateToGroups}>Cancel</MaterialButton>
            </div>
        </form>
    );
};

const EnhancedForm = formikEnhancer(GroupEditForm);