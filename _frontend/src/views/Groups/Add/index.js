import React from 'react';
import history from "../../../services/history";
import GridItem from "../../../components/Grid/GridItem";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import GridContainer from "../../../components/Grid/GridContainer";
import {withFormik} from 'formik';
import MaterialButton from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import {TextInput} from "../../../components/Inputs/TextInput";
import '../../../assets/css/formik.css'
import * as Yup from 'yup';
import {toast} from "react-toastify";
import {css} from "glamor";

const navigateToGroups = () => {
    history.push('/groups');
};

export default function AddGroupForm(props) {
    return <GridContainer key={'addGroupGrid'}>
        <GridItem xs={12} sm={12} md={12}>
            <Card>
                <CardBody>
                    <h2>Add group</h2>
                    <MyEnhancedForm key={'addGroupForm'} group={{groupDescription: ''}}/>
                </CardBody>
            </Card>
        </GridItem>
    </GridContainer>
}
const formikEnhancer = withFormik({
    validationSchema: Yup.object().shape({
        groupDescription: Yup.string()
            .min(2, "The group name should be longer than 2")
            .required('Group name is required.'),
    }),

    mapPropsToValues: ({group}) => ({
        ...group,
    }),
    handleSubmit: (payload, {setSubmitting, resetForm}) => {
        let data = payload;
        data.responsibleEmail = 'jose@family.com';
        fetch("http://localhost:8080/groups",
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            })
            .then(function (res) {
                toast.success("Group saved successfully", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    className: css({
                        backgroundColor: '#007E33',
                        fontWeight: 500,
                        color: '#ffffff'
                    })
                });
                resetForm();
                history.push('/groups')
            })
            .catch(function (res) {
                toast.error(res.message);
            })
        setSubmitting(false);
    },
    displayName: 'AddGroupFrom',
});

const AddGroupFrom = props => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    } = props;
    return (
        <form onSubmit={handleSubmit} key={'AddGroupFormTag'}>
            <TextInput
                key={values.id}
                id="groupDescription"
                name="groupDescription"
                type="text"
                label="Name"
                placeholder="Insert group name..."
                error={touched.groupDescription && errors.groupDescription}
                value={values.groupDescription}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <div style={{textAlign: 'center', marginTop: '20px'}}>
                <MaterialButton type="submit" variant="contained"
                                color="primary"
                                size="medium"
                                style={{margin: '0 10px 0 0'}}
                                startIcon={<SaveIcon/>}
                                disabled={isSubmitting}>Save</MaterialButton>
                <MaterialButton variant="contained"
                                size="medium"
                                startIcon={<CancelIcon/>}
                                color="default" onClick={navigateToGroups}>Cancel</MaterialButton>
            </div>
        </form>
    );
};

const MyEnhancedForm = formikEnhancer(AddGroupFrom);