import React, {useContext, useEffect} from 'react';
import {Link} from 'react-router-dom';
import history from "../../services/history";
import AppContext from '../../context/AppContext';
import {fetchGroupsFailure, fetchGroupsStarted, fetchGroupsSuccess} from '../../actions/Actions';
// core components
import CircularProgress from '@material-ui/core/CircularProgress';
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Card from "../../components/Card/Card.js";
import CardBody from "../../components/Card/CardBody.js";
import Table from "../../components/Table/Table.js";
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import PlusIcon from '@material-ui/icons/Add';
import Grid from "@material-ui/core/Grid";
import {toast} from "react-toastify";
import {css} from "glamor";
import GetHypermediaHref from "../../utils/GetHypermediaHref";

export default function Groups(props) {
    const {state, dispatch} = useContext(AppContext);
    const {groups, user} = state;
    const {loading, error, data} = groups;
    const {personID} = user;
    const navigateToGroupsAdd = () => {
        history.push('/groups/add');
    };
    const fetchUrl = GetHypermediaHref("groups");
    useEffect(() => {
        dispatch(fetchGroupsStarted());
        fetch(fetchUrl)
            .then(res => res.json())
            .then(res => dispatch(fetchGroupsSuccess(res)))
            .catch(err => dispatch(fetchGroupsFailure(err.message)))
        ;
    }, [dispatch, personID]);

    const columns = React.useMemo(
        () => [
            {
                Header: 'Name',
                accessor: 'groupDescription'
            }, {
                Header: "Actions",
                id: "edit-button",
                Cell: ({row}) => (
                    <div style={{textAlign: 'center'}}>
                        <Link to={`/groups/edit/${row.id}`} style={{backgroundColor: '#539f8c'}}
                              className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary">
                            <EditIcon fontSize="small" style={{marginRight: '10px'}}/> Edit
                        </Link>
                    </div>
                )
            }
        ],
        []
    );

    let result = '';

    if (loading === true) {
        result = <Grid container style={{height: "100%"}}>
            <Grid item xs={12} sm={12} md={12} container direction="column"
                  alignItems="center"
                  justify="center">
                <CircularProgress color="primary"/>
            </Grid>
        </Grid>;
    } else {
        if (error !== null) {
            toast.error("Please check your Internet connection!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                className: css({
                    fontWeight: 500,
                    color: '#ffffff'
                })
            });
            return '';
        } else {
            if (data.length > 0) {
                result = <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardBody>
                                <Table columns={columns} data={data}/>
                                <div onClick={navigateToGroupsAdd} style={{textAlign: 'center', marginTop: '20px'}}>
                                    <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>;
            } else {
                result = <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardBody>
                            <div onClick={navigateToGroupsAdd} style={{textAlign: 'center', marginBottom: '20px'}}>
                                <Button id="addNewGroup" startIcon={<PlusIcon/>} variant="contained" color="primary">Add
                                    new</Button>
                            </div>
                            <div>
                                You haven't created any groups yet.
                            </div>
                            <div onClick={navigateToGroupsAdd} style={{textAlign: 'center', marginTop: '20px'}}>
                                <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>;
            }
        }
    }
    return result;
}