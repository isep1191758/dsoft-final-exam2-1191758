import React, {useContext} from 'react';
import {URL_API} from "../../../actions/Actions";
import history from "../../../services/history";
import GridItem from "../../../components/Grid/GridItem";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import GridContainer from "../../../components/Grid/GridContainer";
import {withFormik} from 'formik';
import MaterialButton from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import {TextInput} from "../../../components/Inputs/TextInput";
import '../../../assets/css/formik.css'
import * as Yup from 'yup';
import {toast} from "react-toastify";
import {css} from "glamor";
import {Label} from "../../../components/Inputs/Label";
import {ReactSelect} from "../../../components/Inputs/ReactSelect";
import AppContext from "../../../context/AppContext";
import Groups from "../../Groups";

const navigateToAccounts = () => {
    history.push('/Accounts');
};

let groupsList = [];

let groupId = '';

export default function AddGroupAccountsForm(props) {
    const {state} = useContext(AppContext);
    const {groups} = state;
    const {data} = groups;
    groupsList = [];
    for(let j = 0; j < data.length; j++) {
            groupsList.push({
                value: data[j].groupDescription, label: data[j].groupDescription
            })
    }

    console.log(groupsList);


    return <GridContainer key={'addAccountGrid'}>
        <GridItem xs={12} sm={12} md={12}>
            <Card>
                <CardBody>
                    <h2>Add account</h2>
                    <MyEnhancedForm key={'addAccountForm'} groupAccount={{groupDescription: ''}}/>
                </CardBody>
            </Card>
        </GridItem>
    </GridContainer>
}

    const formikEnhancer = withFormik({
        validationSchema: Yup.object().shape({
            denomination: Yup.string()
                .min(2, "The account denomination should be longer than 2")
                .required('Account denomination is required.'),
            description: Yup.string()
                .min(2, "The account description should be longer than 2")
                .required('Account description is required.')
        }),

        mapPropsToValues: ({account}) => ({
            ...account,
        }),
        handleSubmit: (payload, {setSubmitting, resetForm}) => {
            payload.responsibleEmail = 'jose@family.com';
            let data = payload;
            groupId = payload.group.value;
            fetch(`${URL_API}/groups/${groupId}/accounts`,
                {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
//                body: JSON.stringify(data)
                    body: JSON.stringify(data)
                })
                .then(function (res) {
                    toast.success("Account saved successfully", {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        className: css({
                            backgroundColor: '#28a745',
                            fontWeight: 500,
                            color: '#ffffff'
                        })
                    });
                    resetForm();
                    history.push('/accounts')
                })
                .catch(function (res) {
                    toast.error(res.message);
                });
            setSubmitting(false);
        },
        displayName: 'AddAccountForm',
    });

    const AddGroupAccountForm = props => {
        const {
            values,
            touched,
            errors,
            dirty,
            setFieldValue,
            setFieldTouched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
        } = props;

        return (
            <form onSubmit={handleSubmit} key={'AddAccountFormTag'}>

                <GridContainer>
                    <GridItem xs={12} sm={12} md={3}>
                        <Label>Group</Label>
                        <ReactSelect
                            id={`groupDescription-${values.groups}`}
                            name="group"
                            error={touched.groupDescription && errors.groupDescription}
                            options={groupsList}
                            onChange={setFieldValue}
                            onBlur={setFieldTouched}
                        />
                    </GridItem>
                </GridContainer>

                <TextInput
                    key={values.id}
                    id="denomination"
                    name="denomination"
                    type="text"
                    label="Denomination"
                    placeholder="Insert account denomination..."
                    error={touched.denomination && errors.denomination}
                    value={values.denomination}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <TextInput
                    key={values.id}
                    id="description"
                    name="description"
                    type="text"
                    label="Description"
                    placeholder="Insert account description..."
                    error={touched.description && errors.description}
                    value={values.description}
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
                <div style={{textAlign: 'center', marginTop: '20px'}}>
                    <MaterialButton type="submit" variant="contained"
                                    color="primary" onClick{...Groups()}
                                    size="medium"
                                    style={{margin: '0 10px 0 0'}}
                                    startIcon={<SaveIcon/>}
                                    disabled={isSubmitting}>Save</MaterialButton>
                    <MaterialButton variant="contained"
                                    size="medium"
                                    startIcon={<CancelIcon/>}
                                    color="default" onClick={navigateToAccounts}>Cancel</MaterialButton>
                </div>
            </form>
        );
    };

    const MyEnhancedForm = formikEnhancer(AddGroupAccountForm);

