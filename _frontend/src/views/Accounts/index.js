import React, {useContext, useEffect} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import history from "../../services/history";
import AppContext from '../../context/AppContext';
import GetHypermediaHref from '../../utils/GetHypermediaHref'
import {
    fetchAccountsFailure,
    fetchAccountsGroupFailure,
    fetchAccountsGroupStarted,
    fetchAccountsGroupSuccess,
    fetchAccountsStarted,
    fetchAccountsSuccess
} from '../../actions/AccountActions';
// core components
import CircularProgress from '@material-ui/core/CircularProgress';
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Card from "../../components/Card/Card.js";
import CardBody from "../../components/Card/CardBody.js";
import Table from "../../components/Table/Table.js";
import Button from '@material-ui/core/Button';
// @material-ui/icons
import EditIcon from '@material-ui/icons/Edit';
import PlusIcon from '@material-ui/icons/Add';
import PersonIcon from '@material-ui/icons/Person';
import GroupIcon from '@material-ui/icons/Group';
import Grid from "@material-ui/core/Grid";
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

export default function Accounts(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    let personalAccounts = PersonalAccounts();
    let groupsAccounts = GroupAccounts();
    return (
        <div className={classes.root}>
            <AppBar position="static" color="default" elevation={1}>
                <Tabs value={value} onChange={handleChange} aria-label="accounts tabs" indicatorColor="primary"
                >
                    <Tab label="Personal" icon={<PersonIcon/>} {...a11yProps(0)} />
                    <Tab label="Group" icon={<GroupIcon/>} {...a11yProps(1)} />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                {personalAccounts}
            </TabPanel>
            <TabPanel value={value} index={1}>
                {groupsAccounts}
            </TabPanel>
        </div>
    );
}

function TabPanel(props) {
    const {children, value, index, ...other} = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

export function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

function PersonalAccounts(props) {
    const {state, dispatch} = useContext(AppContext);
    const {accounts, user, hypermedia} = state;
    const {loading, error, data} = accounts;
    const {personID} = user;
    const navigateToPersonalAccountsAdd = () => {
        history.push('/accounts/add');
    };

    const fetchUrl = GetHypermediaHref("accounts");

    useEffect(() => {
        dispatch(fetchAccountsStarted());
        fetch(fetchUrl)
            .then(res => res.json())
            .then(res => dispatch(fetchAccountsSuccess(res)))
            .catch(err => dispatch(fetchAccountsFailure(err.message)))
        ;
        console.log(accounts);
    }, [dispatch]);
    const columns = React.useMemo(
        () => [
            {
                Header: 'Denomination',
                accessor: 'denomination'
            }, {
                Header: 'Description',
                accessor: 'description'
            }, {
                Header: "Actions",
                id: "edit-button",
                Cell: ({row}) => (
                    <div style={{textAlign: 'center'}}>
                        <Link to={`/accounts/edit/${row.id}`} style={{backgroundColor: '#539f8c'}}
                              className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary">
                            <EditIcon fontSize="small" style={{marginRight: '10px'}}/> Edit
                        </Link>
                    </div>
                )
            }
        ],
        []
    );
    let result = '';
    if (loading === true) {
        result = <Grid container style={{height: "100%"}}>
            <Grid item xs={12} sm={12} md={12} container direction="column"
                  alignItems="center"
                  justify="center">
                <CircularProgress color="primary"/>
            </Grid>
        </Grid>;
    } else {
        if (error !== null) {
            return '';
        } else {
            if (data.length > 0) {
                result = <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardBody>
                                <Table columns={columns} data={data}/>
                                <div onClick={navigateToPersonalAccountsAdd}
                                     style={{textAlign: 'center', marginTop: '20px'}}>
                                    <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>;
            } else {
                result = <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardBody>
                            <div onClick={navigateToPersonalAccountsAdd}
                                 style={{textAlign: 'center', marginBottom: '20px'}}>
                                <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                            </div>
                            <div>
                                You haven't created any personal accounts yet.
                            </div>
                            <div onClick={navigateToPersonalAccountsAdd}
                                 style={{textAlign: 'center', marginTop: '20px'}}>
                                <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>;
            }
        }
    }
    return result;
}

export function GroupAccounts(props) {
    const {state, dispatch} = useContext(AppContext);
    const {groupAccounts, user, hypermedia} = state;
    const {loading, error, data} = groupAccounts;
    const {personID} = user;
    const navigateToGroupAccountsAdd = () => {
        history.push('/accounts/groupAdd');
    };

    const fetchUrl = GetHypermediaHref("accountsGroup");

    useEffect(() => {
        dispatch(fetchAccountsGroupStarted());
        fetch(fetchUrl)
            .then(res => res.json())
            .then(res => dispatch(fetchAccountsGroupSuccess(res)))
            .catch(err => dispatch(fetchAccountsGroupFailure(err.message)))
        ;
    }, [dispatch]);

    const columns = React.useMemo(
        () => [
            {
                Header: 'Denomination',
                accessor: 'denomination'
            }, {
                Header: 'Description',
                accessor: 'description'
            }, {
                Header: 'Group',
                accessor: 'ownerID'
            }, {
                Header: "Actions",
                id: "edit-button",
                Cell: ({row}) => (
                    <div style={{textAlign: 'center'}}>
                        <Link to={`/accounts/edit/${row.id}`} style={{backgroundColor: '#539f8c'}}
                              className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary">
                            <EditIcon fontSize="small" style={{marginRight: '10px'}}/> Edit
                        </Link>
                    </div>
                )
            }
        ],
        []
    );
    let result = '';
    if (loading === true) {
        result = <Grid container style={{height: "100%"}}>
            <Grid item xs={12} sm={12} md={12} container direction="column"
                  alignItems="center"
                  justify="center">
                <CircularProgress color="primary"/>
            </Grid>
        </Grid>;
    } else {
        if (error !== null) {
            return '';
        } else {
            if (data.length > 0) {
                result = <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardBody>
                                <Table columns={columns} data={data}/>
                                <div onClick={navigateToGroupAccountsAdd}
                                     style={{textAlign: 'center', marginTop: '20px'}}>
                                    <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>;
            } else {
                result = <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardBody>
                            <div onClick={navigateToGroupAccountsAdd}
                                 style={{textAlign: 'center', marginBottom: '20px'}}>
                                <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                            </div>
                            <div>
                                You haven't created any group accounts yet.
                            </div>
                            <div onClick={navigateToGroupAccountsAdd}
                                 style={{textAlign: 'center', marginTop: '20px'}}>
                                <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>;
            }
        }
    }
    return result;
}
