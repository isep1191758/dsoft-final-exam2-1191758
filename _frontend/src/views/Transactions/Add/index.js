// @material-ui/icons
import history from "../../../services/history";
import GridContainer from "../../../components/Grid/GridContainer";
import GridItem from "../../../components/Grid/GridItem";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import {withFormik} from "formik";
import * as Yup from "yup";
import {TextInput} from "../../../components/Inputs/TextInput";
import MaterialButton from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import React, {useContext} from "react";
import {ReactSelect} from "../../../components/Inputs/ReactSelect";
import AppContext from "../../../context/AppContext";
import {Label} from "../../../components/Inputs/Label";
import {URL_API} from "../../../actions/Actions";
import {toast} from "react-toastify";
import {css} from "glamor";
import Categories from "../../Categories/index.js";

const navigateToTransactions = () => {
    history.push('/transactions');
};
let categoriesArr = [];
let accountsArr = [];
let groupId = '';

export default function AddGroupTransactionForm(props) {
    const {state} = useContext(AppContext);
    const {groups, groupAccounts, groupCategories, user} = state;
    const {data} = groups;

    categoriesArr = [];

    accountsArr = [];

    let groupAccountsData = groupAccounts.data;


    for (let j = 0; j < groupAccountsData.length; j++) {
        let denomination = groupAccountsData[j].denomination;
        if (groupId === groupAccountsData[j].ownerID) {
            const found = accountsArr.some(el => el.value === denomination);
            if (!found) {
                accountsArr.push({
                    value: denomination,
                    label: denomination
                });
            }
        }
    }


    if (groupCategories.data.length > 0) {
        for (let j = 0; j < groupCategories.data.length; j++) {
            let categoryDesignation = groupCategories.data[j].designation;

            const found = categoriesArr.some(el => el.value === categoryDesignation);
            if (!found) {
                categoriesArr.push({
                    value: categoryDesignation,
                    label: categoryDesignation
                });
            }

        }
    } else {
        {
            Categories.call(groupCategories)
        }
    }
    console.log(categoriesArr);

    return <GridContainer key={'addTransactionGrid'}>
        <GridItem xs={12} sm={12} md={12}>
            <Card>
                <CardBody>
                    <h2>Add transaction</h2>
                    <MyEnhancedForm key={'addTransactionsForm'} transaction={{
                        personID: user.personID,
                        groupID: groupId,
                        amount: '',
                        description: '',
                        category: categoriesArr,
                        debit: accountsArr,
                        credit: accountsArr,
                        type: '',
                    }}/>
                </CardBody>
            </Card>
        </GridItem>
    </GridContainer>
}

const formikEnhancer = withFormik({
    validationSchema: Yup.object().shape({
        amount: Yup.string()
            .min(1, "The amount should be longer than 1")
            .required('Amount is required.'),
        description: Yup.string()
            .min(2, "The description should be longer than 2")
            .required('Description is required.'),
        /*  credit: Yup.object().shape({
              label: Yup.string().required("Required"),
              value: Yup.string().required("Required")*/
    }),

    mapPropsToValues: ({transaction}) => ({
        ...transaction,
    }),

    handleSubmit: (payload, {setSubmitting, resetForm}) => {
        let data = payload;
        data.groupID = groupId;
        data.personID = payload.personID;
        data.credit = payload.credit.value;
        data.debit = payload.debit.value;
        data.type = payload.type.value;
        data.category = payload.category.value;
        console.log('esse data');
        console.log(data);
        fetch(`${URL_API}/groups/${groupId}/transaction`,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            })
            .then(function (res) {
                toast.success("Transaction saved successfully", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    className: css({
                        backgroundColor: '#28a745',
                        fontWeight: 500,
                        color: '#ffffff'
                    })
                });
                resetForm();
                history.push('/transactions')
            })
            .catch(function (res) {
                toast.error(res.message);
            });
        setSubmitting(false);
    },
    displayName: 'AddTransactionsForm',
});

const AddTransactionsForm = props => {
    const {
        values,
        touched,
        setFieldValue,
        setFieldTouched,
        errors,
        dirty,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    } = props;
    return (
        <form onSubmit={handleSubmit} key={'AddTransactionsFormTag'}>
            {console.log(touched, errors, values, dirty)}
            <TextInput
                key={values.id}
                id="amount"
                name="amount"
                type="number"
                label="Amount"
                placeholder="Insert transaction amount..."
                error={touched.amount && errors.amount}
                value={values.amount}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <TextInput
                key={values.id}
                id="description"
                name="description"
                type="text"
                label="Description"
                placeholder="Insert transaction description..."
                error={touched.description && errors.description}
                value={values.description}
                onChange={handleChange}
                onBlur={handleBlur}
            />
            <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                    <Label>Credit Account</Label>
                    <ReactSelect
                        id={`credit-${values.credit}`}
                        name="credit"
                        error={touched.credit && errors.credit}
                        options={accountsArr}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                    <Label>Debit Account</Label>
                    <ReactSelect
                        id={`debit-${values.debit}`}
                        name="debit"
                        error={touched.debit && errors.debit}
                        options={accountsArr}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                    <Label>Category</Label>
                    <ReactSelect
                        key={values.id}
                        id="category"
                        name="category"
                        error={touched.category && errors.category}
                        options={categoriesArr}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                    <Label>Type</Label>
                    <ReactSelect
                        key={values.id}
                        id="type"
                        name="type"
                        error={touched.type && errors.type}
                        options={[{value: 'CREDIT', label: 'Credit'}, {value: 'DEBIT', label: 'Debit'},]}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                    />
                </GridItem>
            </GridContainer>

            <div style={{textAlign: 'center', marginTop: '20px'}}>
                <MaterialButton type="submit" variant="contained"
                                color="primary"
                                size="medium"
                                style={{margin: '0 10px 0 0'}}
                                startIcon={<SaveIcon/>}
                                disabled={isSubmitting}>Save</MaterialButton>
                <MaterialButton variant="contained"
                                size="medium"
                                startIcon={<CancelIcon/>}
                                color="default" onClick={navigateToTransactions}>Cancel</MaterialButton>
            </div>
        </form>
    );
};

const MyEnhancedForm = formikEnhancer(AddTransactionsForm);
