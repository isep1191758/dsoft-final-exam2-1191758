import React, {useContext, useEffect} from "react";
import AppContext from "../../../context/AppContext";
import history from "../../../services/history";
import {
    fetchGroupTransactionsFailure,
    fetchGroupTransactionsStarted,
    fetchGroupTransactionsSuccess,
    URL_API
} from "../../../actions/Actions";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import {toast} from "react-toastify";
import {css} from "glamor";
import GridContainer from "../../../components/Grid/GridContainer";
import GridItem from "../../../components/Grid/GridItem";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import Table from "../../../components/Table/Table";
import MaterialButton from "@material-ui/core/Button";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

let groupId = '';
const navigateToGroupsTransactionsView = () => {
    history.push('/transactions');
};
let buttonT = '';
export default function GroupsTransactions(props) {
    const {state, dispatch} = useContext(AppContext);
    const {groupTransactions, groups} = state;
    const {loading, error, data} = groupTransactions;
    buttonT = button();

    groupId = groups.data[props.match.params.id].groupDescription;

    useEffect(() => {
        dispatch(fetchGroupTransactionsStarted());
        fetch(`${URL_API}/groups/${groupId}/transactions`)
            .then(res => res.json())
            .then(res => dispatch(fetchGroupTransactionsSuccess(res)))
            .catch(err => dispatch(fetchGroupTransactionsFailure(err.message)))
        ;
    }, [dispatch]);

    const columns = React.useMemo(
        () => [
            {
                Header: 'Date',
                accessor: 'date',
                Cell: ({row}) => (
                    <>{row.values.date.substring(0, 10)}</>
                )
            }, {
                Header: 'Description',
                accessor: 'description'
            }, {
                Header: 'Category',
                accessor: 'category'
            }, {
                Header: 'Debit',
                accessor: 'debit'
            }, {
                Header: 'Credit',
                accessor: 'credit'
            }, {
                Header: 'Type',
                accessor: 'type'
            }, {
                Header: 'Amount',
                accessor: 'amount',
                Cell: ({row}) => (
                    <div style={{textAlign: 'center'}}>
                        {row.values.amount}
                    </div>
                )
            }
        ],
        []
    );
    let result = '';
    if (loading === true) {
        result = <Grid container style={{height: "100%"}}>
            <Grid item xs={12} sm={12} md={12} container direction="column"
                  alignItems="center"
                  justify="center">
                <CircularProgress color="primary"/>
            </Grid>
        </Grid>;
    } else {
        if (error !== null) {
            toast.error("Please check your Internet connection!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                className: css({
                    fontWeight: 500,
                    color: '#ffffff'
                })
            });
            return '';
        } else {
            if (data.length > 0) {
                result = <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardBody>
                                <div style={{textAlign: 'center', marginBottom: '20px'}}>
                                    {buttonT}
                                </div>
                                <Table columns={columns} data={data}/>
                                <div style={{textAlign: 'center', marginTop: '20px'}}>
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>;
            } else {
                result = <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardBody>
                            <div style={{textAlign: 'center', marginBottom: '20px'}}>
                            </div>
                            <div>
                                You haven't created any group transaction yet.
                            </div>
                            <div style={{textAlign: 'center', marginTop: '20px'}}>
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>;
            }
        }
    }
    return result;
}

function button() {
    return (
        <div style={{textAlign: 'left', marginTop: '20px'}}>

            <MaterialButton variant="contained"
                            size="medium"
                            startIcon={<ArrowBackIcon/>}
                            color="default" onClick={navigateToGroupsTransactionsView}>Back
            </MaterialButton>
        </div>)
}
