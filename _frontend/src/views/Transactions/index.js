import React, {useContext, useEffect} from "react";
// @material-ui/icons
import PersonIcon from "@material-ui/icons/Person";
import GroupIcon from "@material-ui/icons/Group";
import AppContext from "../../context/AppContext";
import history from "../../services/history";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import {makeStyles} from "@material-ui/core/styles";
import {
    fetchGroupsFailure,
    fetchGroupsStarted,
    fetchGroupsSuccess,
    fetchPersonalTransactionsFailure,
    fetchPersonalTransactionsStarted,
    fetchPersonalTransactionsSuccess
} from "../../actions/Actions";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import {toast} from "react-toastify";
import {css} from "glamor";
import GridContainer from "../../components/Grid/GridContainer";
import GridItem from "../../components/Grid/GridItem";
import PlusIcon from "@material-ui/icons/Add";
import Table from "../../components/Table/Table";
import {Link} from "react-router-dom";
import VisibilityIcon from '@material-ui/icons/Visibility';
import GroupAccounts from "../Accounts/index"
import GetHypermediaHref from "../../utils/GetHypermediaHref";

export default function Transactions(props) {
    const {state, dispatch} = useContext(AppContext);
    const {transactions, user} = state;
    const {loading, error, data} = transactions;
    const {groupID} = user;
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const navigateToTransactionsAdd = () => {
        history.push('/transaction/add');
    };

    let groupsForTransactions = Groups();
    let personalTransactions = PersonalTransactions();

    return (
        <div className={classes.root}>
            <AppBar position="static" color="default" elevation={1}>
                <Tabs value={value} onChange={handleChange} aria-label="transactions tabs" indicatorColor="primary"
                >
                    <Tab label="Personal" icon={<PersonIcon/>} {...a11yProps(0)} />
                    <Tab label="Group" icon={<GroupIcon/>} {...a11yProps(1)} />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>{personalTransactions}
            </TabPanel>
            <TabPanel value={value} index={1}>
                {groupsForTransactions}
            </TabPanel>
        </div>
    );
}

function TabPanel(props) {
    const {children, value, index, ...other} = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

function Groups(props) {
    const {state, dispatch} = useContext(AppContext);
    const {groups, user} = state;
    const {loading, error, data} = groups;
    const {personID} = user;
    const navigateToGroupsAdd = () => {
        history.push('/transaction/add');
    };

    const fetchUrl = GetHypermediaHref("groups");

    useEffect(() => {
        dispatch(fetchGroupsStarted());
        fetch(fetchUrl)
            .then(res => res.json())
            .then(res => dispatch(fetchGroupsSuccess(res)))
            .catch(err => dispatch(fetchGroupsFailure(err.message)))
        ;
    }, [dispatch]);

    const columns = React.useMemo(
        () => [
            {
                Header: 'Name',
                accessor: 'groupDescription'
            }, {
                Header: "Actions",
                id: "view-button",
                Cell: ({row}) => (
                    <div style={{width: '320px', textAlign: 'center'}}>
                        <Link to={`/transactions/view/${row.id}`} style={{backgroundColor: '#539f8c'}}
                              className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary">
                            <VisibilityIcon fontSize="small" style={{marginRight: '10px'}}/> View
                        </Link> <Link to={`/transaction/add/${row.id}`} style={{backgroundColor: '#3f51b5'}}
                                      className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary">
                        <PlusIcon fontSize="small" style={{marginRight: '10px'}} onClick={GroupAccounts()}/> Add New
                    </Link>
                    </div>
                )
            }
        ],
        []
    );

    let result = '';

    if (loading === true) {
        result = <Grid container style={{height: "100%"}}>
            <Grid item xs={12} sm={12} md={12} container direction="column"
                  alignItems="center"
                  justify="center">
                <CircularProgress color="primary"/>
            </Grid>
        </Grid>;
    } else {
        if (error !== null) {
            toast.error("Please check your Internet connection!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                className: css({
                    fontWeight: 500,
                    color: '#ffffff'
                })
            });
            return '';
        } else {
            if (data.length > 0) {
                result = <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <div style={{textAlign: 'center', marginBottom: '20px'}}>
                        </div>
                        <Table columns={columns} data={data}/>
                        <div style={{textAlign: 'center', marginTop: '20px'}}>
                        </div>
                    </GridItem>
                </GridContainer>;
            } else {
                result = <GridItem xs={12} sm={12} md={12}>
                    <div style={{textAlign: 'center', marginBottom: '20px'}}>
                    </div>
                    <div>
                        You haven't created any groups yet.
                    </div>
                    <div style={{textAlign: 'center', marginTop: '20px'}}>
                    </div>
                </GridItem>;
            }
        }
    }
    return result;
}

function PersonalTransactions(props) {
    const {state, dispatch} = useContext(AppContext);
    const {transactions, user} = state;
    const {loading, error, data} = transactions;
    const {personID} = user;
    const navigateToPersonalTransactions = () => {
        history.push('/transactions');
    };
    const fetchUrl = GetHypermediaHref("transactions");
    useEffect(() => {
        dispatch(fetchPersonalTransactionsStarted());
        fetch(fetchUrl)
            .then(res => res.json())
            .then(res => dispatch(fetchPersonalTransactionsSuccess(res)))
            .catch(err => dispatch(fetchPersonalTransactionsFailure(err.message)))
        ;
    }, [dispatch]);

    const columns = React.useMemo(
        () => [
            {
                Header: 'Date',
                accessor: 'date',
                Cell: ({row}) => (
                    <>{row.values.date.substring(0, 10)}</>
                )
            }, {
                Header: 'Description',
                accessor: 'description'
            }, {
                Header: 'Category',
                accessor: 'category'
            }, {
                Header: 'Debit',
                accessor: 'debit'
            }, {
                Header: 'Credit',
                accessor: 'credit'
            }, {
                Header: 'Type',
                accessor: 'type'
            }, {
                Header: 'Amount',
                accessor: 'amount',
                Cell: ({row}) => (
                    <div style={{textAlign: 'center'}}>
                        {row.values.amount}
                    </div>
                )
            }
        ],
        []
    );
    let result = '';
    if (loading === true) {
        result = <Grid container style={{height: "100%"}}>
            <Grid item xs={12} sm={12} md={12} container direction="column"
                  alignItems="center"
                  justify="center">
                <CircularProgress color="primary"/>
            </Grid>
        </Grid>;
    } else {
        if (error !== null) {
            toast.error("Please check your Internet connection!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                className: css({
                    fontWeight: 500,
                    color: '#ffffff'
                })
            });
            return '';
        } else {
            if (data.length > 0) {
                result = <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <div style={{textAlign: 'center', marginBottom: '20px'}}>
                        </div>
                        <Table columns={columns} data={data}/>
                        <div style={{textAlign: 'center', marginTop: '20px'}}>
                        </div>
                    </GridItem>
                </GridContainer>;
            } else {
                result = <GridItem xs={12} sm={12} md={12}>
                    <div style={{textAlign: 'center', marginBottom: '20px'}}>
                    </div>
                    <div>
                        You haven't created any personal transaction yet.
                    </div>
                    <div style={{textAlign: 'center', marginTop: '20px'}}>
                    </div>
                </GridItem>;
            }
        }
    }
    return result;
}
