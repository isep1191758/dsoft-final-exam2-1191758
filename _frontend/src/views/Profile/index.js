import React, {useContext} from "react";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import GridItem from "../../components/Grid/GridItem";
import AppContext from "../../context/AppContext";
import {withFormik} from "formik";
import * as Yup from "yup";
import {toast} from "react-toastify";
import {css} from "glamor";
import {TextInput} from "../../components/Inputs/TextInput";
import GridContainer from "../../components/Grid/GridContainer";
import MaterialButton from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import jorgemiguel from '../../assets/img/face.jpg';

export default function Profile() {
    const {state} = useContext(AppContext);
    const {user} = state;
    let personsArr = [];

    for (let j = 0; j < state.persons.data.length; j++) {
        let person = state.persons.data[j];
        const found = personsArr.some(el => el.value === person);
        if (!found) {
            personsArr.push({
                value: state.persons.data[j].personID,
                label: state.persons.data[j].name
            });
        }
    }
    let mothersName = '';
    let fathersName = '';
    for (var i in personsArr) {
        if (personsArr[i].value === state.user.mother) {
            mothersName = personsArr[i].label
        }
        if (personsArr[i].value === state.user.father) {
            fathersName = personsArr[i].label
        }
    }
    return (
        <GridItem xs={12} sm={12} md={12}>
            <Card>
                <CardBody>
                    <EnhancedForm key={user.personID} data={user} mother={mothersName} father={fathersName}/>
                </CardBody>
            </Card>
        </GridItem>);
}
const formikEnhancer = withFormik({
    enableReinitialize: true,
    validationSchema: Yup.object().shape({
        name: Yup.array()
            .of(
                Yup.object().shape({
                    label: Yup.string().required(),
                    value: Yup.string().required(),
                })
            ),
    }),
    handleSubmit: (values, {setSubmitting, resetForm}) => {
        toast.success("Profile updated successfully", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            className: css({
                backgroundColor: '#007E33',
                fontWeight: 500,
                color: '#ffffff'
            })
        });
        setSubmitting(false);
    },
    displayName: 'ProfileForm',
});

const ProfileForm = props => {
    const {
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    } = props;
    return (
        <form onSubmit={handleSubmit}>
            <h2>User</h2>
            <GridContainer>
                <GridItem xs={12} sm={12} md={3}>
                    <img src={jorgemiguel} style={{width: '100%'}} alt="Jorge Miguel"/>
                </GridItem>
                <GridItem xs={12} sm={12} md={9}>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={12}>
                            <TextInput
                                id="name"
                                name="name"
                                type="text"
                                label="Name"
                                placeholder="Insert your name..."
                                error={touched.name && errors.name}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={props.data.name}
                                disabled
                            />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <TextInput
                                id="birthdate"
                                name="birthdate"
                                type="text"
                                label="Birthdate"
                                placeholder="Insert your birthdate..."
                                error={touched.birthdate && errors.birthdate}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={props.data.birthdate}
                                disabled
                            />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <TextInput
                                id="birthplace"
                                name="birthplace"
                                type="text"
                                label="Birthplace"
                                placeholder="Insert your birthplace..."
                                error={touched.birthplace && errors.birthplace}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={props.data.birthplace}
                                disabled
                            />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <TextInput
                                id="mother"
                                name="mother"
                                type="text"
                                label="Mother"
                                placeholder="Insert your mother's name..."
                                error={touched.mother && errors.mother}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={props.mother}
                                disabled
                            />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <TextInput
                                id="father"
                                name="father"
                                type="text"
                                label="Father"
                                placeholder="Insert your father's name..."
                                error={touched.father && errors.father}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={props.father}
                                disabled
                            />
                        </GridItem>
                    </GridContainer>
                </GridItem>
            </GridContainer>

            <div style={{textAlign: 'center', marginTop: '20px'}}>
                <MaterialButton type="submit" variant="contained"
                                color="primary"
                                size="medium"
                                style={{margin: '0 10px 0 0'}}
                                startIcon={<SaveIcon/>}
                                disabled={isSubmitting}>Update</MaterialButton>
            </div>
        </form>
    );
};

const EnhancedForm = formikEnhancer(ProfileForm);
