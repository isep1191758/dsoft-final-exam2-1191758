import React from "react";
import PropTypes from "prop-types";
import SideBar from "../../../components/Sidebar/Sidebar";
import Navbar from "../../../components/Navbars/Navbar.js";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../../assets/jss/material-dashboard-react/layouts/adminStyle.js";
// images
import logo from "../../../assets/img/logo.png";

const useStyles = makeStyles(styles);

let ps;

export default function DefaultLayout({children}) {
    // styles
    const classes = useStyles();
    // ref to help us initialize PerfectScrollbar on windows devices
    const mainPanel = React.createRef();
    // states and functions
    const [color] = React.useState("blue");
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const resizeFunction = () => {
        if (window.innerWidth >= 960) {
            setMobileOpen(false);
        }
    };
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    // initialize and destroy the PerfectScrollbar plugin
    React.useEffect(() => {
        if (navigator.platform.indexOf("Win") > -1) {
            ps = new PerfectScrollbar(mainPanel.current, {
                suppressScrollX: true,
                suppressScrollY: false
            });
            document.body.style.overflow = "hidden";
        }
        window.addEventListener("resize", resizeFunction);
        // Specify how to clean up after this effect:
        return function cleanup() {
            if (navigator.platform.indexOf("Win") > -1) {
                ps.destroy();
            }
            window.removeEventListener("resize", resizeFunction);
        };
    }, [mainPanel]);

    return <div className={classes.wrapper}>
        <SideBar logoText={"Personal Finances"}
                 logo={logo}
                 handleDrawerToggle={handleDrawerToggle}
                 open={mobileOpen}
                 color={color}/>
        <div className={classes.mainPanel} ref={mainPanel}>
            <Navbar
                handleDrawerToggle={handleDrawerToggle}
            />
            <div className={classes.content}>
                <div className={classes.container}>{children}</div>
            </div>
        </div>
    </div>;
}

DefaultLayout.propTypes = {
    children: PropTypes.element.isRequired
};
