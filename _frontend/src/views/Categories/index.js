import React, {useContext, useEffect} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import history from "../../services/history";
import AppContext from '../../context/AppContext';
import {fetchCategoriesFailure, fetchCategoriesStarted, fetchCategoriesSuccess} from "../../actions/CategoryActions";
// core components
import CircularProgress from '@material-ui/core/CircularProgress';
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Card from "../../components/Card/Card.js";
import CardBody from "../../components/Card/CardBody.js";
import Table from "../../components/Table/Table.js";
import Button from '@material-ui/core/Button';
// @material-ui/icons
import EditIcon from '@material-ui/icons/Edit';
import PlusIcon from '@material-ui/icons/Add';
import PersonIcon from '@material-ui/icons/Person';
import GroupIcon from '@material-ui/icons/Group';
import Grid from "@material-ui/core/Grid";
import {toast} from "react-toastify";
import {css} from "glamor";
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Groups from '../Groups/index';
import GetHypermediaHref from "../../utils/GetHypermediaHref";

export default function Categories(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    let personalCategories = PersonalCategories();
    let groupCategories = GroupCategories();
    {
        Groups()
    }
    return (
        <div className={classes.root}>
            <AppBar position="static" color="default" elevation={1}>
                <Tabs value={value} onChange={handleChange} aria-label="categories tabs" indicatorColor="primary"
                >
                    <Tab label="Personal" icon={<PersonIcon/>} {...a11yProps(0)} />
                    <Tab label="Group" icon={<GroupIcon/>}  {...a11yProps(1)}  />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                {personalCategories}
            </TabPanel>
            <TabPanel value={value} index={1}>
                {groupCategories}
            </TabPanel>
        </div>
    );
}

function TabPanel(props) {
    const {children, value, index, ...other} = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

export function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

function PersonalCategories(props) {
    const {state, dispatch} = useContext(AppContext);
    const {categories, user} = state;
    const {loading, error, data} = categories;
    const {personID} = user;
    const navigateToPersonalCategoriesAdd = () => {
        history.push('/categories/add');
    };
    const fetchUrl = GetHypermediaHref("categories");
    useEffect(() => {
        dispatch(fetchCategoriesStarted());
        fetch(fetchUrl)
            .then(res => res.json())
            .then(res => dispatch(fetchCategoriesSuccess(res)))
            .catch(err => dispatch(fetchCategoriesFailure(err.message)))
        ;
    }, [dispatch]);
    const columns = React.useMemo(
        () => [
            {
                Header: 'Designation',
                accessor: 'designation'
            }, {
                Header: "Actions",
                id: "edit-button",
                Cell: ({row}) => (
                    <div style={{textAlign: 'center'}}>
                        <Link to={`/categories/edit/${row.id}`} style={{backgroundColor: '#539f8c'}}
                              className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary">
                            <EditIcon fontSize="small" style={{marginRight: '10px'}}/> Edit
                        </Link>
                    </div>
                )
            }
        ],
        []
    );
    let result = '';
    if (loading === true) {
        result = <Grid container style={{height: "100%"}}>
            <Grid item xs={12} sm={12} md={12} container direction="column"
                  alignItems="center"
                  justify="center">
                <CircularProgress color="primary"/>
            </Grid>
        </Grid>;
    } else {
        if (error !== null) {
            return '';
        } else {
            if (data.length > 0) {
                result = <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardBody>
                                <Table columns={columns} data={data}/>
                                <div onClick={navigateToPersonalCategoriesAdd}
                                     style={{textAlign: 'center', marginTop: '20px'}}>
                                    <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>;
            } else {
                result = <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardBody>
                            <div onClick={navigateToPersonalCategoriesAdd}
                                 style={{textAlign: 'center', marginBottom: '20px'}}>
                                <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                            </div>
                            <div>
                                You haven't created any personal categories yet.
                            </div>
                            <div onClick={navigateToPersonalCategoriesAdd}
                                 style={{textAlign: 'center', marginTop: '20px'}}>
                                <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>;
            }
        }
    }
    return result;
}

function GroupCategories(props) {
    const {state, dispatch} = useContext(AppContext);
    const {groups, user} = state;
    const {loading, error, data} = groups;
    const {personID} = user;
    const navigateToGroupCategoriesAdd = () => {
        history.push('/categories/groupAdd');
    };

    let categoriesArr = [];

    for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < data[i].categories.length; j++) {
            categoriesArr.push({
                category: data[i].categories[j],
                groupDescription: data[i].groupDescription
            });
        }
    }

    const columns = React.useMemo(
        () => [
            {
                Header: 'Designation',
                accessor: 'category',
            }, {
                Header: 'Group',
                accessor: 'groupDescription'
            }, {
                Header: "Actions",
                id: "edit-button",
                Cell: ({row}) => (
                    <div style={{textAlign: 'center'}}>
                        <Link to={`/categories/edit/${row.id}`} style={{backgroundColor: '#539f8c'}}
                              className="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSecondary">
                            <EditIcon fontSize="small" style={{marginRight: '10px'}}/> Edit
                        </Link>
                    </div>
                )
            }
        ],
        []
    );
    let result = '';
    if (loading === true) {
        result = <Grid container style={{height: "100%"}}>
            <Grid item xs={12} sm={12} md={12} container direction="column"
                  alignItems="center"
                  justify="center">
                <CircularProgress color="primary"/>
            </Grid>
        </Grid>;
    } else {
        if (error !== null) {
            toast.error("Please check your Internet connection!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                className: css({
                    fontWeight: 500,
                    color: '#ffffff'
                })
            });
            return '';
        } else {
            if (data.length > 0) {
                result = <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                        <Card>
                            <CardBody>
                                <Table columns={columns} data={categoriesArr}/>
                                <div onClick={navigateToGroupCategoriesAdd}
                                     style={{textAlign: 'center', marginTop: '20px'}}>
                                    <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                                </div>
                            </CardBody>
                        </Card>
                    </GridItem>
                </GridContainer>;
            } else {
                result = <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardBody>

                            <div onClick={navigateToGroupCategoriesAdd}
                                 style={{textAlign: 'center', marginBottom: '20px'}}>
                                <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                            </div>
                            <div>
                                You haven't created any group categories yet.
                            </div>
                            <div onClick={navigateToGroupCategoriesAdd}
                                 style={{textAlign: 'center', marginTop: '20px'}}>
                                <Button startIcon={<PlusIcon/>} variant="contained" color="primary">Add new</Button>
                            </div>
                        </CardBody>
                    </Card>
                </GridItem>
            }
        }
    }
    return result;
}
