import React from "react";
import Select from "react-select";

export class ReactSelect extends React.Component {
    handleChange = value => {
        this.props.onChange(this.props.name, value);
    };

    handleBlur = () => {
        this.props.onBlur(this.props.name, true);
    };

    render() {
        return (
            <div style={{margin: '1rem 0'}}>
                <label htmlFor="color">{this.props.label}</label>
                <Select
                    id={this.props.id}
                    name={this.props.name}
                    options={this.props.options}
                    multi={true}
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.props.value}
                    defaultValue={this.props.defaultValue}
                    isMulti={this.props.isMulti}
                />
                {!!this.props.error &&
                this.props.touched && (
                    <div style={{color: 'red', marginTop: '.5rem'}}>{this.props.error}</div>
                )}
            </div>
        );
    }
}