import React from 'react';
import {observer} from 'mobx-react';
import TextField from '@material-ui/core/TextField';

export default observer(({
                             field,
                             type = 'text',
                             defaultValue
                         }) => (
    <div>
        <TextField defaultValue={field.defaultValue} {...field.bind({type, defaultValue})} fullWidth
                   variant={'outlined'}
                   error={!!field.error}
                   helperText={field.error}/>
        <small
            className="f7 red-60 db mt1 mb3 red"
        >
        </small>
    </div>
));