import React from 'react';
import {observer} from 'mobx-react';
import Select from 'react-select';

export default observer(({field}) => (
    <div>
        <Select
            placeholder={field.label}
            {...field.bind()}
            options={field.extra}
            resetValue={[]}
            multi
        />
    </div>
));