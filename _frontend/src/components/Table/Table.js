import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import {usePagination, useRowSelect, useSortBy, useTable} from 'react-table'
import MaUTable from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Checkbox from "@material-ui/core/Checkbox";
import NativeSelect from '@material-ui/core/NativeSelect';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import LastPageIcon from '@material-ui/icons/LastPage';
import styles from "../../assets/jss/material-dashboard-react/components/tableStyle.js";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(styles);

export default function CustomTable({columns, data}) {
    // styles
    const classes = useStyles();
    // Use the state and functions returned from useTable to build the UI
    const {
        getTableProps,
        headerGroups,
        prepareRow,
        page, // Instead of using 'rows', we'll use page,
        // which has only the rows for the active page
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        state: {
            pageIndex,
            pageSize,
        },
    } = useTable(
        {
            columns,
            data,
            disableMultiSort: true,
        },
        useSortBy,
        usePagination,
        useRowSelect,
        // Here we will use a plugin to add our selection column
        hooks => {
            hooks.visibleColumns.push(columns => {
                return [
                    {
                        id: 'selection',
                        // Make this column a groupByBoundary. This ensures that groupBy columns
                        // are placed after it
                        groupByBoundary: true,
                        // The header can use the table's getToggleAllRowsSelectedProps method
                        // to render a checkbox
                        Header: ({getToggleAllRowsSelectedProps}) => (
                            <div>
                                <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
                            </div>
                        ),
                        // The cell can use the individual row's getToggleRowSelectedProps method
                        // to the render a checkbox
                        Cell: ({row}) => (
                            <div>
                                <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
                            </div>
                        ),
                    },
                    ...columns,
                ]
            })
        }
    )

    // Render the UI for your table
    return (
        <>
            <MaUTable {...getTableProps()}>
                <TableHead>
                    {headerGroups.map(headerGroup => (
                        <TableRow className={classes.tableHeadRow} {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <TableCell {...column.getHeaderProps()}>
                                    <span {...column.getSortByToggleProps()}>
                                    {column.render("Header")}
                                        {column.isSorted
                                            ? column.isSortedDesc
                                                ? ' 🔽'
                                                : ' 🔼'
                                            : ''}
                                    </span>
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableHead>
                <TableBody>
                    {page.map((row, i) => {
                        prepareRow(row);
                        return (
                            <TableRow {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return (
                                        <TableCell {...cell.getCellProps()}>
                                            {cell.render("Cell")}
                                        </TableCell>
                                    );
                                })}
                            </TableRow>
                        );
                    })}
                </TableBody>
            </MaUTable>
            <div className={classes.pagination}>
                <IconButton onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
                    <FirstPageIcon/>
                </IconButton>
                {' '}
                <IconButton size="small" onClick={() => previousPage()} disabled={!canPreviousPage}>
                    <KeyboardArrowLeftIcon/>
                </IconButton>
                {' '}
                <span className=''>
                  Page{' '}
                    <strong>
                    {pageIndex + 1} of {pageOptions.length}
                  </strong>{' '}
                </span>
                {' '}
                <IconButton size="small" onClick={() => nextPage()} disabled={!canNextPage}>
                    <KeyboardArrowRightIcon/>
                </IconButton>
                {' '}
                <IconButton size="small" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
                    <LastPageIcon/>
                </IconButton>
                {'  '}
                <span className={classes.tableInputPage}>
                    Go to page:{' '}
                    <TextField style={{marginTop: '9px'}} type="number"
                               defaultValue={pageIndex + 1}
                               onChange={e => {
                                   const page = e.target.value ? Number(e.target.value) - 1 : 0
                                   gotoPage(page)
                               }}
                    />
                    </span>{' | '}
                <NativeSelect
                    className={classes.tableSelectInput}
                    value={pageSize}
                    onChange={e => {
                        setPageSize(Number(e.target.value))
                    }}
                    inputProps={{
                        name: 'page',
                        id: 'age-native-helper',
                    }}
                >
                    {[2, 10, 20, 30, 40, 50].map(pageSize => (
                        <option key={pageSize} value={pageSize}>
                            Show {pageSize}
                        </option>
                    ))}
                </NativeSelect>
            </div>
        </>
    );
}

const IndeterminateCheckbox = React.forwardRef(
    ({indeterminate, ...rest}, ref) => {
        const defaultRef = React.useRef()
        const resolvedRef = ref || defaultRef

        React.useEffect(() => {
            resolvedRef.current.indeterminate = indeterminate
        }, [resolvedRef, indeterminate])

        return (
            <>
                <FormControlLabel
                    control={<Checkbox ref={resolvedRef} {...rest} color="primary"/>}
                    label=""
                />
            </>
        )
    }
)

CustomTable.defaultProps = {
    tableHeaderColor: "gray"
};

CustomTable.propTypes = {
    tableHeaderColor: PropTypes.oneOf([
        "warning",
        "primary",
        "danger",
        "success",
        "info",
        "rose",
        "gray"
    ]),
    tableHead: PropTypes.arrayOf(PropTypes.string),
    tableData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
};