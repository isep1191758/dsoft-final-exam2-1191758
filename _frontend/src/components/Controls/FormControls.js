import React from 'react';
import {observer} from 'mobx-react';
import Button from '@material-ui/core/Button';

export default observer(({form, controls = null}) => (
    <div className="tc tl-ns mt2">

        {(!controls || controls.onSubmit) &&
        <Button
            type="submit"
            className='f6 link dim br2 ba bw1 ph3 pv2 mv2 mr1 dib b--light-gray bg-white light-red'
            onClick={form.onSubmit}
            content={(form.submitting || form.validating)
                ? <b><i className="fa fa-spinner fa-spin"/></b>
                : <b><i className="fa fa-dot-circle-o"/> Submit</b>}
        />}

        {(!controls || controls.onClear) &&
        <Button
            text={'Clear'}
            icon="eraser"
            className='f6 link dim br2 ba bw1 ph3 pv2 mv2 mr1 dib b--light-gray bg-white light-red'
            onClick={form.onClear}
        />}

        {(!controls || controls.onReset) &&
        <Button
            text={'Reset'}
            icon="refresh"
            className='f6 link dim br2 ba bw1 ph3 pv2 mv2 mr1 dib b--light-gray bg-white light-red'
            onClick={form.onReset}
        />}

        <div className="f6 db red">
            {form.error}
        </div>

    </div>
));