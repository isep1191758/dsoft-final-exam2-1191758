import DashboardIcon from "@material-ui/icons/Home";
import TransactionsIcon from "@material-ui/icons/Money";
import GroupsIcon from "@material-ui/icons/Group";
import AccountsIcon from "@material-ui/icons/LibraryBooks";
import CategoriesIcon from "@material-ui/icons/Category";
// core components/views for Admin layout
import Dashboard from "../views/Dashboard/index.js";
import Transactions from "../views/Transactions/index";
import Groups from "../views/Groups/index.js";
import Accounts from "../views/Accounts/index.js";
import Categories from "../views/Categories/index.js";
import Profile from "../views/Profile";

const dashboardRoutes = [
    {
        path: "/dashboard",
        name: "Home",
        icon: DashboardIcon,
        component: Dashboard,
        layout: "",
        visible: true
    },
    {
        path: "/transactions",
        name: "Transactions",
        icon: TransactionsIcon,
        component: Transactions,
        layout: "",
        visible: true
    },
    {
        path: "/groups",
        name: "Groups",
        icon: GroupsIcon,
        component: Groups,
        layout: "",
        visible: true
    },
    {
        path: "/accounts",
        name: "Accounts",
        icon: AccountsIcon,
        component: Accounts,
        layout: "",
        visible: true
    },
    {
        path: "/categories",
        name: "Categories",
        icon: CategoriesIcon,
        component: Categories,
        layout: "",
        visible: true
    },
    {
        path: "/profile",
        name: "Profile",
        icon: CategoriesIcon,
        component: Profile,
        layout: "",
        visible: false
    }
];

export default dashboardRoutes;
