import {useContext} from "react";
import AppContext from "../context/AppContext";
import history from "../services/history";

export default function GetHypermediaHref(props) {
    const {state} = useContext(AppContext);
    const {hypermedia} = state;
    const {data} = hypermedia;
    let href = '';

    for(let i = 0; i<data[0].links.length; i++){
        if(data[0].links[i].rel === props){
            href = data[0].links[i].href;
            }
    }

    return href;
}