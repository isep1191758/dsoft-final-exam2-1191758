export const FETCH_ACCOUNTS_STARTED = 'FETCH_ACCOUNTS_STARTED';
export const FETCH_ACCOUNTS_SUCCESS = 'FETCH_ACCOUNTS_SUCCESS';
export const FETCH_ACCOUNTS_FAILURE = 'FETCH_ACCOUNTS_FAILURE';
export const FETCH_ACCOUNTS_GROUP_STARTED = 'FETCH_ACCOUNTS_GROUP_STARTED';
export const FETCH_ACCOUNTS_GROUP_SUCCESS = 'FETCH_ACCOUNTS_GROUP_SUCCESS';
export const FETCH_ACCOUNTS_GROUP_FAILURE = 'FETCH_ACCOUNTS_GROUP_FAILURE';

export function fetchAccountsStarted() {
    return {
        type: FETCH_ACCOUNTS_STARTED,

    }
}

export function fetchAccountsSuccess(accounts) {
    return {
        type: FETCH_ACCOUNTS_SUCCESS,
        payload: {
            data:
                [...accounts]
        }

    }
}

export function fetchAccountsFailure(message) {
    return {
        type: FETCH_ACCOUNTS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchAccountsGroupStarted() {
    return {
        type: FETCH_ACCOUNTS_GROUP_STARTED,

    }
}

export function fetchAccountsGroupSuccess(groupAccounts) {
    return {
        type: FETCH_ACCOUNTS_GROUP_SUCCESS,
        payload: {
            data:
                [...groupAccounts]
        }
    }
}

export function fetchAccountsGroupFailure(message) {
    return {
        type: FETCH_ACCOUNTS_GROUP_FAILURE,
        payload: {
            error: message
        }
    }
}
