export const URL_API = 'http://localhost:8080';

export const FETCH_GROUPS_STARTED = 'FETCH_GROUPS_STARTED';
export const FETCH_GROUPS_SUCCESS = 'FETCH_GROUPS_SUCCESS';
export const FETCH_GROUPS_FAILURE = 'FETCH_GROUPS_FAILURE';
export const DELETE_GROUP = 'DELETE_GROUP';
export const FETCH_PERSONS_STARTED = 'FETCH_PERSONS_STARTED';
export const FETCH_PERSONS_SUCCESS = 'FETCH_PERSONS_SUCCESS';
export const FETCH_PERSONS_FAILURE = 'FETCH_PERSONS_FAILURE';
export const FETCH_TRANSACTIONS_STARTED = 'FETCH_TRANSACTIONS_STARTED';
export const FETCH_TRANSACTIONS_SUCCESS = 'FETCH_TRANSACTIONS_SUCCESS';
export const FETCH_TRANSACTIONS_FAILURE = 'FETCH_TRANSACTIONS_FAILURE';
export const FETCH_PERSONAL_TRANSACTIONS_STARTED = 'FETCH_PERSONAL_TRANSACTIONS_STARTED';
export const FETCH_PERSONAL_TRANSACTIONS_SUCCESS = 'FETCH_PERSONAL_TRANSACTIONS_SUCCESS';
export const FETCH_PERSONAL_TRANSACTIONS_FAILURE = 'FETCH_PERSONAL_TRANSACTIONS_FAILURE';
export const FETCH_GROUP_TRANSACTIONS_STARTED = 'FETCH_GROUP_TRANSACTIONS_STARTED';
export const FETCH_GROUP_TRANSACTIONS_SUCCESS = 'FETCH_GROUP_TRANSACTIONS_SUCCESS';
export const FETCH_GROUP_TRANSACTIONS_FAILURE = 'FETCH_GROUP_TRANSACTIONS_FAILURE';
export const FETCH_ACCOUNTS_STARTED = 'FETCH_ACCOUNTS_STARTED';
export const FETCH_ACCOUNTS_SUCCESS = 'FETCH_ACCOUNTS_SUCCESS';
export const FETCH_ACCOUNTS_FAILURE = 'FETCH_ACCOUNTS_FAILURE';
export const FETCH_USER_STARTED = 'FETCH_USER_STARTED';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';
export const FETCH_PIE_DASHBOARD_STARTED = 'FETCH_PIE_DASHBOARD_STARTED';
export const FETCH_PIE_DASHBOARD_SUCCESS = 'FETCH_PIE_DASHBOARD_SUCCESS';
export const FETCH_PIE_DASHBOARD_FAILURE = 'FETCH_PIE_DASHBOARD_FAILURE';
export const FETCH_LINES_DASHBOARD_STARTED = 'FETCH_LINES_DASHBOARD_STARTED';
export const FETCH_LINES_DASHBOARD_SUCCESS = 'FETCH_LINES_DASHBOARD_SUCCESS';
export const FETCH_LINES_DASHBOARD_FAILURE = 'FETCH_LINES_DASHBOARD_FAILURE';
export const FETCH_TRANSACTIONS_DASHBOARD_STARTED = 'FETCH_TRANSACTIONS_DASHBOARD_STARTED';
export const FETCH_TRANSACTIONS_DASHBOARD_SUCCESS = 'FETCH_TRANSACTIONS_DASHBOARD_SUCCESS';
export const FETCH_TRANSACTIONS_DASHBOARD_FAILURE = 'FETCH_TRANSACTIONS_DASHBOARD_FAILURE';
export const FETCH_HYPERMEDIA_STARTED = 'FETCH_HYPERMEDIA_STARTED';
export const FETCH_HYPERMEDIA_SUCCESS = 'FETCH_HYPERMEDIA_SUCCESS';
export const FETCH_HYPERMEDIA_FAILURE = 'FETCH_HYPERMEDIA_FAILURE';
export const LOGGEDIN = 'LOGGEDIN';
export const LOGGEDOUT = 'LOGGEDOUT';

export function fetchHypermediaStarted() {
    return {
        type: FETCH_HYPERMEDIA_STARTED,

    }
}

export function fetchHypermediaSuccess(hyperlinks) {
    return {
        type: FETCH_HYPERMEDIA_SUCCESS,
        payload: {
            data:
                [...hyperlinks]
        }

    }
}

export function fetchHypermediaFailure(message) {
    return {
        type: FETCH_HYPERMEDIA_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchGroupsStarted() {
    return {
        type: FETCH_GROUPS_STARTED,

    }
}

export function fetchGroupsSuccess(groups) {
    return {
        type: FETCH_GROUPS_SUCCESS,
        payload: {
            data:
                [...groups]
        }

    }
}

export function fetchGroupsFailure(message) {
    return {
        type: FETCH_GROUPS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function deleteGroup() {
    return {
        type: DELETE_GROUP,
    }
}

export function fetchUserStarted() {
    return {
        type: FETCH_USER_STARTED,

    }
}

export function fetchUserSuccess(user) {
    return {
        type: FETCH_USER_SUCCESS,
        payload: {
            data:
                [...user]
        }

    }
}

export function fetchUserFailure(message) {
    return {
        type: FETCH_USER_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchPersonsStarted() {
    return {
        type: FETCH_PERSONS_STARTED,

    }
}

export function fetchPersonsSuccess(persons) {
    return {
        type: FETCH_PERSONS_SUCCESS,
        payload: {
            data:
                [...persons]
        }

    }
}

export function fetchPersonsFailure(message) {
    return {
        type: FETCH_PERSONS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function login() {
    return {
        type: LOGGEDIN,
    }
}

export function logout() {
    return {
        type: LOGGEDOUT,
    }
}

export function fetchTransactionsStarted() {
    return {
        type: FETCH_TRANSACTIONS_STARTED,
    }
}

export function fetchTransactionsSuccess(transactions) {
    return {
        type: FETCH_TRANSACTIONS_SUCCESS,
        payload: {
            data:
                [...transactions]
        }

    }
}

export function fetchTransactionsFailure(message) {
    return {
        type: FETCH_TRANSACTIONS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchAccountsStarted() {
    return {
        type: FETCH_ACCOUNTS_STARTED,

    }
}

export function fetchAccountsSuccess(persons) {
    return {
        type: FETCH_ACCOUNTS_SUCCESS,
        payload: {
            data:
                [...persons]
        }

    }
}

export function fetchAccountsFailure(message) {
    return {
        type: FETCH_ACCOUNTS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchPieDashboardFailure(message) {
    return {
        type: FETCH_PIE_DASHBOARD_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchPieDashboardStarted() {
    return {
        type: FETCH_PIE_DASHBOARD_STARTED,

    }
}

export function fetchPieDashboardSuccess(info) {
    return {
        type: FETCH_PIE_DASHBOARD_SUCCESS,
        payload: {
            data:
                [...info]
        }

    }
}

export function fetchLinesDashboardFailure(message) {
    return {
        type: FETCH_LINES_DASHBOARD_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchLinesDashboardStarted() {
    return {
        type: FETCH_LINES_DASHBOARD_STARTED,

    }
}

export function fetchLinesDashboardSuccess(info) {
    return {
        type: FETCH_LINES_DASHBOARD_SUCCESS,
        payload: {
            data:
                [...info]
        }
    }
}

export function fetchTransactionsDashboardStarted() {
    return {
        type: FETCH_TRANSACTIONS_DASHBOARD_STARTED,
    }
}

export function fetchTransactionsDashboardSuccess(groupTransactions) {
    return {
        type: FETCH_TRANSACTIONS_DASHBOARD_SUCCESS,
        payload: {
            data:
                [...groupTransactions]
        }

    }
}

export function fetchTransactionsDashboardFailure(message) {
    return {
        type: FETCH_TRANSACTIONS_DASHBOARD_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchPersonalTransactionsStarted() {
    return {
        type: FETCH_PERSONAL_TRANSACTIONS_STARTED,
    }
}

export function fetchPersonalTransactionsSuccess(transactions) {
    return {
        type: FETCH_PERSONAL_TRANSACTIONS_SUCCESS,
        payload: {
            data:
                [...transactions]
        }

    }
}

export function fetchPersonalTransactionsFailure(message) {
    return {
        type: FETCH_PERSONAL_TRANSACTIONS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchGroupTransactionsStarted() {
    return {
        type: FETCH_GROUP_TRANSACTIONS_STARTED,
    }
}

export function fetchGroupTransactionsSuccess(groupTransactions) {
    return {
        type: FETCH_GROUP_TRANSACTIONS_SUCCESS,
        payload: {
            data:
                [...groupTransactions]
        }

    }
}

export function fetchGroupTransactionsFailure(message) {
    return {
        type: FETCH_GROUP_TRANSACTIONS_FAILURE,
        payload: {
            error: message
        }
    }
}
