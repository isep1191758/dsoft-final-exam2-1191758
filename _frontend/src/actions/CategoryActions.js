export const FETCH_CATEGORIES_STARTED = 'FETCH_CATEGORIES_STARTED';
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_CATEGORIES_FAILURE = 'FETCH_CATEGORIES_FAILURE';
export const FETCH_CATEGORIES_GROUP_STARTED = 'FETCH_CATEGORIES_GROUP_STARTED';
export const FETCH_CATEGORIES_GROUP_SUCCESS = 'FETCH_CATEGORIES_GROUP_SUCCESS';
export const FETCH_CATEGORIES_GROUP_FAILURE = 'FETCH_CATEGORIES_GROUP_FAILURE';


export function fetchCategoriesStarted() {
    return {
        type: FETCH_CATEGORIES_STARTED,

    }
}

export function fetchCategoriesSuccess(categories) {
    return {
        type: FETCH_CATEGORIES_SUCCESS,
        payload: {
            data:
                [...categories]
        }

    }
}

export function fetchCategoriesFailure(message) {
    return {
        type: FETCH_CATEGORIES_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchCategoriesGroupStarted() {
    return {
        type: FETCH_CATEGORIES_GROUP_STARTED,

    }
}

export function fetchCategoriesGroupSuccess(groupCategories) {
    return {
        type: FETCH_CATEGORIES_GROUP_SUCCESS,
        payload: {
            data:
                [...groupCategories]
        }
    }
}

export function fetchCategoriesGroupFailure(message) {
    return {
        type: FETCH_CATEGORIES_GROUP_FAILURE,
        payload: {
            error: message
        }
    }
}

