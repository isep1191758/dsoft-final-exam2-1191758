import {
    dangerColor,
    defaultFont,
    grayColor,
    infoColor,
    primaryColor,
    roseColor,
    successColor,
    warningColor
} from "../../material-dashboard-react.js";

const tableStyle = theme => ({
    warningTableHeader: {
        color: warningColor[0]
    },
    primaryTableHeader: {
        color: primaryColor[0]
    },
    dangerTableHeader: {
        color: dangerColor[0]
    },
    successTableHeader: {
        color: successColor[0]
    },
    infoTableHeader: {
        color: infoColor[0]
    },
    roseTableHeader: {
        color: roseColor[0]
    },
    grayTableHeader: {
        color: grayColor[0]
    },
    table: {
        marginBottom: "0",
        width: "100%",
        maxWidth: "100%",
        backgroundColor: "transparent",
        borderSpacing: "0",
        borderCollapse: "collapse"
    },
    tableHeadCell: {
        color: "inherit",
        ...defaultFont,
        "&, &$tableCell": {
            fontSize: "1em"
        }
    },
    tableCell: {
        ...defaultFont,
        lineHeight: "1.42857143",
        padding: "12px 8px",
        verticalAlign: "middle",
        fontSize: "0.8125rem"
    },
    tableResponsive: {
        width: "100%",
        marginTop: theme.spacing(3),
        overflowX: "auto"
    },
    tableHeadRow: {
        height: "52px",
        color: "inherit",
        display: "table-row",
        outline: "none",
        verticalAlign: "middle",
        backgroundColor: "#eaeaea",
        "& th": {
            padding: "padding: 0 16px;"
        },
        "& th:first-child": {
            width: "20px"
        },
        "& th:last-child": {
            textAlign: 'center',
            width: "200px"
        }
    },
    tableBodyRow: {
        height: "48px",
        color: "inherit",
        display: "table-row",
        outline: "none",
        verticalAlign: "middle",
        marginBottom: "1px solid #d3d3d3"
    },
    tableSelectInput: {
        "& select": {
            fontSize: "13px"
        }
    },
    tableInputPage: {
        "& .MuiInputBase-input.MuiInput-input": {
            fontSize: "13px",
            width: "68px"
        }
    },
    pagination: {
        textAlign: "center"
    }
});
export default tableStyle;
